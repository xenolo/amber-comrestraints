#include "copyright.i"

/***************************************************/
/*                                                 */
/*      AMBER NVIDIA CUDA GPU IMPLEMENTATION       */
/*                 PMEMD VERSION                   */
/*                   Feb 2014                      */
/*                      by                         */
/*                Scott Le Grand                   */
/*                     and                         */
/*                Ross C. Walker                   */
/*                                                 */
/***************************************************/

{
#define NMR_TIMED

#ifdef NMR_ENERGY
#if __CUDA_ARCH__ >= 300
const int THREADS_PER_BLOCK = SM_3X_NMRFORCES_THREADS_PER_BLOCK;
#else
const int THREADS_PER_BLOCK = SM_2X_NMRFORCES_THREADS_PER_BLOCK;
#endif
    volatile __shared__ PMEAccumulator sE[THREADS_PER_BLOCK];
#endif

    int pos                                 = blockIdx.x * blockDim.x + threadIdx.x;

    // Calculate distance restraints
    if (pos < cSim.NMRDistanceOffset)
    {
#ifdef NMR_ENERGY
        PMEAccumulator edistance            = 0;
#endif
        if (pos < cSim.NMRDistances)
        {
#ifdef NMR_NEIGHBORLIST
            int2 atom                       = cSim.pImageNMRDistanceID[pos];
#else
            int2 atom                       = cSim.pNMRDistanceID[pos];
#endif
            PMEDouble2 R1R2;
            PMEDouble2 R3R4;
            PMEDouble2 K2K3;
#ifdef NMR_TIMED
            int2 Step                       = cSim.pNMRDistanceStep[pos];
            int Inc                         = cSim.pNMRDistanceInc[pos];
            // Skip restraint if not active
            if ((step < Step.x) || ((step > Step.y) && (Step.y > 0)))
                goto exit1;

            // Read timed data
            PMEDouble2 R1R2Slp              = cSim.pNMRDistanceR1R2Slp[pos];
            PMEDouble2 R1R2Int              = cSim.pNMRDistanceR1R2Int[pos];
            PMEDouble2 R3R4Slp              = cSim.pNMRDistanceR3R4Slp[pos];
            PMEDouble2 R3R4Int              = cSim.pNMRDistanceR3R4Int[pos];
            PMEDouble2 K2K3Slp              = cSim.pNMRDistanceK2K3Slp[pos];
            PMEDouble2 K2K3Int              = cSim.pNMRDistanceK2K3Int[pos];

            // Calculate increment
            double dstep                    = step - (double)((step - Step.x) % abs(Inc));

            // Calculate restraint values
            R1R2.x                          = R1R2Slp.x * dstep + R1R2Int.x;
            R1R2.y                          = R1R2Slp.y * dstep + R1R2Int.y;
            R3R4.x                          = R3R4Slp.x * dstep + R3R4Int.x;
            R3R4.y                          = R3R4Slp.y * dstep + R3R4Int.y;
            if (Inc > 0)
            {
                K2K3.x                      = K2K3Slp.x * dstep + K2K3Int.x;
                K2K3.y                      = K2K3Slp.y * dstep + K2K3Int.y;
            }
            else
            {
                int nstepu                  = (step - Step.x) / abs(Inc);
                K2K3.x                      = K2K3Int.x * pow(K2K3Slp.x, nstepu);
                K2K3.y                      = K2K3Int.y * pow(K2K3Slp.y, nstepu);
            }
#else
            R1R2                            = cSim.pNMRDistanceR1R2[pos];
            R3R4                            = cSim.pNMRDistanceR3R4[pos];
            K2K3                            = cSim.pNMRDistanceK2K3[pos];
#endif

 //           printf("%12.5f %12.5f %12.5f %12.5f %12.5f %12.5f\n", R1R2.x, R1R2.y, R3R4.x, R3R4.y, K2K3.x, K2K3.y);

#if defined(NODPTEXTURE)
#ifdef NMR_NEIGHBORLIST
            PMEDouble atomIX                = cSim.pImageX[atom.x];
            PMEDouble atomJX                = cSim.pImageX[atom.y];
            PMEDouble atomIY                = cSim.pImageY[atom.x];
            PMEDouble atomJY                = cSim.pImageY[atom.y];
            PMEDouble atomIZ                = cSim.pImageZ[atom.x];
            PMEDouble atomJZ                = cSim.pImageZ[atom.y];
//		printf("Image: %12.5f %12.5f %12.5f %12.5f %12.5f %12.5f\n", atomIX, atomIY, atomIZ, atomJX, atomJY, atomJZ);
#else
            PMEDouble atomIX                = cSim.pAtomX[atom.x];
            PMEDouble atomJX                = cSim.pAtomX[atom.y];
            PMEDouble atomIY                = cSim.pAtomY[atom.x];
            PMEDouble atomJY                = cSim.pAtomY[atom.y];
            PMEDouble atomIZ                = cSim.pAtomZ[atom.x];
            PMEDouble atomJZ                = cSim.pAtomZ[atom.y];
//                printf("Atom: %12.5f %12.5f %12.5f %12.5f %12.5f %12.5f\n", atomIX, atomIY, atomIZ, atomJX, atomJY, atomJZ);

#endif
            PMEDouble xij                   = atomIX - atomJX;
            PMEDouble yij                   = atomIY - atomJY;
#else
            int2 iatomIX                    = tex1Dfetch(texref, atom.x);
            int2 iatomJX                    = tex1Dfetch(texref, atom.y);
            int2 iatomIY                    = tex1Dfetch(texref, atom.x + cSim.stride);
            int2 iatomJY                    = tex1Dfetch(texref, atom.y + cSim.stride);
            int2 iatomIZ                    = tex1Dfetch(texref, atom.x + cSim.stride2);
            int2 iatomJZ                    = tex1Dfetch(texref, atom.y + cSim.stride2);
            PMEDouble atomIX                = __hiloint2double(iatomIX.y, iatomIX.x);
            PMEDouble atomJX                = __hiloint2double(iatomJX.y, iatomJX.x);
            PMEDouble xij                   = atomIX - atomJX;
            PMEDouble atomIY                = __hiloint2double(iatomIY.y, iatomIY.x);
            PMEDouble atomJY                = __hiloint2double(iatomJY.y, iatomJY.x);
            PMEDouble yij                   = atomIY - atomJY;
            PMEDouble atomIZ                = __hiloint2double(iatomIZ.y, iatomIZ.x);
            PMEDouble atomJZ                = __hiloint2double(iatomJZ.y, iatomJZ.x);
                //printf("Atom: %12.5f %12.5f %12.5f %12.5f %12.5f %12.5f\n", atomIX, atomIY, atomIZ, atomJX, atomJY, atomJZ);
#endif
            PMEDouble zij                   = atomIZ - atomJZ;
            PMEDouble rij                   = sqrt(xij * xij + yij * yij + zij * zij);
            PMEDouble df;
#ifdef NMR_ENERGY
            PMEDouble e;
#endif
            if (rij < R1R2.x)
            {
                PMEDouble dif               = R1R2.x - R1R2.y;
                df                          = (PMEDouble)2.0 * K2K3.x * dif;
#ifdef NMR_ENERGY
                e                           = df * (rij - R1R2.x) + K2K3.x * dif * dif;
#endif
            }
            else if (rij < R1R2.y)
            {
                PMEDouble dif               = rij - R1R2.y;
                df                          = (PMEDouble)2.0 * K2K3.x * dif;
#ifdef NMR_ENERGY
                e                           = K2K3.x * dif * dif;
#endif
            }
            else if (rij < R3R4.x)
            {
                df                          = (PMEDouble)0.0;
#ifdef NMR_ENERGY
                e                           = (PMEDouble)0.0;
#endif
            }
            else if (rij < R3R4.y)
            {
                PMEDouble dif               = rij - R3R4.x;
                df                          = (PMEDouble)2.0 * K2K3.y * dif;
#ifdef NMR_ENERGY
                e                           = K2K3.y * dif * dif;
#endif
            }
            else
            {
                PMEDouble dif               = R3R4.y - R3R4.x;
                df                          = (PMEDouble)2.0 * K2K3.y * dif;
#ifdef NMR_ENERGY
                e                           = df * (rij - R3R4.y) + K2K3.y * dif * dif;
#endif
            }

            if (cSim.bJar)
            {
                double fold                 = cSim.pNMRJarData[2];
                double work                 = cSim.pNMRJarData[3];
                double first                = cSim.pNMRJarData[4];
                double fcurr                = (PMEDouble)-2.0 * K2K3.x * (rij - R1R2.y);
                if (first == (PMEDouble)0.0) {
                    fold                    = -fcurr;
                    cSim.pNMRJarData[4]     = (PMEDouble)1.0;
                }
                work                       += (PMEDouble)0.5 * (fcurr + fold) * cSim.drjar;
                cSim.pNMRJarData[0]         = R1R2.y;
                cSim.pNMRJarData[1]         = rij;
                cSim.pNMRJarData[2]         = fcurr;
                cSim.pNMRJarData[3]         = work;
            }

#ifdef NMR_ENERGY
            edistance                      += llrint(ENERGYSCALE * e);
#endif

            df                             *= (PMEDouble)1.0 / rij;
            PMEDouble fx                    = df * xij;
            PMEDouble fy                    = df * yij;
            PMEDouble fz                    = df * zij;
            PMEAccumulator ifx              = llrint(fx * FORCESCALE);
            PMEAccumulator ify              = llrint(fy * FORCESCALE);
            PMEAccumulator ifz              = llrint(fz * FORCESCALE);
            atomicAdd((unsigned long long int*)&cSim.pBondedForceXAccumulator[atom.y], llitoulli(ifx));
            atomicAdd((unsigned long long int*)&cSim.pBondedForceYAccumulator[atom.y], llitoulli(ify));
            atomicAdd((unsigned long long int*)&cSim.pBondedForceZAccumulator[atom.y], llitoulli(ifz));
            atomicAdd((unsigned long long int*)&cSim.pBondedForceXAccumulator[atom.x], llitoulli(-ifx));
            atomicAdd((unsigned long long int*)&cSim.pBondedForceYAccumulator[atom.x], llitoulli(-ify));
            atomicAdd((unsigned long long int*)&cSim.pBondedForceZAccumulator[atom.x], llitoulli(-ifz));
        }
exit1:
        {}
#ifdef NMR_ENERGY
        sE[threadIdx.x]                     = edistance;
        sE[threadIdx.x]                    += sE[threadIdx.x ^ 1];
        sE[threadIdx.x]                    += sE[threadIdx.x ^ 2];
        sE[threadIdx.x]                    += sE[threadIdx.x ^ 4];
        sE[threadIdx.x]                    += sE[threadIdx.x ^ 8];
        sE[threadIdx.x]                    += sE[threadIdx.x ^ 16];
        if ((threadIdx.x & GRIDBITSMASK) == 0)
        {
            atomicAdd(cSim.pENMRDistance, llitoulli(sE[threadIdx.x]));
        }
#endif
    }

    // Calculate angle restraints
    else if (pos < cSim.NMRAngleOffset)
    {
        pos                                -= cSim.NMRDistanceOffset;
#ifdef NMR_ENERGY
        PMEAccumulator eangle               = 0;
#endif
        if (pos < cSim.NMRAngles)
        {
#ifdef NMR_NEIGHBORLIST
            int2 atom1                      = cSim.pImageNMRAngleID1[pos];
            int atom2                       = cSim.pImageNMRAngleID2[pos];
#else
            int2 atom1                      = cSim.pNMRAngleID1[pos];
            int atom2                       = cSim.pNMRAngleID2[pos];
#endif
            PMEDouble2 R1R2;
            PMEDouble2 R3R4;
            PMEDouble2 K2K3;
#ifdef NMR_TIMED
            int2 Step                       = cSim.pNMRAngleStep[pos];
            int Inc                         = cSim.pNMRAngleInc[pos];
            // Skip restraint if not active
            if ((step < Step.x) || ((step > Step.y) && (Step.y > 0)))
                goto exit2;

            // Read timed data
            PMEDouble2 R1R2Slp              = cSim.pNMRAngleR1R2Slp[pos];
            PMEDouble2 R1R2Int              = cSim.pNMRAngleR1R2Int[pos];
            PMEDouble2 R3R4Slp              = cSim.pNMRAngleR3R4Slp[pos];
            PMEDouble2 R3R4Int              = cSim.pNMRAngleR3R4Int[pos];
            PMEDouble2 K2K3Slp              = cSim.pNMRAngleK2K3Slp[pos];
            PMEDouble2 K2K3Int              = cSim.pNMRAngleK2K3Int[pos];

            // Calculate increment
            double dstep                    = step - (double)((step - Step.x) % abs(Inc));

            // Calculate restraint values
            R1R2.x                          = R1R2Slp.x * dstep + R1R2Int.x;
            R1R2.y                          = R1R2Slp.y * dstep + R1R2Int.y;
            R3R4.x                          = R3R4Slp.x * dstep + R3R4Int.x;
            R3R4.y                          = R3R4Slp.y * dstep + R3R4Int.y;
            if (Inc > 0)
            {
                K2K3.x                      = K2K3Slp.x * dstep + K2K3Int.x;
                K2K3.y                      = K2K3Slp.y * dstep + K2K3Int.y;
            }
            else
            {
                int nstepu                  = (step - Step.x) / abs(Inc);
                K2K3.x                      = K2K3Int.x * pow(K2K3Slp.x, nstepu);
                K2K3.y                      = K2K3Int.y * pow(K2K3Slp.y, nstepu);
            }
#else
            R1R2                            = cSim.pNMRAngleR1R2[pos];
            R3R4                            = cSim.pNMRAngleR3R4[pos];
            K2K3                            = cSim.pNMRAngleK2K3[pos];
#endif

#if defined(NODPTEXTURE)
#ifdef NMR_NEIGHBORLIST
            PMEDouble atomIX                = cSim.pImageX[atom1.x];
            PMEDouble atomJX                = cSim.pImageX[atom1.y];
            PMEDouble atomKX                = cSim.pImageX[atom2];
            PMEDouble atomIY                = cSim.pImageY[atom1.x];
            PMEDouble atomJY                = cSim.pImageY[atom1.y];
            PMEDouble atomKY                = cSim.pImageY[atom2];
            PMEDouble atomIZ                = cSim.pImageZ[atom1.x];
            PMEDouble atomJZ                = cSim.pImageZ[atom1.y];
            PMEDouble atomKZ                = cSim.pImageZ[atom2];
#else
            PMEDouble atomIX                = cSim.pAtomX[atom1.x];
            PMEDouble atomJX                = cSim.pAtomX[atom1.y];
            PMEDouble atomKX                = cSim.pAtomX[atom2];
            PMEDouble atomIY                = cSim.pAtomY[atom1.x];
            PMEDouble atomJY                = cSim.pAtomY[atom1.y];
            PMEDouble atomKY                = cSim.pAtomY[atom2];
            PMEDouble atomIZ                = cSim.pAtomZ[atom1.x];
            PMEDouble atomJZ                = cSim.pAtomZ[atom1.y];
            PMEDouble atomKZ                = cSim.pAtomZ[atom2];
#endif
            PMEDouble xij                   = atomIX - atomJX;
            PMEDouble xkj                   = atomKX - atomJX;
            PMEDouble yij                   = atomIY - atomJY;
            PMEDouble ykj                   = atomKY - atomJY;
#else
            int2 iatomIX                    = tex1Dfetch(texref, atom1.x);
            int2 iatomJX                    = tex1Dfetch(texref, atom1.y);
            int2 iatomKX                    = tex1Dfetch(texref, atom2);
            int2 iatomIY                    = tex1Dfetch(texref, atom1.x + cSim.stride);
            int2 iatomJY                    = tex1Dfetch(texref, atom1.y + cSim.stride);
            int2 iatomKY                    = tex1Dfetch(texref, atom2 + cSim.stride);
            int2 iatomIZ                    = tex1Dfetch(texref, atom1.x + cSim.stride2);
            int2 iatomJZ                    = tex1Dfetch(texref, atom1.y + cSim.stride2);
            int2 iatomKZ                    = tex1Dfetch(texref, atom2 + cSim.stride2);
            PMEDouble atomIX                = __hiloint2double(iatomIX.y, iatomIX.x);
            PMEDouble atomJX                = __hiloint2double(iatomJX.y, iatomJX.x);
            PMEDouble atomKX                = __hiloint2double(iatomKX.y, iatomKX.x);
            PMEDouble xij                   = atomIX - atomJX;
            PMEDouble xkj                   = atomKX - atomJX;
            PMEDouble atomIY                = __hiloint2double(iatomIY.y, iatomIY.x);
            PMEDouble atomJY                = __hiloint2double(iatomJY.y, iatomJY.x);
            PMEDouble atomKY                = __hiloint2double(iatomKY.y, iatomKY.x);
            PMEDouble yij                   = atomIY - atomJY;
            PMEDouble ykj                   = atomKY - atomJY;
            PMEDouble atomIZ                = __hiloint2double(iatomIZ.y, iatomIZ.x);
            PMEDouble atomJZ                = __hiloint2double(iatomJZ.y, iatomJZ.x);
            PMEDouble atomKZ                = __hiloint2double(iatomKZ.y, iatomKZ.x);
#endif
            PMEDouble zij                   = atomIZ - atomJZ;
            PMEDouble zkj                   = atomKZ - atomJZ;
            PMEDouble rij2                  = xij * xij + yij * yij + zij * zij;
            PMEDouble rkj2                  = xkj * xkj + ykj * ykj + zkj * zkj;
            PMEDouble rij                   = sqrt(rij2);
            PMEDouble rkj                   = sqrt(rkj2);
            PMEDouble rdenom                = rij * rkj;
            PMEDouble cst                   = min(1.0, max(-1.0, (xij * xkj + yij * ykj + zij * zkj) / rdenom));
            PMEDouble theta                 = acos(cst);

            PMEDouble df;
#ifdef NMR_ENERGY
            PMEDouble e;
#endif
            if (theta < R1R2.x)
            {
                PMEDouble dif               = R1R2.x - R1R2.y;
                df                          = (PMEDouble)2.0 * K2K3.x * dif;
#ifdef NMR_ENERGY
                e                           = df * (theta - R1R2.x) + K2K3.x * dif * dif;
#endif
            }
            else if (theta < R1R2.y)
            {
                PMEDouble dif               = theta - R1R2.y;
                df                          = (PMEDouble)2.0 * K2K3.x * dif;
#ifdef NMR_ENERGY
                e                           = K2K3.x * dif * dif;
#endif
            }
            else if (theta < R3R4.x)
            {
                df                          = (PMEDouble)0.0;
#ifdef NMR_ENERGY
                e                           = (PMEDouble)0.0;
#endif
            }
            else if (theta < R3R4.y)
            {
                PMEDouble dif               = theta - R3R4.x;
                df                          = (PMEDouble)2.0 * K2K3.y * dif;
#ifdef NMR_ENERGY
                e                           = K2K3.y * dif * dif;
#endif
            }
            else
            {
                PMEDouble dif               = R3R4.y - R3R4.x;
                df                          = (PMEDouble)2.0 * K2K3.y * dif;
#ifdef NMR_ENERGY
                e                           = df * (theta - R3R4.y) + K2K3.y * dif * dif;
#endif
            }

            if (cSim.bJar)
            {
                double fold                 = cSim.pNMRJarData[2];
                double work                 = cSim.pNMRJarData[3];
                double first                = cSim.pNMRJarData[4];
                double fcurr                = (PMEDouble)-2.0 * K2K3.x * (theta - R1R2.y);
                if (first == (PMEDouble)0.0)
                {
                    fold                    = -fcurr;
                    cSim.pNMRJarData[4]     = (PMEDouble)1.0;
                }
                work                       += (PMEDouble)0.5 * (fcurr + fold) * cSim.drjar;
                cSim.pNMRJarData[0]         = R1R2.y;
                cSim.pNMRJarData[1]         = theta;
                cSim.pNMRJarData[2]         = fcurr;
                cSim.pNMRJarData[3]         = work;
            }

#ifdef NMR_ENERGY
            eangle                         += llrint(ENERGYSCALE * e);
#endif

            PMEDouble snt                   = sin(theta);
            if (abs(snt) < (PMEDouble)1.0e-14)
                snt                         = (PMEDouble)1.0e-14;
            PMEDouble st                    = -df / snt;
            PMEDouble sth                   = st * cst;
            PMEDouble cik                   = st / rdenom;
            PMEDouble cii                   = sth / rij2;
            PMEDouble ckk                   = sth / rkj2;
            PMEDouble fx1                   = cii * xij - cik * xkj;
            PMEDouble fy1                   = cii * yij - cik * ykj;
            PMEDouble fz1                   = cii * zij - cik * zkj;
            PMEDouble fx2                   = ckk * xkj - cik * xij;
            PMEDouble fy2                   = ckk * ykj - cik * yij;
            PMEDouble fz2                   = ckk * zkj - cik * zij;
            PMEAccumulator ifx1             = llrint(fx1 * FORCESCALE);
            PMEAccumulator ify1             = llrint(fy1 * FORCESCALE);
            PMEAccumulator ifz1             = llrint(fz1 * FORCESCALE);
            PMEAccumulator ifx2             = llrint(fx2 * FORCESCALE);
            PMEAccumulator ify2             = llrint(fy2 * FORCESCALE);
            PMEAccumulator ifz2             = llrint(fz2 * FORCESCALE);
            atomicAdd((unsigned long long int*)&cSim.pBondedForceXAccumulator[atom1.x], llitoulli(ifx1));
            atomicAdd((unsigned long long int*)&cSim.pBondedForceYAccumulator[atom1.x], llitoulli(ify1));
            atomicAdd((unsigned long long int*)&cSim.pBondedForceZAccumulator[atom1.x], llitoulli(ifz1));
            atomicAdd((unsigned long long int*)&cSim.pBondedForceXAccumulator[atom2], llitoulli(ifx2));
            atomicAdd((unsigned long long int*)&cSim.pBondedForceYAccumulator[atom2], llitoulli(ify2));
            atomicAdd((unsigned long long int*)&cSim.pBondedForceZAccumulator[atom2], llitoulli(ifz2));
            atomicAdd((unsigned long long int*)&cSim.pBondedForceXAccumulator[atom1.y], llitoulli(-ifx1 - ifx2));
            atomicAdd((unsigned long long int*)&cSim.pBondedForceYAccumulator[atom1.y], llitoulli(-ify1 - ify2));
            atomicAdd((unsigned long long int*)&cSim.pBondedForceZAccumulator[atom1.y], llitoulli(-ifz1 - ifz2));
        }
exit2:
        {}
#ifdef NMR_ENERGY
        sE[threadIdx.x]                     = eangle;
        sE[threadIdx.x]                    += sE[threadIdx.x ^ 1];
        sE[threadIdx.x]                    += sE[threadIdx.x ^ 2];
        sE[threadIdx.x]                    += sE[threadIdx.x ^ 4];
        sE[threadIdx.x]                    += sE[threadIdx.x ^ 8];
        sE[threadIdx.x]                    += sE[threadIdx.x ^ 16];
        if ((threadIdx.x & GRIDBITSMASK) == 0)
        {
            atomicAdd(cSim.pENMRAngle, llitoulli(sE[threadIdx.x]));
        }
#endif
    }

    // Calculate torsion restraints
    else if (pos < cSim.NMRTorsionOffset)
    {
        pos                                -= cSim.NMRAngleOffset;
#ifdef NMR_ENERGY
        PMEAccumulator etorsion             = 0;
#endif
        if (pos < cSim.NMRTorsions)
        {

#ifdef NMR_NEIGHBORLIST
            int4 atom1                      = cSim.pImageNMRTorsionID1[pos];
#else
            int4 atom1                      = cSim.pNMRTorsionID1[pos];
#endif
            PMEDouble2 R1R2;
            PMEDouble2 R3R4;
            PMEDouble2 K2K3;
#ifdef NMR_TIMED
            int2 Step                       = cSim.pNMRTorsionStep[pos];
            int Inc                         = cSim.pNMRTorsionInc[pos];

            // Skip restraint if not active
            if ((step < Step.x) || ((step > Step.y) && (Step.y > 0)))
                goto exit3;

            // Read timed data
            PMEDouble2 R1R2Slp              = cSim.pNMRTorsionR1R2Slp[pos];
            PMEDouble2 R1R2Int              = cSim.pNMRTorsionR1R2Int[pos];
            PMEDouble2 R3R4Slp              = cSim.pNMRTorsionR3R4Slp[pos];
            PMEDouble2 R3R4Int              = cSim.pNMRTorsionR3R4Int[pos];
            PMEDouble2 K2K3Slp              = cSim.pNMRTorsionK2K3Slp[pos];
            PMEDouble2 K2K3Int              = cSim.pNMRTorsionK2K3Int[pos];

            // Calculate increment
            double dstep                    = step - (double)((step - Step.x) % abs(Inc));

            // Calculate restraint values
            R1R2.x                          = R1R2Slp.x * dstep + R1R2Int.x;
            R1R2.y                          = R1R2Slp.y * dstep + R1R2Int.y;
            R3R4.x                          = R3R4Slp.x * dstep + R3R4Int.x;
            R3R4.y                          = R3R4Slp.y * dstep + R3R4Int.y;
            if (Inc > 0)
            {
                K2K3.x                      = K2K3Slp.x * dstep + K2K3Int.x;
                K2K3.y                      = K2K3Slp.y * dstep + K2K3Int.y;
            }
            else
            {
                int nstepu                  = (step - Step.x) / abs(Inc);
                K2K3.x                      = K2K3Int.x * pow(K2K3Slp.x, nstepu);
                K2K3.y                      = K2K3Int.y * pow(K2K3Slp.y, nstepu);
            }
#else
            R1R2                            = cSim.pNMRTorsionR1R2[pos];
            R3R4                            = cSim.pNMRTorsionR3R4[pos];
            K2K3                            = cSim.pNMRTorsionK2K3[pos];
#endif

#if defined(NODPTEXTURE)
#ifdef NMR_NEIGHBORLIST
            PMEDouble atomIX                = cSim.pImageX[atom1.x];
            PMEDouble atomJX                = cSim.pImageX[atom1.y];
            PMEDouble atomIY                = cSim.pImageY[atom1.x];
            PMEDouble atomJY                = cSim.pImageY[atom1.y];
            PMEDouble atomIZ                = cSim.pImageZ[atom1.x];
            PMEDouble atomJZ                = cSim.pImageZ[atom1.y];
            PMEDouble atomKX                = cSim.pImageX[atom1.z];
            PMEDouble atomKY                = cSim.pImageY[atom1.z];
            PMEDouble atomKZ                = cSim.pImageZ[atom1.z];
            PMEDouble atomLX                = cSim.pImageX[atom1.w];
            PMEDouble atomLY                = cSim.pImageY[atom1.w];
            PMEDouble atomLZ                = cSim.pImageZ[atom1.w];
#else
            PMEDouble atomIX                = cSim.pAtomX[atom1.x];
            PMEDouble atomJX                = cSim.pAtomX[atom1.y];
            PMEDouble atomIY                = cSim.pAtomY[atom1.x];
            PMEDouble atomJY                = cSim.pAtomY[atom1.y];
            PMEDouble atomIZ                = cSim.pAtomZ[atom1.x];
            PMEDouble atomJZ                = cSim.pAtomZ[atom1.y];
            PMEDouble atomKX                = cSim.pAtomX[atom1.z];
            PMEDouble atomKY                = cSim.pAtomY[atom1.z];
            PMEDouble atomKZ                = cSim.pAtomZ[atom1.z];
            PMEDouble atomLX                = cSim.pAtomX[atom1.w];
            PMEDouble atomLY                = cSim.pAtomY[atom1.w];
            PMEDouble atomLZ                = cSim.pAtomZ[atom1.w];
#endif
            PMEDouble xij                   = atomIX - atomJX;
            PMEDouble yij                   = atomIY - atomJY;
            PMEDouble zij                   = atomIZ - atomJZ;
            PMEDouble xkj                   = atomKX - atomJX;
            PMEDouble ykj                   = atomKY - atomJY;
            PMEDouble zkj                   = atomKZ - atomJZ;
            PMEDouble ykl                   = atomKY - atomLY;
#else
            int2 iatomIX                    = tex1Dfetch(texref, atom1.x);
            int2 iatomJX                    = tex1Dfetch(texref, atom1.y);
            int2 iatomKX                    = tex1Dfetch(texref, atom1.z);
            int2 iatomLX                    = tex1Dfetch(texref, atom1.w);
            int2 iatomIY                    = tex1Dfetch(texref, atom1.x + cSim.stride);
            int2 iatomJY                    = tex1Dfetch(texref, atom1.y + cSim.stride);
            int2 iatomKY                    = tex1Dfetch(texref, atom1.z + cSim.stride);
            int2 iatomLY                    = tex1Dfetch(texref, atom1.w + cSim.stride);
            int2 iatomIZ                    = tex1Dfetch(texref, atom1.x + cSim.stride2);
            int2 iatomJZ                    = tex1Dfetch(texref, atom1.y + cSim.stride2);
            int2 iatomKZ                    = tex1Dfetch(texref, atom1.z + cSim.stride2);
            int2 iatomLZ                    = tex1Dfetch(texref, atom1.w + cSim.stride2);
            PMEDouble atomIY                = __hiloint2double(iatomIY.y, iatomIY.x);
            PMEDouble atomJY                = __hiloint2double(iatomJY.y, iatomJY.x);
            PMEDouble yij                   = atomIY - atomJY;
            PMEDouble atomKZ                = __hiloint2double(iatomKZ.y, iatomKZ.x);
            PMEDouble atomJZ                = __hiloint2double(iatomJZ.y, iatomJZ.x);
            PMEDouble zkj                   = atomKZ - atomJZ;
            PMEDouble atomIZ                = __hiloint2double(iatomIZ.y, iatomIZ.x);
            PMEDouble zij                   = atomIZ - atomJZ;
            PMEDouble atomKX                = __hiloint2double(iatomKX.y, iatomKX.x);
            PMEDouble atomJX                = __hiloint2double(iatomJX.y, iatomJX.x);
            PMEDouble xkj                   = atomKX - atomJX;
            PMEDouble atomIX                = __hiloint2double(iatomIX.y, iatomIX.x);
            PMEDouble xij                   = atomIX - atomJX;
            PMEDouble atomKY                = __hiloint2double(iatomKY.y, iatomKY.x);
            PMEDouble ykj                   = atomKY - atomJY;
            PMEDouble atomLY                = __hiloint2double(iatomLY.y, iatomLY.x);
            PMEDouble ykl                   = atomKY - atomLY;
            PMEDouble atomLX                = __hiloint2double(iatomLX.y, iatomLX.x);
            PMEDouble atomLZ                = __hiloint2double(iatomLZ.y, iatomLZ.x);
#endif
            PMEDouble zkl                   = atomKZ - atomLZ;
            PMEDouble xkl                   = atomKX - atomLX;

            // Calculate ij X jk AND kl X jk:
            PMEDouble dx                    = yij * zkj - zij * ykj;
            PMEDouble dy                    = zij * xkj - xij * zkj;
            PMEDouble dz                    = xij * ykj - yij * xkj;

            PMEDouble gx                    = zkj * ykl - ykj * zkl;
            PMEDouble gy                    = xkj * zkl - zkj * xkl;
            PMEDouble gz                    = ykj * xkl - xkj * ykl;

            // Calculate the magnitudes of above vectors, and their dot product:
            PMEDouble fxi                   = dx * dx + dy * dy + dz * dz + tm24;
            PMEDouble fyi                   = gx * gx + gy * gy + gz * gz + tm24;
            PMEDouble ct                    = dx * gx + dy * gy + dz * gz;

            // Branch if linear dihedral:
            PMEDouble z1                    = rsqrt(fxi);
            PMEDouble z2                    = rsqrt(fyi);
            PMEDouble z11                   = z1 * z1;
            PMEDouble z22                   = z2 * z2;
            PMEDouble z12                   = z1 * z2;
            ct                             *= z1 * z2;
            ct                              = max((PMEDouble)-0.9999999999999, min(ct, (PMEDouble)0.9999999999999));
            PMEDouble ap                    = acos(ct);

            PMEDouble s                     = xkj * (dz * gy - dy * gz) + ykj * (dx * gz - dz * gx) + zkj * (dy * gx - dx * gy);
            if (s < (PMEDouble)0.0)
                ap = -ap;
            ap                              = PI - ap;
            PMEDouble sphi, cphi;
            faster_sincos(ap, &sphi, &cphi);

            // Translate the value of the torsion (by +- n*360) to bring it as close as
            // possible to one of the two central "cutoff" points (r2,r3). Use this as
            // the value of the torsion in the following comparison.
            PMEDouble apmean = (R1R2.y + R3R4.x) * (PMEDouble)0.5;
            if (ap - apmean > PI)
                ap                         -= (PMEDouble)2.0 * (PMEDouble)PI;
            if (apmean - ap > PI)
                ap                         += (PMEDouble)2.0 * (PMEDouble)PI;

            PMEDouble df;
#ifdef NMR_ENERGY
            PMEDouble e;
#endif
            if (ap < R1R2.x)
            {
                PMEDouble dif               = R1R2.x - R1R2.y;
                df                          = (PMEDouble)2.0 * K2K3.x * dif;
#ifdef NMR_ENERGY
                e                           = df * (ap - R1R2.x) + K2K3.x * dif * dif;
#endif
            }
            else if (ap < R1R2.y)
            {
                PMEDouble dif               = ap - R1R2.y;
                df                          = (PMEDouble)2.0 * K2K3.x * dif;
#ifdef NMR_ENERGY
                e                           = K2K3.x * dif * dif;
#endif
            }
            else if (ap < R3R4.x)
            {
                df                          = (PMEDouble)0.0;
#ifdef NMR_ENERGY
                e                           = (PMEDouble)0.0;
#endif
            }
            else if (ap < R3R4.y)
            {
                PMEDouble dif               = ap - R3R4.x;
                df                          = (PMEDouble)2.0 * K2K3.y * dif;
#ifdef NMR_ENERGY
                e                           = K2K3.y * dif * dif;
#endif
            }
            else
            {
                PMEDouble dif               = R3R4.y - R3R4.x;
                df                          = (PMEDouble)2.0 * K2K3.y * dif;
#ifdef NMR_ENERGY
                e                           = df * (ap - R3R4.y) + K2K3.y * dif * dif;
#endif
            }
            df                             *= -1.0 / sphi;

            if (cSim.bJar)
            {
                double fold                 = cSim.pNMRJarData[2];
                double work                 = cSim.pNMRJarData[3];
                double first                = cSim.pNMRJarData[4];
                double fcurr                = (PMEDouble)-2.0 * K2K3.x * (ap - R1R2.y);
                if (first == 0.0) {
                    fold                    = -fcurr;
                    cSim.pNMRJarData[4]     = (PMEDouble)1.0;
                }
                work                       += (PMEDouble)0.5 * (fcurr + fold) * cSim.drjar;
                cSim.pNMRJarData[0]         = R1R2.y;
                cSim.pNMRJarData[1]         = ap;
                cSim.pNMRJarData[2]         = fcurr;
                cSim.pNMRJarData[3]         = work;
            }


#ifdef NMR_ENERGY
            etorsion                        = llrint(ENERGYSCALE * e);
#endif




            // dc = First derivative of cos(phi) w/respect to the cartesian differences t.
            PMEDouble dcdx                  =-gx * z12 - cphi * dx * z11;
            PMEDouble dcdy                  =-gy * z12 - cphi * dy * z11;
            PMEDouble dcdz                  =-gz * z12 - cphi * dz * z11;
            PMEDouble dcgx                  = dx * z12 + cphi * gx * z22;
            PMEDouble dcgy                  = dy * z12 + cphi * gy * z22;
            PMEDouble dcgz                  = dz * z12 + cphi * gz * z22;
            PMEDouble dr1                   = df * ( dcdz * ykj - dcdy * zkj);
            PMEDouble dr2                   = df * ( dcdx * zkj - dcdz * xkj);
            PMEDouble dr3                   = df * ( dcdy * xkj - dcdx * ykj);
            PMEDouble dr4                   = df * ( dcgz * ykj - dcgy * zkj);
            PMEDouble dr5                   = df * ( dcgx * zkj - dcgz * xkj);
            PMEDouble dr6                   = df * ( dcgy * xkj - dcgx * ykj);
            PMEDouble drx                   = df * (-dcdy * zij + dcdz * yij + dcgy * zkl -  dcgz * ykl);
            PMEDouble dry                   = df * ( dcdx * zij - dcdz * xij - dcgx * zkl +  dcgz * xkl);
            PMEDouble drz                   = df * (-dcdx * yij + dcdy * xij + dcgx * ykl -  dcgy * xkl);
            PMEAccumulator idr1             = llrint(dr1 * FORCESCALE);
            PMEAccumulator idr2             = llrint(dr2 * FORCESCALE);
            PMEAccumulator idr3             = llrint(dr3 * FORCESCALE);
            PMEAccumulator idr4             = llrint(dr4 * FORCESCALE);
            PMEAccumulator idr5             = llrint(dr5 * FORCESCALE);
            PMEAccumulator idr6             = llrint(dr6 * FORCESCALE);
            PMEAccumulator idrx             = llrint(drx * FORCESCALE);
            PMEAccumulator idry             = llrint(dry * FORCESCALE);
            PMEAccumulator idrz             = llrint(drz * FORCESCALE);
            atomicAdd((unsigned long long int*)&cSim.pBondedForceXAccumulator[atom1.x], llitoulli(-idr1));
            atomicAdd((unsigned long long int*)&cSim.pBondedForceYAccumulator[atom1.x], llitoulli(-idr2));
            atomicAdd((unsigned long long int*)&cSim.pBondedForceZAccumulator[atom1.x], llitoulli(-idr3));
            atomicAdd((unsigned long long int*)&cSim.pBondedForceXAccumulator[atom1.y], llitoulli(-idrx + idr1));
            atomicAdd((unsigned long long int*)&cSim.pBondedForceYAccumulator[atom1.y], llitoulli(-idry + idr2));
            atomicAdd((unsigned long long int*)&cSim.pBondedForceZAccumulator[atom1.y], llitoulli(-idrz + idr3));
            atomicAdd((unsigned long long int*)&cSim.pBondedForceXAccumulator[atom1.z], llitoulli(idrx + idr4));
            atomicAdd((unsigned long long int*)&cSim.pBondedForceYAccumulator[atom1.z], llitoulli(idry + idr5));
            atomicAdd((unsigned long long int*)&cSim.pBondedForceZAccumulator[atom1.z], llitoulli(idrz + idr6));
            atomicAdd((unsigned long long int*)&cSim.pBondedForceXAccumulator[atom1.w], llitoulli(-idr4));
            atomicAdd((unsigned long long int*)&cSim.pBondedForceYAccumulator[atom1.w], llitoulli(-idr5));
            atomicAdd((unsigned long long int*)&cSim.pBondedForceZAccumulator[atom1.w], llitoulli(-idr6));
        }
exit3:
        {}
#ifdef NMR_ENERGY
        sE[threadIdx.x]                     = etorsion;
        sE[threadIdx.x]                    += sE[threadIdx.x ^ 1];
        sE[threadIdx.x]                    += sE[threadIdx.x ^ 2];
        sE[threadIdx.x]                    += sE[threadIdx.x ^ 4];
        sE[threadIdx.x]                    += sE[threadIdx.x ^ 8];
        sE[threadIdx.x]                    += sE[threadIdx.x ^ 16];
        if ((threadIdx.x & GRIDBITSMASK) == 0)
        {
            atomicAdd(cSim.pENMRTorsion, llitoulli(sE[threadIdx.x]));
        }
#endif
    }
    // Calculate COM distance restraints
    else if (pos < cSim.NMRCOMDistanceOffset)
    {
#ifdef NMR_ENERGY
        PMEAccumulator eCOMdistance            = 0;
#endif
        pos                                -= cSim.NMRTorsionOffset;
        if (pos < cSim.NMRCOMDistances)
        {
#ifdef NMR_NEIGHBORLIST
            int2 atom                       = cSim.pImageNMRCOMDistanceID[pos];
#else
            int2 atom                       = cSim.pNMRCOMDistanceID[pos];
#endif
 	    PMEDouble2 rmstot;
	    rmstot.x = 0.0;
	    rmstot.y = 0.0;
            PMEDouble2 R1R2;
            PMEDouble2 R3R4;
            PMEDouble2 K2K3;
#ifdef NMR_TIMED
            int2 Step                       = cSim.pNMRCOMDistanceStep[pos];
            int Inc                         = cSim.pNMRCOMDistanceInc[pos];
            // Skip restraint if not active
            if ((step < Step.x) || ((step > Step.y) && (Step.y > 0)))
                goto exit4;

            // Read timed data
            PMEDouble2 R1R2Slp              = cSim.pNMRCOMDistanceR1R2Slp[pos];
            PMEDouble2 R1R2Int              = cSim.pNMRCOMDistanceR1R2Int[pos];
            PMEDouble2 R3R4Slp              = cSim.pNMRCOMDistanceR3R4Slp[pos];
            PMEDouble2 R3R4Int              = cSim.pNMRCOMDistanceR3R4Int[pos];
            PMEDouble2 K2K3Slp              = cSim.pNMRCOMDistanceK2K3Slp[pos];
            PMEDouble2 K2K3Int              = cSim.pNMRCOMDistanceK2K3Int[pos];

            // Calculate increment
            double dstep                    = step - (double)((step - Step.x) % abs(Inc));

            // Calculate restraint values
            R1R2.x                          = R1R2Slp.x * dstep + R1R2Int.x;
            R1R2.y                          = R1R2Slp.y * dstep + R1R2Int.y;
            R3R4.x                          = R3R4Slp.x * dstep + R3R4Int.x;
            R3R4.y                          = R3R4Slp.y * dstep + R3R4Int.y;
            if (Inc > 0)
            {
                K2K3.x                      = K2K3Slp.x * dstep + K2K3Int.x;
                K2K3.y                      = K2K3Slp.y * dstep + K2K3Int.y;
            }
            else
            {
                int nstepu                  = (step - Step.x) / abs(Inc);
                K2K3.x                      = K2K3Int.x * pow(K2K3Slp.x, nstepu);
                K2K3.y                      = K2K3Int.y * pow(K2K3Slp.y, nstepu);
            }
#else
            R1R2                            = cSim.pNMRCOMDistanceR1R2[pos];
            R3R4                            = cSim.pNMRCOMDistanceR3R4[pos];
            K2K3                            = cSim.pNMRCOMDistanceK2K3[pos];
#endif

 //           printf("%12.5f %12.5f %12.5f %12.5f %12.5f %12.5f\n", R1R2.x, R1R2.y, R3R4.x, R3R4.y, K2K3.x, K2K3.y);
            	PMEDouble atomIX;
            	PMEDouble atomIY;
            	PMEDouble atomIZ;
            	PMEDouble atomJX;
            	PMEDouble atomJY;
            	PMEDouble atomJZ;

#if defined(NODPTEXTURE)
#ifdef NMR_NEIGHBORLIST
	    if (atom.x > 0)
	    {
            	atomIX            = cSim.pImageX[atom.x];
            	atomIY            = cSim.pImageY[atom.x];
            	atomIZ            = cSim.pImageZ[atom.x];
	    }
	    else
	    {
//	Find COM
		PMEDouble xtot              = 0.0;
		PMEDouble ytot              = 0.0;
		PMEDouble ztot              = 0.0;
		int2 COMgrps                = cSim.pImageNMRCOMDistanceCOMGrp[pos * 2];

	        for (int ip = COMgrps.x; ip < COMgrps.y; ip++)
		{
		     int2 COMatms           = cSim.pImageNMRCOMDistanceCOM[ip];

		     for (int j = COMatms.x; j < COMatms.y + 1; j++ )
		     {
			PMEDouble rmass     = cSim.pImageMass[j];
			xtot                = xtot + cSim.pImageX[j] * rmass;
			ytot                = ytot + cSim.pImageY[j] * rmass;
			ztot                = ztot + cSim.pImageZ[j] * rmass;
			rmstot.x            = rmstot.x + rmass;
		     }
                }
		atomIX            = xtot / rmstot.x;
                atomIY            = ytot / rmstot.x;
                atomIZ            = ztot / rmstot.x;
	    }
	    if (atom.y > 0)
	    {
            	atomJX                = cSim.pImageX[atom.y];
            	atomJY                = cSim.pImageY[atom.y];
            	atomJZ                = cSim.pImageZ[atom.y];
	    }
	    else
	    {
//	Find COM
		PMEDouble xtot              = 0.0;
		PMEDouble ytot              = 0.0;
		PMEDouble ztot              = 0.0;
                rmstot.y                    = 0.0;
		int2 COMgrps                = cSim.pImageNMRCOMDistanceCOMGrp[pos * 2 + 1];

	        for (int ip = COMgrps.x; ip < COMgrps.y; ip++)
		{
		     int2 COMatms           = cSim.pImageNMRCOMDistanceCOM[ip];

		     for (int j = COMatms.x; j < COMatms.y + 1; j++ )
		     {
			PMEDouble rmass     = cSim.pImageMass[j];
			xtot                = xtot + cSim.pImageX[j] * rmass;
			ytot                = ytot + cSim.pImageY[j] * rmass;
			ztot                = ztot + cSim.pImageZ[j] * rmass;
			rmstot.y            = rmstot.y + rmass;
		     }
                }
		atomJX            = xtot / rmstot.y;
                atomJY            = ytot / rmstot.y;
                atomJZ            = ztot / rmstot.y;
	    }
//		printf("Image: %12.5f %12.5f %12.5f %12.5f %12.5f %12.5f\n", atomIX, atomIY, atomIZ, atomJX, atomJY, atomJZ);
#else
	    if (atom.x > 0)
	    {
            	atomIX            = cSim.pAtomX[atom.x];
            	atomIY            = cSim.pAtomY[atom.x];
            	atomIZ            = cSim.pAtomZ[atom.x];
	    }
	    else
	    {
//	Find COM
		PMEDouble xtot              = 0.0;
		PMEDouble ytot              = 0.0;
		PMEDouble ztot              = 0.0;
                rmstot.x                    = 0.0;
		int2 COMgrps                = cSim.pNMRCOMDistanceCOMGrp[pos * 2];

	        for (int ip = COMgrps.x; ip < COMgrps.y; ip++)
		{
		     int2 COMatms           = cSim.pNMRCOMDistanceCOM[ip];

		     for (int j = COMatms.x; j < COMatms.y + 1; j++ )
		     {
			PMEDouble rmass     = cSim.pAtomMass[j];
			xtot                = xtot + cSim.pAtomX[j] * rmass;
			ytot                = ytot + cSim.pAtomY[j] * rmass;
			ztot                = ztot + cSim.pAtomZ[j] * rmass;
			rmstot.x            = rmstot.x + rmass;
		     }
                }
		atomIX            = xtot / rmstot.x;
                atomIY            = ytot / rmstot.x;
                atomIZ            = ztot / rmstot.x;
	    }
	    if (atom.y > 0)
	    {
            atomJX                = cSim.pAtomX[atom.y];
            atomJY                = cSim.pAtomY[atom.y];
            atomJZ                = cSim.pAtomZ[atom.y];
	    }
	    else
	    {
//	Find COM
		PMEDouble xtot              = 0.0;
		PMEDouble ytot              = 0.0;
		PMEDouble ztot              = 0.0;
                rmstot.y                    = 0.0;
		int2 COMgrps                = cSim.pNMRCOMDistanceCOMGrp[pos * 2 + 1];

	        for (int ip = COMgrps.x; ip < COMgrps.y; ip++)
		{
		     int2 COMatms           = cSim.pNMRCOMDistanceCOM[ip];

		     for (int j = COMatms.x; j < COMatms.y + 1; j++ )
		     {
			PMEDouble rmass     = cSim.pAtomMass[j];
			xtot                = xtot + cSim.pAtomX[j] * rmass;
			ytot                = ytot + cSim.pAtomY[j] * rmass;
			ztot                = ztot + cSim.pAtomZ[j] * rmass;
			rmstot.y            = rmstot.y + rmass;
		     }
                }
		atomJX            = xtot / rmstot.y;
                atomJY            = ytot / rmstot.y;
                atomJZ            = ztot / rmstot.y;
	    }
//		printf("Image: %12.5f %12.5f %12.5f %12.5f %12.5f %12.5f\n", atomIX, atomIY, atomIZ, atomJX, atomJY, atomJZ);
#endif
#else
	    if (atom.x > 0)
	    {
            	int2 iatomIX                    = tex1Dfetch(texref, atom.x);
            	int2 iatomIY                    = tex1Dfetch(texref, atom.x + cSim.stride);
            	int2 iatomIZ                    = tex1Dfetch(texref, atom.x + cSim.stride2);

            	atomIX                = __hiloint2double(iatomIX.y, iatomIX.x);
            	atomIY                = __hiloint2double(iatomIY.y, iatomIY.x);
            	atomIZ                = __hiloint2double(iatomIZ.y, iatomIZ.x);
	    }
	    else
	    {
//	Find COM
		PMEDouble xtot              = 0.0;
		PMEDouble ytot              = 0.0;
		PMEDouble ztot              = 0.0;
                rmstot.x                    = 0.0;
#ifdef NMR_NEIGHBORLIST
		int2 COMgrps                = cSim.pImageNMRCOMDistanceCOMGrp[pos * 2];
#else
		int2 COMgrps                = cSim.pNMRCOMDistanceCOMGrp[pos * 2];
#endif
	        for (int ip = COMgrps.x; ip < COMgrps.y; ip++)
		{
#ifdef NMR_NEIGHBORLIST
		     int2 COMatms           = cSim.pImageNMRCOMDistanceCOM[ip];
#else
		     int2 COMatms           = cSim.pNMRCOMDistanceCOM[ip];
#endif
		     for (int j = COMatms.x; j < COMatms.y + 1; j++ )
		     {
#ifdef NMR_NEIGHBORLIST
			PMEDouble rmass     = cSim.pImageMass[j];
#else
			PMEDouble rmass     = cSim.pAtomMass[j];
#endif
                        int2 iatomIX        = tex1Dfetch(texref, j);
                        int2 iatomIY        = tex1Dfetch(texref, j + cSim.stride);
                        int2 iatomIZ        = tex1Dfetch(texref, j + cSim.stride2);

                        atomIX    = __hiloint2double(iatomIX.y, iatomIX.x);
                        atomIY    = __hiloint2double(iatomIY.y, iatomIY.x);
                        atomIZ    = __hiloint2double(iatomIZ.y, iatomIZ.x);
			xtot                = xtot + atomIX * rmass;
			ytot                = ytot + atomIY * rmass;
			ztot                = ztot + atomIZ * rmass;
			rmstot.x            = rmstot.x + rmass;
		     }
                }
		atomIX            = xtot / rmstot.x;
                atomIY            = ytot / rmstot.x;
                atomIZ            = ztot / rmstot.x;
	    }

	    if (atom.y > 0)
	    {
            	int2 iatomJX                    = tex1Dfetch(texref, atom.y);
            	int2 iatomJY                    = tex1Dfetch(texref, atom.y + cSim.stride);
            	int2 iatomJZ                    = tex1Dfetch(texref, atom.y + cSim.stride2);

            	atomJX                = __hiloint2double(iatomJX.y, iatomJX.x);
            	atomJY                = __hiloint2double(iatomJY.y, iatomJY.x);
            	atomJZ                = __hiloint2double(iatomJZ.y, iatomJZ.x);
	    }
	    else
	    {
//	Find COM
		PMEDouble xtot              = 0.0;
		PMEDouble ytot              = 0.0;
		PMEDouble ztot              = 0.0;
                rmstot.y                    = 0.0;
#ifdef NMR_NEIGHBORLIST
		int2 COMgrps                = cSim.pImageNMRCOMDistanceCOMGrp[pos * 2 + 1];
#else
		int2 COMgrps                = cSim.pNMRCOMDistanceCOMGrp[pos * 2 + 1];
#endif
	        for (int ip = COMgrps.x; ip < COMgrps.y; ip++)
		{
#ifdef NMR_NEIGHBORLIST
		     int2 COMatms           = cSim.pImageNMRCOMDistanceCOM[ip];
#else
		     int2 COMatms           = cSim.pNMRCOMDistanceCOM[ip];
#endif
		     for (int j = COMatms.x; j < COMatms.y + 1; j++ )
		     {
#ifdef NMR_NEIGHBORLIST
			PMEDouble rmass     = cSim.pImageMass[j];
#else
			PMEDouble rmass     = cSim.pAtomMass[j];
#endif
                        int2 iatomJX        = tex1Dfetch(texref, j);
                        int2 iatomJY        = tex1Dfetch(texref, j + cSim.stride);
                        int2 iatomJZ        = tex1Dfetch(texref, j + cSim.stride2);

                        atomJX    = __hiloint2double(iatomJX.y, iatomJX.x);
                        atomJY    = __hiloint2double(iatomJY.y, iatomJY.x);
                        atomJZ    = __hiloint2double(iatomJZ.y, iatomJZ.x);
			xtot                = xtot + atomJX * rmass;
			ytot                = ytot + atomJY * rmass;
			ztot                = ztot + atomJZ * rmass;
			rmstot.y            = rmstot.y + rmass;
		     }
                }
		atomJX            = xtot / rmstot.y;
                atomJY            = ytot / rmstot.y;
                atomJZ            = ztot / rmstot.y;
	    }

#endif
            PMEDouble rij;
            PMEDouble xweight               = cSim.pNMRCOMDistanceWeights[pos*5];
            PMEDouble yweight               = cSim.pNMRCOMDistanceWeights[pos*5+1];
            PMEDouble zweight               = cSim.pNMRCOMDistanceWeights[pos*5+2];
            PMEDouble direction             = cSim.pNMRCOMDistanceWeights[pos*5+3]; //0 to 3
            PMEDouble posneg                = cSim.pNMRCOMDistanceWeights[pos*5+4]; //initial neg or pos -1 or 1
//          printf("id: %f xyz: %f   %f   %f  %f %f \n",pos,xweight,yweight,zweight,direction,posneg);
            PMEDouble xij                   = atomIX - atomJX;
            PMEDouble yij                   = atomIY - atomJY;
            PMEDouble zij                   = atomIZ - atomJZ;
            cSim.pNMRCOMDistanceXYZ[pos*3]  = xij;
            cSim.pNMRCOMDistanceXYZ[pos*3+1]= yij;
            cSim.pNMRCOMDistanceXYZ[pos*3+2]= zij;
            if(direction == 0)
                rij               = sqrt(xij * xij * xweight * xweight + yij * yij * yweight * yweight + zij * zij * zweight * zweight);
            else
                rij               = (xij*xweight + yij*yweight + zij*zweight)*posneg;
            PMEDouble df;
#ifdef NMR_ENERGY
            PMEDouble e;
#endif
            if (rij < R1R2.x)
            {
                PMEDouble dif               = R1R2.x - R1R2.y;
                df                          = (PMEDouble)2.0 * K2K3.x * dif;
#ifdef NMR_ENERGY
                e                           = df * (rij - R1R2.x) + K2K3.x * dif * dif;
#endif
            }
            else if (rij < R1R2.y)
            {
                PMEDouble dif               = rij - R1R2.y;
                df                          = (PMEDouble)2.0 * K2K3.x * dif;
#ifdef NMR_ENERGY
                e                           = K2K3.x * dif * dif;
#endif
            }
            else if (rij < R3R4.x)
            {
                df                          = (PMEDouble)0.0;
#ifdef NMR_ENERGY
                e                           = (PMEDouble)0.0;
#endif
            }
            else if (rij < R3R4.y)
            {
                PMEDouble dif               = rij - R3R4.x;
                df                          = (PMEDouble)2.0 * K2K3.y * dif;
#ifdef NMR_ENERGY
                e                           = K2K3.y * dif * dif;
#endif
            }
            else
            {
                PMEDouble dif               = R3R4.y - R3R4.x;
                df                          = (PMEDouble)2.0 * K2K3.y * dif;
#ifdef NMR_ENERGY
                e                           = df * (rij - R3R4.y) + K2K3.y * dif * dif;
#endif
            }

            if (cSim.bJar)
            {
                double fold                 = cSim.pNMRJarData[2];
                double work                 = cSim.pNMRJarData[3];
                double first                = cSim.pNMRJarData[4];
                double fcurr                = (PMEDouble)-2.0 * K2K3.x * (rij - R1R2.y);
                if (first == (PMEDouble)0.0) {
                    fold                    = -fcurr;
                    cSim.pNMRJarData[4]     = (PMEDouble)1.0;
                }
                work                       += (PMEDouble)0.5 * (fcurr + fold) * cSim.drjar;
                cSim.pNMRJarData[0]         = R1R2.y;
                cSim.pNMRJarData[1]         = rij;
                cSim.pNMRJarData[2]         = fcurr;
                cSim.pNMRJarData[3]         = work;
            }
#ifdef NMR_ENERGY
            eCOMdistance                   += llrint(ENERGYSCALE * e);
#endif

            df                             *= (PMEDouble)1.0 / rij;
            PMEDouble dfx                    = df * xij * xweight * xweight;
            PMEDouble dfy                    = df * yij * yweight * yweight;
            PMEDouble dfz                    = df * zij * zweight * zweight;
            PMEAccumulator ifx;
            PMEAccumulator ify;
            PMEAccumulator ifz;
	    if (atom.y > 0)
	    {
            	ifx              = llrint(dfx * FORCESCALE);
            	ify              = llrint(dfy * FORCESCALE);
            	ifz              = llrint(dfz * FORCESCALE);
            	atomicAdd((unsigned long long int*)&cSim.pBondedForceXAccumulator[atom.y], llitoulli(ifx));
            	atomicAdd((unsigned long long int*)&cSim.pBondedForceYAccumulator[atom.y], llitoulli(ify));
            	atomicAdd((unsigned long long int*)&cSim.pBondedForceZAccumulator[atom.y], llitoulli(ifz));
      	    }
	    else
	    {
#ifdef NMR_NEIGHBORLIST
		int2 COMgrps                = cSim.pImageNMRCOMDistanceCOMGrp[pos * 2 + 1];
#else
		int2 COMgrps                = cSim.pNMRCOMDistanceCOMGrp[pos * 2 + 1];
#endif
	        for (int ip = COMgrps.x; ip < COMgrps.y; ip++)
		{
#ifdef NMR_NEIGHBORLIST
		     int2 COMatms           = cSim.pImageNMRCOMDistanceCOM[ip];
#else
		     int2 COMatms           = cSim.pNMRCOMDistanceCOM[ip];
#endif
		     for (int j = COMatms.x; j < COMatms.y + 1; j++ )
	  	     {
#ifdef NMR_NEIGHBORLIST
			PMEDouble rmass     = cSim.pImageMass[j];
#else
			PMEDouble rmass     = cSim.pAtomMass[j];
#endif
			PMEDouble dcomdx    = rmass / rmstot.y;
		        PMEDouble fx                 = dfx * dcomdx;
		        PMEDouble fy                 = dfy * dcomdx;
		        PMEDouble fz                 = dfz * dcomdx;
            	 	ifx              = llrint(fx * FORCESCALE);
            	 	ify              = llrint(fy * FORCESCALE);
            	 	ifz              = llrint(fz * FORCESCALE);
            		atomicAdd((unsigned long long int*)&cSim.pBondedForceXAccumulator[j], llitoulli(ifx));
            		atomicAdd((unsigned long long int*)&cSim.pBondedForceYAccumulator[j], llitoulli(ify));
            		atomicAdd((unsigned long long int*)&cSim.pBondedForceZAccumulator[j], llitoulli(ifz));
		     }
		}
	    }

	    if (atom.x > 0)
	    {
            	ifx              = llrint(dfx * FORCESCALE);
            	ify              = llrint(dfy * FORCESCALE);
            	ifz              = llrint(dfz * FORCESCALE);
            	atomicAdd((unsigned long long int*)&cSim.pBondedForceXAccumulator[atom.x], llitoulli(-ifx));
            	atomicAdd((unsigned long long int*)&cSim.pBondedForceYAccumulator[atom.x], llitoulli(-ify));
            	atomicAdd((unsigned long long int*)&cSim.pBondedForceZAccumulator[atom.x], llitoulli(-ifz));
	    }
	    else
	    {
#ifdef NMR_NEIGHBORLIST
		int2 COMgrps                = cSim.pImageNMRCOMDistanceCOMGrp[pos * 2];
#else
		int2 COMgrps                = cSim.pNMRCOMDistanceCOMGrp[pos * 2];
#endif
	        for (int ip = COMgrps.x; ip < COMgrps.y; ip++)
		{
#ifdef NMR_NEIGHBORLIST
		     int2 COMatms           = cSim.pImageNMRCOMDistanceCOM[ip];
#else
		     int2 COMatms           = cSim.pNMRCOMDistanceCOM[ip];
#endif

		     for (int j = COMatms.x; j < COMatms.y + 1; j++ )
	  	     {
#ifdef NMR_NEIGHBORLIST
			PMEDouble rmass     = cSim.pImageMass[j];
#else
			PMEDouble rmass     = cSim.pAtomMass[j];
#endif
			PMEDouble dcomdx    = rmass / rmstot.x;
		        PMEDouble fx                 = dfx * dcomdx;
		        PMEDouble fy                 = dfy * dcomdx;
		        PMEDouble fz                 = dfz * dcomdx;
            	 	ifx              = llrint(fx * FORCESCALE);
            	 	ify              = llrint(fy * FORCESCALE);
            	 	ifz              = llrint(fz * FORCESCALE);
            	        atomicAdd((unsigned long long int*)&cSim.pBondedForceXAccumulator[j], llitoulli(-ifx));
                 	atomicAdd((unsigned long long int*)&cSim.pBondedForceYAccumulator[j], llitoulli(-ify));
            		atomicAdd((unsigned long long int*)&cSim.pBondedForceZAccumulator[j], llitoulli(-ifz));
		     }
		}

	    }

        }
exit4:
        {}
#ifdef NMR_ENERGY
        sE[threadIdx.x]                     = eCOMdistance;
        sE[threadIdx.x]                    += sE[threadIdx.x ^ 1];
        sE[threadIdx.x]                    += sE[threadIdx.x ^ 2];
        sE[threadIdx.x]                    += sE[threadIdx.x ^ 4];
        sE[threadIdx.x]                    += sE[threadIdx.x ^ 8];
        sE[threadIdx.x]                    += sE[threadIdx.x ^ 16];
        if ((threadIdx.x & GRIDBITSMASK) == 0)
        {
            atomicAdd(cSim.pENMRCOMDistance, llitoulli(sE[threadIdx.x]));
        }
#endif
    }

// BUMP POTENTIAL
// BUMP POTENTIAL

    // Calculate COM distance restraints for bump potential
    else if (pos < cSim.NMRCOMBumpOffset)
    {
#ifdef NMR_ENERGY
        PMEAccumulator eCOMBump            = 0;
#endif
        pos                                -= cSim.NMRCOMDistanceOffset;
        if (pos < cSim.NMRCOMBump)
        {
#ifdef NMR_NEIGHBORLIST
            int2 atom                       = cSim.pImageNMRCOMBumpID[pos];
#else
            int2 atom                       = cSim.pNMRCOMBumpID[pos];
#endif
       PMEDouble2 rmstot;
       rmstot.x = 0.0;
       rmstot.y = 0.0;
            PMEDouble2 R1R2;
            PMEDouble2 R3R4;
            PMEDouble2 K2K3;
#ifdef NMR_TIMED
            int2 Step                       = cSim.pNMRCOMBumpStep[pos];
            int Inc                         = cSim.pNMRCOMBumpInc[pos];
            // Skip restraint if not active
            if ((step < Step.x) || ((step > Step.y) && (Step.y > 0)))
                goto exit5;

            // Read timed data
            PMEDouble2 R1R2Slp              = cSim.pNMRCOMBumpR1R2Slp[pos];
            PMEDouble2 R1R2Int              = cSim.pNMRCOMBumpR1R2Int[pos];
            PMEDouble2 R3R4Slp              = cSim.pNMRCOMBumpR3R4Slp[pos];
            PMEDouble2 R3R4Int              = cSim.pNMRCOMBumpR3R4Int[pos];
            PMEDouble2 K2K3Slp              = cSim.pNMRCOMBumpK2K3Slp[pos];
            PMEDouble2 K2K3Int              = cSim.pNMRCOMBumpK2K3Int[pos];

            // Calculate increment
            double dstep                    = step - (double)((step - Step.x) % abs(Inc));

            // Calculate restraint values
            R1R2.x                          = R1R2Slp.x * dstep + R1R2Int.x;
            R1R2.y                          = R1R2Slp.y * dstep + R1R2Int.y;
            R3R4.x                          = R3R4Slp.x * dstep + R3R4Int.x;
            R3R4.y                          = R3R4Slp.y * dstep + R3R4Int.y;
            if (Inc > 0)
            {
                K2K3.x                      = K2K3Slp.x * dstep + K2K3Int.x;
                K2K3.y                      = K2K3Slp.y * dstep + K2K3Int.y;
            }
            else
            {
                int nstepu                  = (step - Step.x) / abs(Inc);
                K2K3.x                      = K2K3Int.x * pow(K2K3Slp.x, nstepu);
                K2K3.y                      = K2K3Int.y * pow(K2K3Slp.y, nstepu);
            }
#else
            R1R2                            = cSim.pNMRCOMBumpR1R2[pos];
            R3R4                            = cSim.pNMRCOMBumpR3R4[pos];
            K2K3                            = cSim.pNMRCOMBumpK2K3[pos];
#endif

 //           printf("%12.5f %12.5f %12.5f %12.5f %12.5f %12.5f\n", R1R2.x, R1R2.y, R3R4.x, R3R4.y, K2K3.x, K2K3.y);
               PMEDouble atomIX;
               PMEDouble atomIY;
               PMEDouble atomIZ;
               PMEDouble atomJX;
               PMEDouble atomJY;
               PMEDouble atomJZ;

#if defined(NODPTEXTURE)
#ifdef NMR_NEIGHBORLIST
       if (atom.x > 0)
       {
               atomIX            = cSim.pImageX[atom.x];
               atomIY            = cSim.pImageY[atom.x];
               atomIZ            = cSim.pImageZ[atom.x];
       }
       else
       {
// Find COM
      PMEDouble xtot              = 0.0;
      PMEDouble ytot              = 0.0;
      PMEDouble ztot              = 0.0;
      int2 COMgrps                = cSim.pImageNMRCOMBumpCOMGrp[pos * 2];

           for (int ip = COMgrps.x; ip < COMgrps.y; ip++)
      {
           int2 COMatms           = cSim.pImageNMRCOMBumpCOM[ip];

           for (int j = COMatms.x; j < COMatms.y + 1; j++ )
           {
         PMEDouble rmass     = cSim.pImageMass[j];
         xtot                = xtot + cSim.pImageX[j] * rmass;
         ytot                = ytot + cSim.pImageY[j] * rmass;
         ztot                = ztot + cSim.pImageZ[j] * rmass;
         rmstot.x            = rmstot.x + rmass;
           }
                }
      atomIX            = xtot / rmstot.x;
                atomIY            = ytot / rmstot.x;
                atomIZ            = ztot / rmstot.x;
       }
       if (atom.y > 0)
       {
               atomJX                = cSim.pImageX[atom.y];
               atomJY                = cSim.pImageY[atom.y];
               atomJZ                = cSim.pImageZ[atom.y];
       }
       else
       {
// Find COM
      PMEDouble xtot              = 0.0;
      PMEDouble ytot              = 0.0;
      PMEDouble ztot              = 0.0;
                rmstot.y                    = 0.0;
      int2 COMgrps                = cSim.pImageNMRCOMBumpCOMGrp[pos * 2 + 1];

           for (int ip = COMgrps.x; ip < COMgrps.y; ip++)
      {
           int2 COMatms           = cSim.pImageNMRCOMBumpCOM[ip];

           for (int j = COMatms.x; j < COMatms.y + 1; j++ )
           {
         PMEDouble rmass     = cSim.pImageMass[j];
         xtot                = xtot + cSim.pImageX[j] * rmass;
         ytot                = ytot + cSim.pImageY[j] * rmass;
         ztot                = ztot + cSim.pImageZ[j] * rmass;
         rmstot.y            = rmstot.y + rmass;
           }
                }
      atomJX            = xtot / rmstot.y;
                atomJY            = ytot / rmstot.y;
                atomJZ            = ztot / rmstot.y;
       }
//    printf("Image: %12.5f %12.5f %12.5f %12.5f %12.5f %12.5f\n", atomIX, atomIY, atomIZ, atomJX, atomJY, atomJZ);
#else
       if (atom.x > 0)
       {
               atomIX            = cSim.pAtomX[atom.x];
               atomIY            = cSim.pAtomY[atom.x];
               atomIZ            = cSim.pAtomZ[atom.x];
       }
       else
       {
// Find COM
      PMEDouble xtot              = 0.0;
      PMEDouble ytot              = 0.0;
      PMEDouble ztot              = 0.0;
                rmstot.x                    = 0.0;
      int2 COMgrps                = cSim.pNMRCOMBumpCOMGrp[pos * 2];

           for (int ip = COMgrps.x; ip < COMgrps.y; ip++)
      {
           int2 COMatms           = cSim.pNMRCOMBumpCOM[ip];

           for (int j = COMatms.x; j < COMatms.y + 1; j++ )
           {
         PMEDouble rmass     = cSim.pAtomMass[j];
         xtot                = xtot + cSim.pAtomX[j] * rmass;
         ytot                = ytot + cSim.pAtomY[j] * rmass;
         ztot                = ztot + cSim.pAtomZ[j] * rmass;
         rmstot.x            = rmstot.x + rmass;
           }
                }
      atomIX            = xtot / rmstot.x;
                atomIY            = ytot / rmstot.x;
                atomIZ            = ztot / rmstot.x;
       }
       if (atom.y > 0)
       {
            atomJX                = cSim.pAtomX[atom.y];
            atomJY                = cSim.pAtomY[atom.y];
            atomJZ                = cSim.pAtomZ[atom.y];
       }
       else
       {
// Find COM
      PMEDouble xtot              = 0.0;
      PMEDouble ytot              = 0.0;
      PMEDouble ztot              = 0.0;
                rmstot.y                    = 0.0;
      int2 COMgrps                = cSim.pNMRCOMBumpCOMGrp[pos * 2 + 1];

           for (int ip = COMgrps.x; ip < COMgrps.y; ip++)
      {
           int2 COMatms           = cSim.pNMRCOMBumpCOM[ip];

           for (int j = COMatms.x; j < COMatms.y + 1; j++ )
           {
         PMEDouble rmass     = cSim.pAtomMass[j];
         xtot                = xtot + cSim.pAtomX[j] * rmass;
         ytot                = ytot + cSim.pAtomY[j] * rmass;
         ztot                = ztot + cSim.pAtomZ[j] * rmass;
         rmstot.y            = rmstot.y + rmass;
           }
                }
      atomJX            = xtot / rmstot.y;
                atomJY            = ytot / rmstot.y;
                atomJZ            = ztot / rmstot.y;
       }
//    printf("Image: %12.5f %12.5f %12.5f %12.5f %12.5f %12.5f\n", atomIX, atomIY, atomIZ, atomJX, atomJY, atomJZ);
#endif
#else
       if (atom.x > 0)
       {
               int2 iatomIX                    = tex1Dfetch(texref, atom.x);
               int2 iatomIY                    = tex1Dfetch(texref, atom.x + cSim.stride);
               int2 iatomIZ                    = tex1Dfetch(texref, atom.x + cSim.stride2);

               atomIX                = __hiloint2double(iatomIX.y, iatomIX.x);
               atomIY                = __hiloint2double(iatomIY.y, iatomIY.x);
               atomIZ                = __hiloint2double(iatomIZ.y, iatomIZ.x);
       }
       else
       {
// Find COM
      PMEDouble xtot              = 0.0;
      PMEDouble ytot              = 0.0;
      PMEDouble ztot              = 0.0;
                rmstot.x                    = 0.0;
#ifdef NMR_NEIGHBORLIST
      int2 COMgrps                = cSim.pImageNMRCOMBumpCOMGrp[pos * 2];
#else
      int2 COMgrps                = cSim.pNMRCOMBumpCOMGrp[pos * 2];
#endif
           for (int ip = COMgrps.x; ip < COMgrps.y; ip++)
      {
#ifdef NMR_NEIGHBORLIST
           int2 COMatms           = cSim.pImageNMRCOMBumpCOM[ip];
#else
           int2 COMatms           = cSim.pNMRCOMBumpCOM[ip];
#endif
           for (int j = COMatms.x; j < COMatms.y + 1; j++ )
           {
#ifdef NMR_NEIGHBORLIST
         PMEDouble rmass     = cSim.pImageMass[j];
#else
         PMEDouble rmass     = cSim.pAtomMass[j];
#endif
                        int2 iatomIX        = tex1Dfetch(texref, j);
                        int2 iatomIY        = tex1Dfetch(texref, j + cSim.stride);
                        int2 iatomIZ        = tex1Dfetch(texref, j + cSim.stride2);

                        atomIX    = __hiloint2double(iatomIX.y, iatomIX.x);
                        atomIY    = __hiloint2double(iatomIY.y, iatomIY.x);
                        atomIZ    = __hiloint2double(iatomIZ.y, iatomIZ.x);
         xtot                = xtot + atomIX * rmass;
         ytot                = ytot + atomIY * rmass;
         ztot                = ztot + atomIZ * rmass;
         rmstot.x            = rmstot.x + rmass;
           }
                }
      atomIX            = xtot / rmstot.x;
                atomIY            = ytot / rmstot.x;
                atomIZ            = ztot / rmstot.x;
       }

       if (atom.y > 0)
       {
               int2 iatomJX                    = tex1Dfetch(texref, atom.y);
               int2 iatomJY                    = tex1Dfetch(texref, atom.y + cSim.stride);
               int2 iatomJZ                    = tex1Dfetch(texref, atom.y + cSim.stride2);

               atomJX                = __hiloint2double(iatomJX.y, iatomJX.x);
               atomJY                = __hiloint2double(iatomJY.y, iatomJY.x);
               atomJZ                = __hiloint2double(iatomJZ.y, iatomJZ.x);
       }
       else
       {
// Find COM
      PMEDouble xtot              = 0.0;
      PMEDouble ytot              = 0.0;
      PMEDouble ztot              = 0.0;
                rmstot.y                    = 0.0;
#ifdef NMR_NEIGHBORLIST
      int2 COMgrps                = cSim.pImageNMRCOMBumpCOMGrp[pos * 2 + 1];
#else
      int2 COMgrps                = cSim.pNMRCOMBumpCOMGrp[pos * 2 + 1];
#endif
           for (int ip = COMgrps.x; ip < COMgrps.y; ip++)
      {
#ifdef NMR_NEIGHBORLIST
           int2 COMatms           = cSim.pImageNMRCOMBumpCOM[ip];
#else
           int2 COMatms           = cSim.pNMRCOMBumpCOM[ip];
#endif
           for (int j = COMatms.x; j < COMatms.y + 1; j++ )
           {
#ifdef NMR_NEIGHBORLIST
         PMEDouble rmass     = cSim.pImageMass[j];
#else
         PMEDouble rmass     = cSim.pAtomMass[j];
#endif
                        int2 iatomJX        = tex1Dfetch(texref, j);
                        int2 iatomJY        = tex1Dfetch(texref, j + cSim.stride);
                        int2 iatomJZ        = tex1Dfetch(texref, j + cSim.stride2);

                        atomJX    = __hiloint2double(iatomJX.y, iatomJX.x);
                        atomJY    = __hiloint2double(iatomJY.y, iatomJY.x);
                        atomJZ    = __hiloint2double(iatomJZ.y, iatomJZ.x);
         xtot                = xtot + atomJX * rmass;
         ytot                = ytot + atomJY * rmass;
         ztot                = ztot + atomJZ * rmass;
         rmstot.y            = rmstot.y + rmass;
           }
                }
      atomJX            = xtot / rmstot.y;
                atomJY            = ytot / rmstot.y;
                atomJZ            = ztot / rmstot.y;
       }

#endif
            PMEDouble rij;
            PMEDouble xweight               = cSim.pNMRCOMBumpWeights[pos*5];
            PMEDouble yweight               = cSim.pNMRCOMBumpWeights[pos*5+1];
            PMEDouble zweight               = cSim.pNMRCOMBumpWeights[pos*5+2];
            PMEDouble direction             = cSim.pNMRCOMBumpWeights[pos*5+3]; //0 to 3
            PMEDouble posneg                = cSim.pNMRCOMBumpWeights[pos*5+4]; //initial neg or pos -1 or 1
//          printf("id: %f xyz: %f   %f   %f  %f %f \n",pos,xweight,yweight,zweight,direction,posneg);
            PMEDouble xij                   = atomIX - atomJX;
            PMEDouble yij                   = atomIY - atomJY;
            PMEDouble zij                   = atomIZ - atomJZ;
            cSim.pNMRCOMBumpXYZ[pos*3]  = xij;
            cSim.pNMRCOMBumpXYZ[pos*3+1]= yij;
            cSim.pNMRCOMBumpXYZ[pos*3+2]= zij;

            if(direction == 0)
                rij               = sqrt(xij * xij * xweight * xweight + yij * yij * yweight * yweight + zij * zij * zweight * zweight);
            else
                rij               = (xij*xweight + yij*yweight + zij*zweight)*posneg;
            PMEDouble df;
#ifdef NMR_ENERGY
            PMEDouble e;
#endif
            if (rij < R1R2.x)
            {
                df                          = (PMEDouble)0.0;
#ifdef NMR_ENERGY
                e                           = (PMEDouble)0.0;
#endif
            }
            else if (rij <= R1R2.y)
            {

                PMEDouble dif1              = (R1R2.x - R1R2.y) * (R1R2.x - R1R2.y);
                PMEDouble kma               = K2K3.x/(dif1 * dif1);
                PMEDouble dif               = rij - R1R2.y;
                dif1                        = (dif*dif - dif1);
                df                          = (PMEDouble)4.0 * kma * dif1 * dif;
#ifdef NMR_ENERGY
                e                           = kma * dif1 * dif1;
#endif
            }
            else if (rij <= R3R4.x)
            {
                df                          = (PMEDouble)0.0 ;
#ifdef NMR_ENERGY
                e                           = (PMEDouble) K2K3.x;
#endif
            }
            else if (rij <= R3R4.y)
            {
                PMEDouble dif1              = (R3R4.x - R3R4.y)* (R3R4.x - R3R4.y);
                PMEDouble kma               = K2K3.y/(dif1*dif1);
                PMEDouble dif               = rij - R3R4.x;
                dif1                        = (dif*dif - dif1);
                df                          = (PMEDouble)4.0 * kma * dif1 * dif;
#ifdef NMR_ENERGY
                e                           = kma * dif1 * dif1;
#endif
            }
            else
            {
                df                          = (PMEDouble)0.0;
#ifdef NMR_ENERGY
                e                           = (PMEDouble)0.0;
#endif
            }

            if (cSim.bJar)
            {
                double fold                 = cSim.pNMRJarData[2];
                double work                 = cSim.pNMRJarData[3];
                double first                = cSim.pNMRJarData[4];
                double fcurr                = (PMEDouble)-2.0 * K2K3.x * (rij - R1R2.y);
                if (first == (PMEDouble)0.0) {
                    fold                    = -fcurr;
                    cSim.pNMRJarData[4]     = (PMEDouble)1.0;
                }
                work                       += (PMEDouble)0.5 * (fcurr + fold) * cSim.drjar;
                cSim.pNMRJarData[0]         = R1R2.y;
                cSim.pNMRJarData[1]         = rij;
                cSim.pNMRJarData[2]         = fcurr;
                cSim.pNMRJarData[3]         = work;
            }
#ifdef NMR_ENERGY
            eCOMBump                   += llrint(ENERGYSCALE * e);
#endif

            df                             *= (PMEDouble)1.0 / rij;
            PMEDouble dfx                    = df * xij * xweight * xweight;
            PMEDouble dfy                    = df * yij * yweight * yweight;
            PMEDouble dfz                    = df * zij * zweight * zweight;
            PMEAccumulator ifx;
            PMEAccumulator ify;
            PMEAccumulator ifz;
       if (atom.y > 0)
       {
               ifx              = llrint(dfx * FORCESCALE);
               ify              = llrint(dfy * FORCESCALE);
               ifz              = llrint(dfz * FORCESCALE);
               atomicAdd((unsigned long long int*)&cSim.pBondedForceXAccumulator[atom.y], llitoulli(ifx));
               atomicAdd((unsigned long long int*)&cSim.pBondedForceYAccumulator[atom.y], llitoulli(ify));
               atomicAdd((unsigned long long int*)&cSim.pBondedForceZAccumulator[atom.y], llitoulli(ifz));
             }
       else
       {
#ifdef NMR_NEIGHBORLIST
      int2 COMgrps                = cSim.pImageNMRCOMBumpCOMGrp[pos * 2 + 1];
#else
      int2 COMgrps                = cSim.pNMRCOMBumpCOMGrp[pos * 2 + 1];
#endif
           for (int ip = COMgrps.x; ip < COMgrps.y; ip++)
      {
#ifdef NMR_NEIGHBORLIST
           int2 COMatms           = cSim.pImageNMRCOMBumpCOM[ip];
#else
           int2 COMatms           = cSim.pNMRCOMBumpCOM[ip];
#endif
           for (int j = COMatms.x; j < COMatms.y + 1; j++ )
           {
#ifdef NMR_NEIGHBORLIST
         PMEDouble rmass     = cSim.pImageMass[j];
#else
         PMEDouble rmass     = cSim.pAtomMass[j];
#endif
         PMEDouble dcomdx    = rmass / rmstot.y;
              PMEDouble fx                 = dfx * dcomdx;
              PMEDouble fy                 = dfy * dcomdx;
              PMEDouble fz                 = dfz * dcomdx;
                  ifx              = llrint(fx * FORCESCALE);
                  ify              = llrint(fy * FORCESCALE);
                  ifz              = llrint(fz * FORCESCALE);
                  atomicAdd((unsigned long long int*)&cSim.pBondedForceXAccumulator[j], llitoulli(ifx));
                  atomicAdd((unsigned long long int*)&cSim.pBondedForceYAccumulator[j], llitoulli(ify));
                  atomicAdd((unsigned long long int*)&cSim.pBondedForceZAccumulator[j], llitoulli(ifz));
           }
      }
       }

       if (atom.x > 0)
       {
               ifx              = llrint(dfx * FORCESCALE);
               ify              = llrint(dfy * FORCESCALE);
               ifz              = llrint(dfz * FORCESCALE);
               atomicAdd((unsigned long long int*)&cSim.pBondedForceXAccumulator[atom.x], llitoulli(-ifx));
               atomicAdd((unsigned long long int*)&cSim.pBondedForceYAccumulator[atom.x], llitoulli(-ify));
               atomicAdd((unsigned long long int*)&cSim.pBondedForceZAccumulator[atom.x], llitoulli(-ifz));
       }
       else
       {
#ifdef NMR_NEIGHBORLIST
      int2 COMgrps                = cSim.pImageNMRCOMBumpCOMGrp[pos * 2];
#else
      int2 COMgrps                = cSim.pNMRCOMBumpCOMGrp[pos * 2];
#endif
           for (int ip = COMgrps.x; ip < COMgrps.y; ip++)
      {
#ifdef NMR_NEIGHBORLIST
           int2 COMatms           = cSim.pImageNMRCOMBumpCOM[ip];
#else
           int2 COMatms           = cSim.pNMRCOMBumpCOM[ip];
#endif

           for (int j = COMatms.x; j < COMatms.y + 1; j++ )
           {
#ifdef NMR_NEIGHBORLIST
         PMEDouble rmass     = cSim.pImageMass[j];
#else
         PMEDouble rmass     = cSim.pAtomMass[j];
#endif
         PMEDouble dcomdx    = rmass / rmstot.x;
              PMEDouble fx                 = dfx * dcomdx;
              PMEDouble fy                 = dfy * dcomdx;
              PMEDouble fz                 = dfz * dcomdx;
                  ifx              = llrint(fx * FORCESCALE);
                  ify              = llrint(fy * FORCESCALE);
                  ifz              = llrint(fz * FORCESCALE);
                       atomicAdd((unsigned long long int*)&cSim.pBondedForceXAccumulator[j], llitoulli(-ifx));
                  atomicAdd((unsigned long long int*)&cSim.pBondedForceYAccumulator[j], llitoulli(-ify));
                  atomicAdd((unsigned long long int*)&cSim.pBondedForceZAccumulator[j], llitoulli(-ifz));
           }
      }

       }

        }
exit5:
        {}
#ifdef NMR_ENERGY
        sE[threadIdx.x]                     = eCOMBump;
        sE[threadIdx.x]                    += sE[threadIdx.x ^ 1];
        sE[threadIdx.x]                    += sE[threadIdx.x ^ 2];
        sE[threadIdx.x]                    += sE[threadIdx.x ^ 4];
        sE[threadIdx.x]                    += sE[threadIdx.x ^ 8];
        sE[threadIdx.x]                    += sE[threadIdx.x ^ 16];
        if ((threadIdx.x & GRIDBITSMASK) == 0)
        {
            atomicAdd(cSim.pENMRCOMBump, llitoulli(sE[threadIdx.x]));
        }
#endif
    }


// BUMP POTENTIAL
// BUMP POTENTIAL



    // Calculate r6av distance restraints
    else if (pos < cSim.NMRr6avDistanceOffset)
    {
#ifdef NMR_ENERGY
        PMEAccumulator er6avdistance            = 0;
#endif
        pos                                -= cSim.NMRCOMBumpOffset;
        if (pos < cSim.NMRr6avDistances)
        {

#ifdef NMR_NEIGHBORLIST
            int2 atom                       = cSim.pImageNMRr6avDistanceID[pos];
#else
            int2 atom                       = cSim.pNMRr6avDistanceID[pos];
#endif
            int const maxpr = 512;
            PMEDouble xij[maxpr][3];
            int2 ijat0[maxpr];
            PMEDouble rm8[maxpr];
            PMEDouble fsum;
            PMEDouble rm6bar;
            PMEDouble rij;
            int nsum;
            PMEDouble2 R1R2;
            PMEDouble2 R3R4;
            PMEDouble2 K2K3;
#ifdef NMR_TIMED
            int2 Step                       = cSim.pNMRr6avDistanceStep[pos];
            int Inc                         = cSim.pNMRr6avDistanceInc[pos];
            // Skip restraint if not active
            if ((step < Step.x) || ((step > Step.y) && (Step.y > 0)))
                goto exit6;

            // Read timed data
            PMEDouble2 R1R2Slp              = cSim.pNMRr6avDistanceR1R2Slp[pos];
            PMEDouble2 R1R2Int              = cSim.pNMRr6avDistanceR1R2Int[pos];
            PMEDouble2 R3R4Slp              = cSim.pNMRr6avDistanceR3R4Slp[pos];
            PMEDouble2 R3R4Int              = cSim.pNMRr6avDistanceR3R4Int[pos];
            PMEDouble2 K2K3Slp              = cSim.pNMRr6avDistanceK2K3Slp[pos];
            PMEDouble2 K2K3Int              = cSim.pNMRr6avDistanceK2K3Int[pos];

            // Calculate increment
            double dstep                    = step - (double)((step - Step.x) % abs(Inc));

            // Calculate restraint values
            R1R2.x                          = R1R2Slp.x * dstep + R1R2Int.x;
            R1R2.y                          = R1R2Slp.y * dstep + R1R2Int.y;
            R3R4.x                          = R3R4Slp.x * dstep + R3R4Int.x;
            R3R4.y                          = R3R4Slp.y * dstep + R3R4Int.y;
            if (Inc > 0)
            {
                K2K3.x                      = K2K3Slp.x * dstep + K2K3Int.x;
                K2K3.y                      = K2K3Slp.y * dstep + K2K3Int.y;
            }
            else
            {
                int nstepu                  = (step - Step.x) / abs(Inc);
                K2K3.x                      = K2K3Int.x * pow(K2K3Slp.x, nstepu);
                K2K3.y                      = K2K3Int.y * pow(K2K3Slp.y, nstepu);
            }
#else
            R1R2                            = cSim.pNMRr6avDistanceR1R2[pos];
            R3R4                            = cSim.pNMRr6avDistanceR3R4[pos];
            K2K3                            = cSim.pNMRr6avDistanceK2K3[pos];
#endif

 //           printf("%12.5f %12.5f %12.5f %12.5f %12.5f %12.5f\n", R1R2.x, R1R2.y, R3R4.x, R3R4.y, K2K3.x, K2K3.y);
            	PMEDouble atomIX;
            	PMEDouble atomIY;
            	PMEDouble atomIZ;
            	PMEDouble atomJX;
            	PMEDouble atomJY;
            	PMEDouble atomJZ;
                int ipr = 0;
                rm6bar = (PMEDouble)0.0;
#if defined(NODPTEXTURE)
#ifdef NMR_NEIGHBORLIST
	    if (atom.y > 0)
	    {
            	atomJX            = cSim.pImageX[atom.y];
            	atomJY            = cSim.pImageY[atom.y];
            	atomJZ            = cSim.pImageZ[atom.y];
//	Find r6av
//
		int2 r6avgrps                = cSim.pImageNMRr6avDistancer6avGrp[pos * 2];

	        for (int ip = r6avgrps.x; ip < r6avgrps.y; ip++)
		{
		     int2 r6avatms           = cSim.pImageNMRr6avDistancer6av[ip];
		     for (int j = r6avatms.x; j < r6avatms.y + 1; j++ )
		     {
			xij[ipr][0]         = cSim.pImageX[j] - atomJX;
			xij[ipr][1]         = cSim.pImageY[j] - atomJY;
			xij[ipr][2]         = cSim.pImageZ[j] - atomJZ;
                        ijat0[ipr].x        = j;
                        ijat0[ipr].y        = atom.y;
                        PMEDouble rij2      = xij[ipr][0]*xij[ipr][0] + xij[ipr][1]*xij[ipr][1] + xij[ipr][2]*xij[ipr][2];
                        PMEDouble rm2       = (PMEDouble)1.0/rij2;
                        rm6bar              = rm6bar + pow(rm2, 3);
                        rm8[ipr]            = pow(rm2, 4);
                        ipr = ipr + 1;
		     }
                }
                nsum     = ipr;
                fsum     = __uint2double_rn (nsum);
                rm6bar   = rm6bar/fsum;
                rij      = pow(rm6bar,-(PMEDouble)1.0/(PMEDouble)6.0);
	    }
	    else if (atom.x > 0)
	    {
            	atomIX                = cSim.pImageX[atom.x];
            	atomIY                = cSim.pImageY[atom.x];
            	atomIZ                = cSim.pImageZ[atom.x];
//	Find r6av
		int2 r6avgrps                = cSim.pImageNMRr6avDistancer6avGrp[pos * 2 + 1];

	        for (int ip = r6avgrps.x; ip < r6avgrps.y; ip++)
		{
		     int2 r6avatms           = cSim.pImageNMRr6avDistancer6av[ip];
		     for (int j = r6avatms.x; j < r6avatms.y + 1; j++ )
		     {
			xij[ipr][0]         = atomIX - cSim.pImageX[j];
			xij[ipr][1]         = atomIY - cSim.pImageY[j];
			xij[ipr][2]         = atomIZ - cSim.pImageZ[j];
                        ijat0[ipr].x        = atom.x;
                        ijat0[ipr].y        = j;
                        PMEDouble rij2      = xij[ipr][0]*xij[ipr][0] + xij[ipr][1]*xij[ipr][1] + xij[ipr][2]*xij[ipr][2];
                        PMEDouble rm2       = (PMEDouble)1.0/rij2;
                        rm6bar              = rm6bar + pow(rm2,3);
                        rm8[ipr]            = pow(rm2,4);
                        ipr = ipr + 1;
		     }
                }
                nsum     = ipr;
                fsum     = __uint2double_rn (nsum);
                rm6bar   = rm6bar/fsum;
                rij      = pow(rm6bar,-(PMEDouble)1.0/(PMEDouble)6.0);
	    }
            else // r6av of 2 groups
            {
		int2 r6avgrpsi               = cSim.pImageNMRr6avDistancer6avGrp[pos * 2];

	        for (int ip = r6avgrpsi.x; ip < r6avgrpsi.y; ip++)
                {
		     int2 r6avatmsi          = cSim.pImageNMRr6avDistancer6av[ip];
		     for (int i = r6avatmsi.x; i < r6avatmsi.y + 1; i++ )
                     {
                          int2 r6avgrpsj               = cSim.pImageNMRr6avDistancer6avGrp[pos * 2 + 1];
                          for (int jp = r6avgrpsj.x; jp < r6avgrpsj.y; jp++)
                          {
                              int2 r6avatmsj          = cSim.pImageNMRr6avDistancer6av[jp];
                              for (int j = r6avatmsj.x; j < r6avatmsj.y + 1; j++ )
                              {
                                  xij[ipr][0]         = cSim.pImageX[i] - cSim.pImageX[j];
                                  xij[ipr][1]         = cSim.pImageY[i] - cSim.pImageY[j];
                                  xij[ipr][2]         = cSim.pImageZ[i] - cSim.pImageZ[j];
                                  ijat0[ipr].x        = i;
                                  ijat0[ipr].y        = j;
                                  PMEDouble rij2      = xij[ipr][0]*xij[ipr][0] + xij[ipr][1]*xij[ipr][1] + xij[ipr][2]*xij[ipr][2];
                                  PMEDouble rm2       = (PMEDouble)1.0/rij2;
                                  rm6bar              = rm6bar + pow(rm2, 3);
//                                  printf("ipr %i rm2 %f\n", ipr, rm2);
                                  rm8[ipr]            = pow(rm2, 4);
                                  ipr = ipr + 1;
                              }
                          }
                     }
                }
                nsum     = ipr;
                fsum     = __uint2double_rn (nsum);
                rm6bar   = rm6bar/fsum;
                rij      = pow(rm6bar, -(PMEDouble)1.0/(PMEDouble)6.0);
            }
//		printf("Image: %12.5f %12.5f %12.5f %12.5f %12.5f %12.5f\n", atomIX, atomIY, atomIZ, atomJX, atomJY, atomJZ);
#else
	    if (atom.y > 0)
	    {
            	atomJX            = cSim.pAtomX[atom.y];
            	atomJY            = cSim.pAtomY[atom.y];
            	atomJZ            = cSim.pAtomZ[atom.y];
//	Find r6av
//
		int2 r6avgrps                = cSim.pNMRr6avDistancer6avGrp[pos * 2];

	        for (int ip = r6avgrps.x; ip < r6avgrps.y; ip++)
		{
		     int2 r6avatms           = cSim.pNMRr6avDistancer6av[ip];
		     for (int j = r6avatms.x; j < r6avatms.y + 1; j++ )
		     {
			xij[ipr][0]         = cSim.pAtomX[j] - atomJX;
			xij[ipr][1]         = cSim.pAtomY[j] - atomJY;
			xij[ipr][2]         = cSim.pAtomZ[j] - atomJZ;
                        ijat0[ipr].x        = j;
                        ijat0[ipr].y        = atom.y;
                        PMEDouble rij2      = xij[ipr][0]*xij[ipr][0] + xij[ipr][1]*xij[ipr][1] + xij[ipr][2]*xij[ipr][2];
                        PMEDouble rm2       = (PMEDouble)1.0/rij2;
                        rm6bar              = rm6bar + pow(rm2, 3);
                        rm8[ipr]            = pow(rm2, 4);
                        ipr = ipr + 1;
		     }
                }
                nsum     = ipr;
                fsum     = __uint2double_rn (nsum);
                rm6bar   = rm6bar/fsum;
                rij      = pow(rm6bar, -(PMEDouble)1.0/(PMEDouble)6.0);
	    }
	    else if (atom.x > 0)
	    {
            	atomIX                = cSim.pAtomX[atom.x];
            	atomIY                = cSim.pAtomY[atom.x];
            	atomIZ                = cSim.pAtomZ[atom.x];
//	Find r6av
		int2 r6avgrps                = cSim.pNMRr6avDistancer6avGrp[pos * 2 + 1];

	        for (int ip = r6avgrps.x; ip < r6avgrps.y; ip++)
		{
		     int2 r6avatms           = cSim.pNMRr6avDistancer6av[ip];
		     for (int j = r6avatms.x; j < r6avatms.y + 1; j++ )
		     {
			xij[ipr][0]         = atomIX - cSim.pAtomX[j];
			xij[ipr][1]         = atomIY - cSim.pAtomY[j];
			xij[ipr][2]         = atomIZ - cSim.pAtomZ[j];
                        ijat0[ipr].x        = atom.x;
                        ijat0[ipr].y        = j;
                        PMEDouble rij2      = xij[ipr][0]*xij[ipr][0] + xij[ipr][1]*xij[ipr][1] + xij[ipr][2]*xij[ipr][2];
                        PMEDouble rm2       = (PMEDouble)1.0/rij2;
                        rm6bar              = rm6bar + pow(rm2, 3);
                        rm8[ipr]            = pow(rm2, 4);
                        ipr = ipr + 1;
		     }
                }
                nsum     = ipr;
                fsum     = __uint2double_rn (nsum);
                rm6bar   = rm6bar/fsum;
                rij      = pow(rm6bar, -(PMEDouble)1.0/(PMEDouble)6.0);
	    }
            else // r6av of 2 groups
            {
		int2 r6avgrpsi               = cSim.pNMRr6avDistancer6avGrp[pos * 2];

	        for (int ip = r6avgrpsi.x; ip < r6avgrpsi.y; ip++)
                {
		     int2 r6avatmsi          = cSim.pNMRr6avDistancer6av[ip];
		     for (int i = r6avatmsi.x; i < r6avatmsi.y + 1; i++ )
                     {
                          int2 r6avgrpsj               = cSim.pNMRr6avDistancer6avGrp[pos * 2 + 1];
                          for (int jp = r6avgrpsj.x; jp < r6avgrpsj.y; jp++)
                          {
                              int2 r6avatmsj          = cSim.pNMRr6avDistancer6av[jp];
                              for (int j = r6avatmsj.x; j < r6avatmsj.y + 1; j++ )
                              {
                                  xij[ipr][0]         = cSim.pAtomX[i] - cSim.pAtomX[j];
                                  xij[ipr][1]         = cSim.pAtomY[i] - cSim.pAtomY[j];
                                  xij[ipr][2]         = cSim.pAtomZ[i] - cSim.pAtomZ[j];
                                  ijat0[ipr].x        = i;
                                  ijat0[ipr].y        = j;
                                  PMEDouble rij2      = xij[ipr][0]*xij[ipr][0] + xij[ipr][1]*xij[ipr][1] + xij[ipr][2]*xij[ipr][2];
                                  PMEDouble rm2       = (PMEDouble)1.0/rij2;
//                                  printf("ipr %i rm2 %f\n", ipr, rm2);
                                  rm6bar              = rm6bar + pow(rm2, 3);
                                  rm8[ipr]            = pow(rm2, 4);
                                  ipr = ipr + 1;
                              }
                          }
                     }
                }
                nsum     = ipr;
                fsum     = __uint2double_rn (nsum);
                rm6bar   = rm6bar/fsum;
                rij      = pow(rm6bar, -(PMEDouble)1.0/(PMEDouble)6.0);
            }
//		printf("Atom : %12.5f %12.5f %12.5f %12.5f %12.5f %12.5f\n", atomIX, atomIY, atomIZ, atomJX, atomJY, atomJZ);
#endif
#else
//            printf("texture atom.x %i atom.y %i\n", atom.x, atom.y);
	    if (atom.y > 0)
	    {
            	int2 iatomJX                    = tex1Dfetch(texref, atom.y);
            	int2 iatomJY                    = tex1Dfetch(texref, atom.y + cSim.stride);
            	int2 iatomJZ                    = tex1Dfetch(texref, atom.y + cSim.stride2);

            	atomJX                = __hiloint2double(iatomJX.y, iatomJX.x);
            	atomJY                = __hiloint2double(iatomJY.y, iatomJY.x);
            	atomJZ                = __hiloint2double(iatomJZ.y, iatomJZ.x);
//	Find r6av
#ifdef NMR_NEIGHBORLIST
		int2 r6avgrps                = cSim.pImageNMRr6avDistancer6avGrp[pos * 2];
#else
		int2 r6avgrps                = cSim.pNMRr6avDistancer6avGrp[pos * 2];
#endif
	        for (int ip = r6avgrps.x; ip < r6avgrps.y; ip++)
		{
#ifdef NMR_NEIGHBORLIST
		     int2 r6avatms           = cSim.pImageNMRr6avDistancer6av[ip];
#else
		     int2 r6avatms           = cSim.pNMRr6avDistancer6av[ip];
#endif
		     for (int j = r6avatms.x; j < r6avatms.y + 1; j++ )
		     {
                        int2 iatomIX        = tex1Dfetch(texref, j);
                        int2 iatomIY        = tex1Dfetch(texref, j + cSim.stride);
                        int2 iatomIZ        = tex1Dfetch(texref, j + cSim.stride2);

                        atomIX    = __hiloint2double(iatomIX.y, iatomIX.x);
                        atomIY    = __hiloint2double(iatomIY.y, iatomIY.x);
                        atomIZ    = __hiloint2double(iatomIZ.y, iatomIZ.x);

			xij[ipr][0]         = atomIX - atomJX;
			xij[ipr][1]         = atomIY - atomJY;
			xij[ipr][2]         = atomIZ - atomJZ;

                        ijat0[ipr].x        = j;
                        ijat0[ipr].y        = atom.y;

                        PMEDouble rij2      = xij[ipr][0]*xij[ipr][0] + xij[ipr][1]*xij[ipr][1] + xij[ipr][2]*xij[ipr][2];
                        PMEDouble rm2       = (PMEDouble)1.0/rij2;
                        rm6bar              = rm6bar + pow(rm2, 3);
                        rm8[ipr]            = pow(rm2, 4);
                        ipr = ipr + 1;
		     }
                }
                nsum     = ipr;
                fsum     = __uint2double_rn (nsum);
                rm6bar   = rm6bar/fsum;
                rij      = pow(rm6bar, -(PMEDouble)1.0/(PMEDouble)6.0);
	    }
	    else if (atom.x > 0)
	    {
            	int2 iatomIX                    = tex1Dfetch(texref, atom.x);
            	int2 iatomIY                    = tex1Dfetch(texref, atom.x + cSim.stride);
            	int2 iatomIZ                    = tex1Dfetch(texref, atom.x + cSim.stride2);

            	atomIX                = __hiloint2double(iatomIX.y, iatomIX.x);
            	atomIY                = __hiloint2double(iatomIY.y, iatomIY.x);
            	atomIZ                = __hiloint2double(iatomIZ.y, iatomIZ.x);
//	Find r6av
#ifdef NMR_NEIGHBORLIST
		int2 r6avgrps                = cSim.pImageNMRr6avDistancer6avGrp[pos * 2 + 1];
#else
		int2 r6avgrps                = cSim.pNMRr6avDistancer6avGrp[pos * 2 + 1];
#endif
	        for (int ip = r6avgrps.x; ip < r6avgrps.y; ip++)
		{
#ifdef NMR_NEIGHBORLIST
		     int2 r6avatms           = cSim.pImageNMRr6avDistancer6av[ip];
#else
		     int2 r6avatms           = cSim.pNMRr6avDistancer6av[ip];
#endif
		     for (int j = r6avatms.x; j < r6avatms.y + 1; j++ )
		     {
                        int2 iatomJX        = tex1Dfetch(texref, j);
                        int2 iatomJY        = tex1Dfetch(texref, j + cSim.stride);
                        int2 iatomJZ        = tex1Dfetch(texref, j + cSim.stride2);

                        atomJX    = __hiloint2double(iatomJX.y, iatomJX.x);
                        atomJY    = __hiloint2double(iatomJY.y, iatomJY.x);
                        atomJZ    = __hiloint2double(iatomJZ.y, iatomJZ.x);

			xij[ipr][0]         = atomIX - atomJX;
			xij[ipr][1]         = atomIY - atomJY;
			xij[ipr][2]         = atomIZ - atomJZ;

                        ijat0[ipr].x        = atom.x;
                        ijat0[ipr].y        = j;

                        PMEDouble rij2      = xij[ipr][0]*xij[ipr][0] + xij[ipr][1]*xij[ipr][1] + xij[ipr][2]*xij[ipr][2];
                        PMEDouble rm2       = (PMEDouble)1.0/rij2;
                        rm6bar              = rm6bar + pow(rm2, 3);
                        rm8[ipr]            = pow(rm2, 4);
                        ipr = ipr + 1;
		     }
                }
                nsum     = ipr;
                fsum     = __uint2double_rn (nsum);
                rm6bar   = rm6bar/fsum;
                rij      = pow(rm6bar,-(PMEDouble)1.0/(PMEDouble)6.0);
	    }
            else // r6av of 2 groups
            {
#ifdef NMR_NEIGHBORLIST
                int2 r6avgrpsi               = cSim.pImageNMRr6avDistancer6avGrp[pos * 2];
#else
                int2 r6avgrpsi               = cSim.pNMRr6avDistancer6avGrp[pos * 2];
#endif

	        for (int ip = r6avgrpsi.x; ip < r6avgrpsi.y; ip++)
                {
#ifdef NMR_NEIGHBORLIST
		     int2 r6avatmsi          = cSim.pImageNMRr6avDistancer6av[ip];
#else
		     int2 r6avatmsi          = cSim.pNMRr6avDistancer6av[ip];
#endif
		     for (int i = r6avatmsi.x; i < r6avatmsi.y + 1; i++ )
                     {
#ifdef NMR_NEIGHBORLIST
		         int2 r6avgrpsj               = cSim.pImageNMRr6avDistancer6avGrp[pos * 2 + 1];
#else
		         int2 r6avgrpsj               = cSim.pNMRr6avDistancer6avGrp[pos * 2 + 1];
#endif
                          for (int jp = r6avgrpsj.x; jp < r6avgrpsj.y; jp++)
                          {
#ifdef NMR_NEIGHBORLIST
                              int2 r6avatmsj          = cSim.pImageNMRr6avDistancer6av[jp];
#else
		              int2 r6avatmsj          = cSim.pNMRr6avDistancer6av[jp];
#endif
                              for (int j = r6avatmsj.x; j < r6avatmsj.y + 1; j++ )
                              {
            	                  int2 iatomIX        = tex1Dfetch(texref, i);
                              	  int2 iatomIY        = tex1Dfetch(texref, i + cSim.stride);
            	                  int2 iatomIZ        = tex1Dfetch(texref, i + cSim.stride2);

            	                  atomIX              = __hiloint2double(iatomIX.y, iatomIX.x);
             	                  atomIY              = __hiloint2double(iatomIY.y, iatomIY.x);
            	                  atomIZ              = __hiloint2double(iatomIZ.y, iatomIZ.x);

            	                  int2 iatomJX        = tex1Dfetch(texref, j);
                              	  int2 iatomJY        = tex1Dfetch(texref, j + cSim.stride);
            	                  int2 iatomJZ        = tex1Dfetch(texref, j + cSim.stride2);

            	                  atomJX              = __hiloint2double(iatomJX.y, iatomJX.x);
             	                  atomJY              = __hiloint2double(iatomJY.y, iatomJY.x);
            	                  atomJZ              = __hiloint2double(iatomJZ.y, iatomJZ.x);

                                  xij[ipr][0]         = atomIX - atomJX;
                                  xij[ipr][1]         = atomIY - atomJY;
                                  xij[ipr][2]         = atomIZ - atomJZ;
                                  ijat0[ipr].x        = i;
                                  ijat0[ipr].y        = j;
                                  PMEDouble rij2      = xij[ipr][0]*xij[ipr][0] + xij[ipr][1]*xij[ipr][1] + xij[ipr][2]*xij[ipr][2];
                                  PMEDouble rm2       = (PMEDouble)1.0/rij2;
                                  rm6bar              = rm6bar + pow(rm2, 3);
                                  rm8[ipr]            = pow(rm2, 4);
//                                  printf("ipr %i rm2 %f atomIX %f atomIY %f atomIZ %f atomJX %f atomJY %f atomJZ %f\n", ipr, rm2, atomIX, atomIY, atomIZ, atomJX, atomJY, atomJZ);
                                  ipr = ipr + 1;
                              }
                          }
                     }
                }
                nsum     = ipr;
                fsum     = __uint2double_rn (nsum);
                rm6bar   = rm6bar/fsum;
                rij      = pow(rm6bar, (PMEDouble)-1.0/(PMEDouble)6.0);
            }
//		printf("Atom : %12.5f %12.5f %12.5f %12.5f %12.5f %12.5f\n", atomIX, atomIY, atomIZ, atomJX, atomJY, atomJZ);
#endif
            PMEDouble df;
#ifdef NMR_ENERGY
            PMEDouble e;
#endif
            if (rij < R1R2.x)
            {
                PMEDouble dif               = R1R2.x - R1R2.y;
                df                          = (PMEDouble)2.0 * K2K3.x * dif;
#ifdef NMR_ENERGY
                e                           = df * (rij - R1R2.x) + K2K3.x * dif * dif;
#endif
            }
            else if (rij < R1R2.y)
            {
                PMEDouble dif               = rij - R1R2.y;
                df                          = (PMEDouble)2.0 * K2K3.x * dif;
#ifdef NMR_ENERGY
                e                           = K2K3.x * dif * dif;
#endif
            }
            else if (rij < R3R4.x)
            {
                df                          = (PMEDouble)0.0;
#ifdef NMR_ENERGY
                e                           = (PMEDouble)0.0;
#endif
            }
            else if (rij < R3R4.y)
            {
                PMEDouble dif               = rij - R3R4.x;
                df                          = (PMEDouble)2.0 * K2K3.y * dif;
#ifdef NMR_ENERGY
                e                           = K2K3.y * dif * dif;
#endif
            }
            else
            {
                PMEDouble dif               = R3R4.y - R3R4.x;
                df                          = (PMEDouble)2.0 * K2K3.y * dif;
#ifdef NMR_ENERGY
                e                           = df * (rij - R3R4.y) + K2K3.y * dif * dif;
#endif
            }

            if (cSim.bJar)
            {
                double fold                 = cSim.pNMRJarData[2];
                double work                 = cSim.pNMRJarData[3];
                double first                = cSim.pNMRJarData[4];
                double fcurr                = (PMEDouble)-2.0 * K2K3.x * (rij - R1R2.y);
                if (first == (PMEDouble)0.0) {
                    fold                    = -fcurr;
                    cSim.pNMRJarData[4]     = (PMEDouble)1.0;
                }
                work                       += (PMEDouble)0.5 * (fcurr + fold) * cSim.drjar;
                cSim.pNMRJarData[0]         = R1R2.y;
                cSim.pNMRJarData[1]         = rij;
                cSim.pNMRJarData[2]         = fcurr;
                cSim.pNMRJarData[3]         = work;
            }
#ifdef NMR_ENERGY
            er6avdistance                   += llrint(ENERGYSCALE * e);
#endif

//            df                             *= (PMEDouble)1.0 / rij;
            PMEDouble fact                  = df * (rij / (fsum * rm6bar));
            int i3, j3;
            PMEDouble fact2;
            PMEDouble dfx;
            PMEDouble dfy;
            PMEDouble dfz;
            PMEAccumulator ifx;
            PMEAccumulator ify;
            PMEAccumulator ifz;
            for (int ipr = 0; ipr < nsum; ipr++)
            {
                i3               = ijat0[ipr].x;
                j3               = ijat0[ipr].y;
                fact2            = fact * rm8[ipr];
                dfx              = fact2 * xij[ipr][0];
                dfy              = fact2 * xij[ipr][1];
                dfz              = fact2 * xij[ipr][2];
//                printf("ipr = %i dfx %f dfy %f dfz %f rm8[ipr] %f xij0 %f xij1 %f xij2 %f\n",ipr, dfx, dfy, dfz, rm8[ipr], xij[ipr][0], xij[ipr][1], xij[ipr][2]);
            	ifx              = llrint(dfx * FORCESCALE);
            	ify              = llrint(dfy * FORCESCALE);
            	ifz              = llrint(dfz * FORCESCALE);
            	atomicAdd((unsigned long long int*)&cSim.pBondedForceXAccumulator[i3], llitoulli(-ifx));
            	atomicAdd((unsigned long long int*)&cSim.pBondedForceYAccumulator[i3], llitoulli(-ify));
            	atomicAdd((unsigned long long int*)&cSim.pBondedForceZAccumulator[i3], llitoulli(-ifz));
            	atomicAdd((unsigned long long int*)&cSim.pBondedForceXAccumulator[j3], llitoulli(ifx));
            	atomicAdd((unsigned long long int*)&cSim.pBondedForceYAccumulator[j3], llitoulli(ify));
            	atomicAdd((unsigned long long int*)&cSim.pBondedForceZAccumulator[j3], llitoulli(ifz));
             }
	    }

exit6:
        {}
#ifdef NMR_ENERGY
        sE[threadIdx.x]                     = er6avdistance;
        sE[threadIdx.x]                    += sE[threadIdx.x ^ 1];
        sE[threadIdx.x]                    += sE[threadIdx.x ^ 2];
        sE[threadIdx.x]                    += sE[threadIdx.x ^ 4];
        sE[threadIdx.x]                    += sE[threadIdx.x ^ 8];
        sE[threadIdx.x]                    += sE[threadIdx.x ^ 16];
        if ((threadIdx.x & GRIDBITSMASK) == 0)
        {
            atomicAdd(cSim.pENMRr6avDistance, llitoulli(sE[threadIdx.x]));
        }
#endif
    }

}


