#include "copyright.i"

/***************************************************/
/*                                                 */
/*      AMBER NVIDIA CUDA GPU IMPLEMENTATION       */
/*                 PMEMD VERSION                   */
/*                   Feb 2014                      */
/*                      by                         */
/*                Scott Le Grand                   */
/*                     and                         */
/*                Ross C. Walker                   */
/*                                                 */
/***************************************************/

#ifndef __GPUTYPES_H__
#define __GPUTYPES_H__
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <cuda.h>
#include <cufft.h>
#include <curand.h>
#include <vector_functions.h>
#include <cuda_runtime_api.h>
#include <builtin_types.h>
#include <cstring>

#ifdef MPI
#undef MPI
#include <mpi.h>
#define MPI
#endif

/* Enforce use of CUDA 5.0 due to GK110 issues with 4.2 */
#if defined(CUDA_VERSION) && (CUDA_VERSION < 5000)
#error "CUDA support requires the use of a 5.0 or later CUDA toolkit. Aborting compilation."
#endif

/* Control of single and double precision use. If neither use_SPXP
   or use_DPFP are defined then the code runs in the default SPFP
   mode which makes mixed use of single and double precision as
   needed. Defining use_DPFP makes it use double precision throughout
   while defining use_SPFP makes it use single precision for nonbonded
   force components, double-precision for bonded force components and SHAKE,
   and 24.40 bit fixed-point for force accumulation.

   Note using Double Precision throughout will cause, in some cases large,
   performance degradation.

   This option is now set in the configure script and the original defines
   are left here just for reference.
*/
//#define use_DPFP
//#define use_SPXP
//#define use_SPFP

// Enforce definition of one and only one precision mode
#if !(defined(use_DPFP) && !defined(use_SPXP) && !defined(use_SPFP)) && \
    !(defined(use_SPXP) && !defined(use_DPFP) && !defined(use_SPFP)) && \
    !(defined(use_SPFP) && !defined(use_DPFP) && !defined(use_SPXP))
#error "You must define one and only one precision mode (use_SPFP, use_SPXP, or use_DPFP) to build pmemd.cuda. Aborting compilation."
#endif

#if defined(_MSC_VER)
#define __align(_boundary_size) __declspec(align(_boundary_size))
#else
#define __align(_boundary_size) __attribute__((aligned(_boundary_size)))
#endif

typedef double                 __align(8) aligned_double;
typedef unsigned long int      __align(8) aligned_uli;
typedef long long int          __align(8) aligned_lli;
typedef unsigned long long int __align(8) PMEUllInt;

typedef long long int  __align(8)  PMEAccumulator;
typedef long long int  __align(8)  PMEForceAccumulator;
typedef long long int  __align(8)  PMEEnergyAccumulator;
typedef long long int  __align(8)  PMEVirialAccumulator;

#if defined(use_DPFP)
typedef double             __align(8)  PMEDouble;
typedef double             __align(8)  PMEFloat;
typedef double             __align(8)  PMEForce;
typedef double             __align(8)  PMEEnergy;
typedef double             __align(8)  PMEVirial;
typedef double2            __align(16) PMEDouble2;
typedef double4            __align(32) PMEDouble4;
typedef double2            __align(16) PMEFloat2;
typedef double4            __align(16) PMEFloat4;
typedef cufftDoubleComplex __align(16) PMEComplex;
#ifdef MPI
static const MPI_Datatype MPI_PMEDOUBLE = MPI_DOUBLE_PRECISION;
static const MPI_Datatype MPI_PMEFLOAT = MPI_DOUBLE_PRECISION;
static const MPI_Datatype MPI_PMEACCUMULATOR = MPI_LONG_LONG_INT;
#endif
#elif defined(use_SPXP) || defined(use_SPFP)
typedef double             __align(8)  PMEDouble;
typedef long long int      __align(8)  PMEForce;
typedef long long int      __align(8)  PMEEnergy;
typedef long long int      __align(8)  PMEVirial;
typedef float PMEFloat;
typedef double2 __align(16) PMEDouble2;
typedef double4 __align(32) PMEDouble4;
typedef float2 __align(8)  PMEFloat2;
typedef float4 __align(16) PMEFloat4;
typedef cufftComplex PMEComplex;
#ifdef MPI
static const MPI_Datatype MPI_PMEDOUBLE = MPI_DOUBLE_PRECISION;
static const MPI_Datatype MPI_PMEFLOAT = MPI_FLOAT;
static const MPI_Datatype MPI_PMEACCUMULATOR = MPI_LONG_LONG_INT;
#endif
#endif // use_DPFP

enum SM_VERSION
{
    SM_10,
    SM_11,
    SM_12,
    SM_13,
    SM_2X,
    SM_3X,
};

// Miscellaneous constants
enum {
    GRID                        = 32,
    GRIDBITS                    = 5,
    GRIDBITSMASK                = (GRID - 1),
    GRIDPADDINGMASK             = 0xffffffff - (GRID - 1),
    CMAPRESOLUTION              = 24,
    CMAPDIMENSION               = CMAPRESOLUTION + 4,
    CMAPSTEPSIZE                = 15,
    NEIGHBORCELLS               = 14,
    CELLIDXBITS                 = 10,
    CELLIDYBITS                 = 10,
    CELLIDZBITS                 = 10,
    CELLIDYSHIFT                = CELLIDXBITS,
    CELLIDZSHIFT                = CELLIDXBITS + CELLIDYBITS,
    CELLIDMASK                  = 0x0000003f,
    CELLHASHXBITS               = 2,
    CELLHASHYBITS               = 2,
    CELLHASHZBITS               = 2,
    CELLHASHBITS                = (CELLHASHXBITS + CELLHASHYBITS + CELLHASHZBITS),
    CELLHASHX                   = (1 << CELLHASHXBITS),
    CELLHASHY                   = (1 << CELLHASHYBITS),
    CELLHASHZ                   = (1 << CELLHASHZBITS),
    CELLHASHXY                  = CELLHASHX * CELLHASHY,
    CELLHASHCELLS               = (1 << CELLHASHBITS),
    CELLXCSHIFT                 = 0,
    CELLYCSHIFT                 = 10,
    CELLZCSHIFT                 = 20,

    NLRECORDCELLCOUNTBITS       = 4,
    NLRECORDCELLCOUNTMASK       = ((1 << NLRECORDCELLCOUNTBITS) - 1),
    NLRECORDYOFFSETSHIFT        = NLRECORDCELLCOUNTBITS,
    NLRECORDCELLSHIFT           = 4,
    NLRECORDCELLTYPEMASK        = ((1 << NLRECORDCELLSHIFT) - 1),

    NLENTRYYMAXSHIFT            = 1,
    NLENTRYHOMECELLMASK         = 1,

    NLATOMCELLSHIFT             = 4,
    NLATOMCELLTYPEMASK          = ((1 << NLATOMCELLSHIFT) - 1),


    NLEXCLUSIONSHIFT            = 8,
    NLEXCLUSIONATOMMASK         = ((1 << NLEXCLUSIONSHIFT) - 1),
    VIRIALOFFSET                = 19,
    AMDEDIHEDRALOFFSET          = 25,
    GAMDEDIHEDRALOFFSET         = 26,
    ENERGYTERMS                 = 28,
    TI_ENERGYTERMS              = ENERGYTERMS * 3,
    PADDING                     = 16,
    PADDINGMASK                 = 0xfffffff0,
#ifdef AWSMM
    PP_ENERGIES                 = 1,
    PP_VELOCITIES               = 2,
    PP_RMSD                     = 4,
#endif
};

struct NLRecordStruct
{
    unsigned int neighborCell[NEIGHBORCELLS];       // Bits 0:3, cell type, buts 4:31, cell ID
    unsigned int homeCell;                          // Homecell ID
    unsigned int neighborCells;                     // Bits 0:3, number of neighbor cells, bits 4:31, y offset;
};

union NLRecord
{
    NLRecordStruct NL;
    unsigned int array[NEIGHBORCELLS + 2];
};

struct NLEntryStruct
{
    unsigned int ypos;                              // First y atom
    unsigned int ymax;                              // Bit 0, home cell flag, Bits 1:31, number of y atoms (1-32)
    unsigned int xatoms;                            // Number of x atoms
    unsigned int offset;                            // Offset to entry's atom list
};

union NLEntry
{
    NLEntryStruct NL;
    unsigned int array[4];
};

// Kernel dimensions - we only support SM 2.0 or better due to double-precision
// and atomic operation requirements
static const int SM_2X_THREADS_PER_BLOCK                        = 512;
static const int SM_3X_THREADS_PER_BLOCK                        = 1024;
static const int SM_2X_NLCALCULATE_OFFSETS_THREADS_PER_BLOCK    = 720;
static const int SM_3X_NLCALCULATE_OFFSETS_THREADS_PER_BLOCK    = 720;
static const int SM_2X_NLBUILD_NEIGHBORLIST8_THREADS_PER_BLOCK  = 768;
static const int SM_3X_NLBUILD_NEIGHBORLIST8_THREADS_PER_BLOCK  = 640;
static const int SM_2X_NLBUILD_NEIGHBORLIST16_THREADS_PER_BLOCK = 768;
static const int SM_3X_NLBUILD_NEIGHBORLIST16_THREADS_PER_BLOCK = 640;
static const int SM_2X_NLBUILD_NEIGHBORLIST32_THREADS_PER_BLOCK = 768;
static const int SM_3X_NLBUILD_NEIGHBORLIST32_THREADS_PER_BLOCK = 640;
static const int SM_2X_NLBUILD_NEIGHBORLIST_BLOCKS_MULTIPLIER   = 1;
static const int SM_3X_NLBUILD_NEIGHBORLIST_BLOCKS_MULTIPLIER   = 2;
static const int SM_2X_LOCALFORCES_BLOCKS                       = 8;
static const int SM_3X_LOCALFORCES_BLOCKS                       = 16;
static const int SM_2X_LOCALFORCES_THREADS_PER_BLOCK            = 64;
static const int SM_3X_LOCALFORCES_THREADS_PER_BLOCK            = 64;
static const int SM_2X_CHARMMFORCES_BLOCKS                      = 8;
static const int SM_3X_CHARMMFORCES_BLOCKS                      = 16;
static const int SM_2X_CHARMMFORCES_THREADS_PER_BLOCK           = 64;
static const int SM_3X_CHARMMFORCES_THREADS_PER_BLOCK           = 64;
static const int SM_2X_NMRFORCES_BLOCKS                         = 8;
static const int SM_3X_NMRFORCES_BLOCKS                         = 16;
static const int SM_2X_NMRFORCES_THREADS_PER_BLOCK              = 64;
static const int SM_3X_NMRFORCES_THREADS_PER_BLOCK              = 64;
static const int SM_2X_CLEARFORCES_THREADS_PER_BLOCK            = 768;
static const int SM_3X_CLEARFORCES_THREADS_PER_BLOCK            = 1024;
static const int SM_2X_NLCLEARFORCES_THREADS_PER_BLOCK          = 768;
static const int SM_3X_NLCLEARFORCES_THREADS_PER_BLOCK          = 1024;
static const int SM_2X_NLREDUCEFORCES_THREADS_PER_BLOCK         = 768;
static const int SM_3X_NLREDUCEFORCES_THREADS_PER_BLOCK         = 1024;
static const int SM_2X_REDUCEFORCES_THREADS_PER_BLOCK           = 768;
static const int SM_3X_REDUCEFORCES_THREADS_PER_BLOCK           = 1024;
static const int SM_2X_REDUCEBUFFER_THREADS_PER_BLOCK           = 768;
static const int SM_3X_REDUCEBUFFER_THREADS_PER_BLOCK           = 1024;
static const int SM_2X_SHAKE_THREADS_PER_BLOCK                  = 64;
static const int SM_3X_SHAKE_THREADS_PER_BLOCK                  = 64;
static const int SM_2X_SHAKE_BLOCKS                             = 8;
static const int SM_3X_SHAKE_BLOCKS                             = 16;
static const int SM_2X_UPDATE_THREADS_PER_BLOCK                 = 256;
static const int SM_3X_UPDATE_THREADS_PER_BLOCK                 = 256;
static const int SM_2X_GENERAL_THREADS_PER_BLOCK                = 512;
static const int SM_3X_GENERAL_THREADS_PER_BLOCK                = 1024;
#ifdef use_DPFP
static const int SM_2X_GBBORNRADII_THREADS_PER_BLOCK            = 512;
static const int SM_3X_GBBORNRADII_THREADS_PER_BLOCK            = 1024;
static const int SM_3X_GBBORNRADII_BLOCKS_MULTIPLIER            = 1;
static const int SM_2X_GBBORNRADII_BLOCKS_MULTIPLIER            = 1;
static const int SM_2X_GBNONBONDENERGY1_THREADS_PER_BLOCK       = 512;
static const int SM_3X_GBNONBONDENERGY1_THREADS_PER_BLOCK       = 1024;
static const int SM_3X_GBNONBONDENERGY1_BLOCKS_MULTIPLIER       = 1;
static const int SM_2X_GBNONBONDENERGY1_BLOCKS_MULTIPLIER       = 1;
static const int SM_2X_GBNONBONDENERGY2_THREADS_PER_BLOCK       = 512;
static const int SM_3X_GBNONBONDENERGY2_THREADS_PER_BLOCK       = 512;
static const int SM_2X_GBNONBONDENERGY2_BLOCKS_MULTIPLIER       = 1;
static const int SM_3X_GBNONBONDENERGY2_BLOCKS_MULTIPLIER       = 1;
static const int SM_2X_PMENONBONDENERGY_THREADS_PER_BLOCK       = 512;
static const int SM_3X_PMENONBONDENERGY_THREADS_PER_BLOCK       = 1024;
static const int SM_2X_PMENONBONDFORCES_THREADS_PER_BLOCK       = 512;
static const int SM_3X_PMENONBONDFORCES_THREADS_PER_BLOCK       = 1024;
static const int SM_2X_PMENONBONDENERGY_BLOCKS_MULTIPLIER       = 1;
static const int SM_3X_PMENONBONDENERGY_BLOCKS_MULTIPLIER       = 1;
static const int SM_2X_IPSNONBONDENERGY_THREADS_PER_BLOCK       = 512;
static const int SM_3X_IPSNONBONDENERGY_THREADS_PER_BLOCK       = 1024;
static const int SM_2X_IPSNONBONDFORCES_THREADS_PER_BLOCK       = 512;
static const int SM_3X_IPSNONBONDFORCES_THREADS_PER_BLOCK       = 1024;
static const int SM_2X_IPSNONBONDENERGY_BLOCKS_MULTIPLIER       = 1;
static const int SM_3X_IPSNONBONDENERGY_BLOCKS_MULTIPLIER       = 1;
#else
static const int SM_2X_GBBORNRADII_THREADS_PER_BLOCK            = 768;
static const int SM_3X_GBBORNRADII_THREADS_PER_BLOCK            = 768;
static const int SM_2X_GBBORNRADII_BLOCKS_MULTIPLIER            = 1;
static const int SM_3X_GBBORNRADII_BLOCKS_MULTIPLIER            = 2;
static const int SM_2X_GBNONBONDENERGY1_THREADS_PER_BLOCK       = 640;
static const int SM_3X_GBNONBONDENERGY1_THREADS_PER_BLOCK       = 576;
static const int SM_2X_GBNONBONDENERGY1_BLOCKS_MULTIPLIER       = 1;
static const int SM_3X_GBNONBONDENERGY1_BLOCKS_MULTIPLIER       = 2;
static const int SM_2X_GBNONBONDENERGY2_THREADS_PER_BLOCK       = 768;
static const int SM_3X_GBNONBONDENERGY2_THREADS_PER_BLOCK       = 640;
static const int SM_2X_GBNONBONDENERGY2_BLOCKS_MULTIPLIER       = 1;
static const int SM_3X_GBNONBONDENERGY2_BLOCKS_MULTIPLIER       = 2;
static const int SM_2X_PMENONBONDENERGY_THREADS_PER_BLOCK       = 768;
static const int SM_3X_PMENONBONDENERGY_THREADS_PER_BLOCK       = 640;
static const int SM_2X_PMENONBONDFORCES_THREADS_PER_BLOCK       = 768;
static const int SM_3X_PMENONBONDFORCES_THREADS_PER_BLOCK       = 768;
static const int SM_2X_PMENONBONDENERGY_BLOCKS_MULTIPLIER       = 1;
static const int SM_3X_PMENONBONDENERGY_BLOCKS_MULTIPLIER       = 2;
static const int SM_2X_IPSNONBONDENERGY_THREADS_PER_BLOCK       = 768;
static const int SM_3X_IPSNONBONDENERGY_THREADS_PER_BLOCK       = 512;
static const int SM_2X_IPSNONBONDFORCES_THREADS_PER_BLOCK       = 768;
static const int SM_3X_IPSNONBONDFORCES_THREADS_PER_BLOCK       = 512;
static const int SM_2X_IPSNONBONDENERGY_BLOCKS_MULTIPLIER       = 1;
static const int SM_3X_IPSNONBONDENERGY_BLOCKS_MULTIPLIER       = 2;
#endif

static const int SM_2X_MAXMOLECULES                             = 1535;
static const int SM_3X_MAXMOLECULES                             = 1024;
static const int SM_2X_MAXPSMOLECULES                           = 2040;
static const int SM_3X_MAXPSMOLECULES                           = 2040;
static const int SM_2X_READ_SIZE                                = 128;
static const int SM_3X_READ_SIZE                                = 128;
static const int GB_TEXTURE_WIDTH                               = 512;
#ifdef DPFP
static const int MAX_RANDOM_STEPS                               = 64;
#else
static const int MAX_RANDOM_STEPS                               = 128;
#endif

// PME data
#define PI_VAL 3.1415926535897932384626433832795
static const PMEDouble PI                                       = (PMEDouble) PI_VAL;
static const PMEFloat PI_F                                      = (PMEFloat)  PI_VAL;


// Used for bounding allowable contents of nonbond cells
#define AVERAGE_ATOM_VOLUME_VAL 9.83
static const PMEDouble AVERAGE_ATOM_VOLUME                      = (PMEDouble) AVERAGE_ATOM_VOLUME_VAL;
static const PMEFloat AVERAGE_ATOM_VOLUMEF                      = (PMEFloat)  AVERAGE_ATOM_VOLUME_VAL;

// PME data
#define KB_VAL (1.380658 * 6.0221367) / (4.184 * 1000.0)        // Boltzmann's constant in internal units
static const PMEDouble KB                                       = (PMEDouble) KB_VAL;
static const PMEFloat KB_F                                      = (PMEFloat)  KB_VAL;

// Compilation flags
static const bool bShadowedOutputBuffers                        = false;    // Turns off sysmem shadowing of really large buffers

struct listdata_rec
{
    int         offset;
    int         cnt;
};

struct bond_rec
{
    int         atm_i;
    int         atm_j;
    int         parm_idx;
};

struct angle_rec
{
    int         atm_i;
    int         atm_j;
    int         atm_k;
    int         parm_idx;
};

struct dihed_rec
{
    int         atm_i;
    int         atm_j;
    int         atm_k;
    int         atm_l;
    int         parm_idx;
};

struct angle_ub_rec
{
    int         atm_i;
    int         atm_j;
    int         parm_idx;
};

struct dihed_imp_rec
{
    int         atm_i;
    int         atm_j;
    int         atm_k;
    int         atm_l;
    int         parm_idx;
};
struct cmap_rec
{
    int         atm_i;
    int         atm_j;
    int         atm_k;
    int         atm_l;
    int         atm_m;
    int         parm_idx;
};

struct shake_bond_rec
{
    int         atm_i;
    int         atm_j;
    double      parm;
};

struct gb_pot_ene_rec
{
    double total;
    double vdw_tot;
    double elec_tot;
    double gb;
    double surf;
    double bond;
    double angle;
    double dihedral;
    double vdw_14;
    double elec_14;
    double restraint;
    double angle_ub;
    double imp;
    double cmap;
    double dvdl;
    double amd_boost;
    double gamd_boost;
};

struct pme_pot_ene_rec
{
    double total;
    double vdw_tot;      // total of dir, recip
    double vdw_dir;
    double vdw_recip;
    double elec_tot;     // total of dir, recip, nb_adjust, self
    double elec_dir;
    double elec_recip;
    double elec_nb_adjust;
    double elec_self;
    double hbond;
    double bond;
    double angle;
    double dihedral;
    double vdw_14;
    double elec_14;
    double restraint;
    double angle_ub;
    double imp;
    double cmap;
    double amd_boost;
    double gamd_boost;
    double emap;
    double efield;
};

struct NTPData
{
    double last_recip[9];
    double recip[9];
    double ucell[9];
    PMEFloat recipf[9];
    PMEFloat ucellf[9];
    PMEFloat one_half_nonbond_skin_squared;
    PMEFloat cutPlusSkin2;
};

struct ep_frame_rec
{
    int extra_pnt[2];
    int ep_cnt;
    int type;
    int parent_atm;
    int frame_atm1;
    int frame_atm2;
    int frame_atm3;
};

#define LSCALE  (1ll << 50)
#define ESCALE  (1ll << 30)
#define FSCALE  (1ll << 40)
#define DFSCALE (1ll << 44)
static const PMEDouble LATTICESCALE                 = (PMEDouble)LSCALE;
static const PMEFloat LATTICESCALEF                 = (PMEFloat)LSCALE;
static const PMEDouble ONEOVERLATTICESCALE          = (PMEDouble)1.0 / (PMEDouble)LSCALE;
static const PMEFloat ONEOVERLATTICESCALEF          = (PMEDouble)1.0 / (PMEDouble)LSCALE;
static const PMEDouble ENERGYSCALE                  = (PMEDouble)ESCALE;
static const PMEFloat ENERGYSCALEF                  = (PMEFloat)ESCALE;
static const PMEDouble ONEOVERENERGYSCALE           = (PMEDouble)1.0 / (PMEDouble)ESCALE;
static const PMEDouble ONEOVERENERGYSCALESQUARED    = (PMEDouble)1.0 / ((PMEDouble)ESCALE * (PMEDouble)ESCALE);
static const PMEDouble FORCESCALE                   = (PMEDouble)FSCALE;
static const PMEFloat FORCESCALEF                   = (PMEFloat)FSCALE;
static const PMEDouble ONEOVERFORCESCALE            = (PMEDouble)1.0 / (PMEDouble)FSCALE;
static const PMEFloat ONEOVERFORCESCALEF            = (PMEDouble)1.0 / (PMEDouble)FSCALE;
static const PMEDouble ONEOVERFORCESCALESQUARED     = (PMEDouble)1.0 / ((PMEDouble)FSCALE * (PMEDouble)FSCALE);
static const PMEDouble DIHEDRALFORCESCALE           = (PMEDouble)DFSCALE;
static const PMEFloat DIHEDRALFORCESCALEF           = (PMEFloat)DFSCALE;
static const PMEDouble ONEOVERDIHEDRALFORCESCALE    = (PMEDouble)1.0 / (PMEDouble)DFSCALE;
static const PMEFloat ONEOVERDIHEDRALFORCESCALEF    = (PMEDouble)1.0 / (PMEDouble)DFSCALE;

struct KineticEnergyRecord
{
    PMEFloat EKE;
    PMEFloat EKPH;
    PMEFloat EKPBS;
};

union KineticEnergy
{
    struct KineticEnergyRecord KE;
    PMEFloat array[3];
};

//#define GVERBOSE
//#define MEMTRACKING
//#define SYNCHRONOUS
#ifdef GVERBOSE
#ifndef MEMTRACKING
#define MEMTRACKING
#endif
#ifdef MPI
#define PRINTMETHOD(name) \
{ \
    printf("Method: %s on node %d\n", name, gpu->gpuID); \
    fflush(stdout); \
}

#ifdef SYNCHRONOUS
#define LAUNCHERROR(s) \
    { \
        printf("Launched %s on node %d\n", s, gpu->gpuID); \
        cudaError_t status = cudaGetLastError(); \
        if (status != cudaSuccess) { \
            printf("Error: %s launching kernel %s\n", cudaGetErrorString(status), s); \
            gpu_shutdown_(); \
            exit(-1); \
        } \
        cudaThreadSynchronize(); \
    }
#else
#define LAUNCHERROR(s) \
    { \
        printf("Launched %s on node %d\n", s, gpu->gpuID); \
        cudaError_t status = cudaGetLastError(); \
        if (status != cudaSuccess) { \
            printf("Error: %s launching kernel %s\n", cudaGetErrorString(status), s); \
            gpu_shutdown_(); \
            exit(-1); \
        } \
    }
#endif
#define LAUNCHERROR_BLOCKING(s) \
    { \
        printf("Launched %s on node %d\n", s, gpu->gpuID); \
        cudaError_t status = cudaGetLastError(); \
        if (status != cudaSuccess) { \
            printf("Error: %s launching kernel %s\n", cudaGetErrorString(status), s); \
            gpu_shutdown_(); \
            exit(-1); \
        } \
        cudaThreadSynchronize(); \
    }
#define LAUNCHERROR_NONBLOCKING(s) \
    { \
        printf("Launched %s on node %d\n", s, gpu->gpuID); \
        cudaError_t status = cudaGetLastError(); \
        if (status != cudaSuccess) { \
            printf("Error: %s launching kernel %s\n", cudaGetErrorString(status), s); \
            gpu_shutdown_(); \
            exit(-1); \
        } \
    }
#else
#define PRINTMETHOD(name) \
{ \
    printf("Method: %s\n", name); \
    fflush(stdout); \
}
#ifdef SYNCHRONOUS
#define LAUNCHERROR(s) \
    { \
        printf("Launched %s\n", s); \
        cudaError_t status = cudaGetLastError(); \
        if (status != cudaSuccess) { \
            printf("Error: %s launching kernel %s\n", cudaGetErrorString(status), s); \
            gpu_shutdown_(); \
            exit(-1); \
        } \
        cudaThreadSynchronize(); \
    }
#else
#define LAUNCHERROR(s) \
    { \
        printf("Launched %s\n", s); \
        cudaError_t status = cudaGetLastError(); \
        if (status != cudaSuccess) { \
            printf("Error: %s launching kernel %s\n", cudaGetErrorString(status), s); \
            gpu_shutdown_(); \
            exit(-1); \
        } \
    }
#endif
#define LAUNCHERROR_BLOCKING(s) \
    { \
        printf("Launched %s\n", s); \
        cudaError_t status = cudaGetLastError(); \
        if (status != cudaSuccess) { \
            printf("Error: %s launching kernel %s\n", cudaGetErrorString(status), s); \
            gpu_shutdown_(); \
            exit(-1); \
        } \
        cudaThreadSynchronize(); \
    }
#define LAUNCHERROR_NONBLOCKING(s) \
    { \
        printf("Launched %s\n", s); \
        cudaError_t status = cudaGetLastError(); \
        if (status != cudaSuccess) { \
            printf("Error: %s launching kernel %s\n", cudaGetErrorString(status), s); \
            gpu_shutdown_(); \
            exit(-1); \
        } \
    }
#endif
#else
#define PRINTMETHOD(name)
#ifdef SYNCHRONOUS
#define LAUNCHERROR(s) \
{ \
        cudaError_t status = cudaGetLastError(); \
        if (status != cudaSuccess) { \
            printf("Error: %s launching kernel %s\n", cudaGetErrorString(status), s); \
            gpu_shutdown_(); \
            exit(-1); \
        } \
        cudaThreadSynchronize(); \
    }
#else
#define LAUNCHERROR(s) \
{ \
        cudaError_t status = cudaGetLastError(); \
        if (status != cudaSuccess) { \
            printf("Error: %s launching kernel %s\n", cudaGetErrorString(status), s); \
            gpu_shutdown_(); \
            exit(-1); \
        } \
    }
#endif
#define LAUNCHERROR_BLOCKING(s) \
{ \
        cudaError_t status = cudaGetLastError(); \
        if (status != cudaSuccess) { \
            printf("Error: %s launching kernel %s\n", cudaGetErrorString(status), s); \
            gpu_shutdown_(); \
            exit(-1); \
        } \
        cudaThreadSynchronize(); \
    }
#define LAUNCHERROR_NONBLOCKING(s) \
{ \
        cudaError_t status = cudaGetLastError(); \
        if (status != cudaSuccess) { \
            printf("Error: %s launching kernel %s\n", cudaGetErrorString(status), s); \
            gpu_shutdown_(); \
            exit(-1); \
        } \
    }
#endif

#define RTERROR(status, s) \
    if (status != cudaSuccess) { \
        printf("%s %s\n", s, cudaGetErrorString(status)); \
        cudaThreadExit(); \
        exit(-1); \
    }

struct cudaSimulation {
    int                         grid;                               // Grid size
    int                         gridBits;                           // Grid bit count
    int                         atoms;                              // Total number of atoms
    int                         paddedNumberOfAtoms;                // Atom count padded out to fit grid size

    // AMBER parameters
    int                         ntp;                                // AMBER constant pressure setting
    int                         barostat;                           // AMBER constant pressure barostat
    int                         alpb;                               // Analytical Linearized Poisson Boltzmann setting
    int                         igb;                                // Generalized Born overall setting
    int                         icnstph;                            // constant pH flag
    int                         ti_mode;                            // Softcore TI mode
    double                      scnb;                               // 1-4 nonbond scale factor
    double                      scee;                               // 1-4 electrostatic scale factor
    PMEFloat                    cut;                                // Nonbond interaction cutoff
    PMEFloat                    cut2;                               // Nonbond interaction cutoff squared
    PMEFloat                    cutinv;                             // Nonbonded interaction cutoff inverse
    PMEFloat                    cut2inv;                            // Nonbonded interaction cutoff inverse squared
    PMEFloat                    cut3inv;                            // Nonbonded interaction cutoff inverse cubed
    PMEFloat                    cut6inv;                            // Nonbonded interaction cutoff inverse to the sixth power
    PMEFloat                    cut3;                               // Nonbonded interaction cutoff cubed
    PMEFloat                    cut6;                               // Nonbonded interaction cutoff to the sixth power
    PMEFloat                    fswitch;                            // VDW force switch constant
    PMEFloat                    fswitch2;                           // VDW force switch constant squared
    PMEFloat                    fswitch3;                           // VDW force switch constant cubed
    PMEFloat                    fswitch6;                           // VDW force switch constant to the sixth power
    PMEFloat                    invfswitch6cut6;                    // VDW force switch constant 1 / (fswitch^6*cut^6)
    PMEFloat                    invfswitch3cut3;                    // VDW force switch constant 1 / (fswitch^3*cut^3)
    PMEFloat                    cut6invcut6minfswitch6;             // VDW force switch constant cut6 / (cut6-fswitch6)
    PMEFloat                    cut3invcut3minfswitch3;             // VDW force switch constant cut3 / (cut3-fswitch3)
    PMEFloat                    cutPlusSkin;                        // Nonbond interaction cutooff plus skin
    PMEFloat                    cutPlusSkin2;                       // Nonbond interaction cutooff plus skin squared
    int                         efn;                                // Normalize electric field vectors
    PMEFloat                    efx;                                // Electric field x vector
    PMEFloat                    efy;                                // Electric field y vector
    PMEFloat                    efz;                                // Electric field z vector
    PMEFloat                    efphase;                            // Electric field spatial phase
    PMEFloat                    effreq;                             // Electric field time frequency
    double                      dielc;                              // Dielectric constant
    double                      gamma_ln;                           // Langevin integration parameter
    double                      c_ave;                              // Langevin integration parameter
    double                      c_implic;                           // Langevin integration parameter
    double                      c_explic;                           // Langevin integration parameter
    bool                        bUseVlimit;                         // Use vlimit flag
    double                      vlimit;                             // vlimit
    double                      tol;                                // SHAKE tolerance
    double                      massH;                              // Hydrogen mass
    aligned_double              invMassH;                           // Inverse hydrogen mass
    PMEFloat                    gb_alpha;                           // Generalized Born parameter
    PMEFloat                    gb_beta;                            // Generalized Born parameter
    PMEFloat                    gb_gamma;                           // Generalized Born parameter
    PMEFloat                    gb_kappa;                           // Generalized Born parameter
    PMEFloat                    gb_kappa_inv;                       // Generalized Born derived parameter
    PMEFloat                    gb_cutoff;                          // Generalized Born cutoff
    PMEFloat                    gb_fs_max;                          // Generalized Born something or other
    PMEFloat                    gb_neckscale;                       // Generalized Born neck scaling factor
    PMEFloat                    gb_neckcut;                         // Generalized Born neck cutoff
    PMEFloat                    gb_neckoffset;                      // Generalized Born neck offset for LUT index
    PMEFloat                    rgbmax;                             // Generalized Born Born radius cutoff
    PMEFloat                    rgbmax1i;                           // Inverse Generalized Born Radius cutoff
    PMEFloat                    rgbmax2i;                           // Inverse Generalized Born Radius cutoff
    PMEFloat                    rgbmaxpsmax2;                       // Generalized Born derived quantity
    PMEFloat                    intdiel;                            // Generalized Born interior dielectric
    PMEFloat                    extdiel;                            // Generalized Born exterior dielectric
    PMEFloat                    alpb_alpha;                         // Generalized Born parameter
    PMEFloat                    alpb_beta;                          // Generalized Born derived parameter
    PMEFloat                    extdiel_inv;                        // Generalized Born derived parameter
    PMEFloat                    intdiel_inv;                        // Generalized Born derived parameter
    PMEFloat                    saltcon;                            // Generalized Born salt conductivity
    PMEFloat                    surften;                            // Generalized Born surface tension
    PMEFloat                    offset;                             // Generalized Born Born radius offset
    PMEFloat                    arad;                               // Generalized Bown atomic radius
    PMEFloat                    one_arad_beta;                      // Generalized Born derived parameters

    // PME parameters
    PMEDouble                   a;                                  // PBC x cell length
    PMEDouble                   b;                                  // PBC y cell length
    PMEDouble                   c;                                  // PBC z cell length
    PMEFloat                    af;                                 // PBC single-precision x cell length
    PMEFloat                    bf;                                 // PBC single-precision y cell length
    PMEFloat                    cf;                                 // PBC single-precision z cell length
    PMEFloat                    alpha;                              // PBC Alpha
    PMEFloat                    beta;                               // PBC Beta
    PMEFloat                    gamma;                              // PBC Gamma
    PMEFloat                    pbc_box[3];                         // PBC Box
    PMEFloat                    reclng[3];                          // PBC reciprocal cell lengths
    PMEFloat                    cut_factor[3];                      // PBC cut factors
    PMEFloat                    ucell[3][3];                        // PBC cell coordinate system
    aligned_double              recip[3][3];                        // PBC reciprocal cell coordinate system
    PMEFloat                    recipf[3][3];                       // Single precision PBC reciprocal cell coordinate system
    PMEFloat                    uc_volume;                          // PBC cell volume
    PMEFloat                    uc_sphere;                          // PBC bounding sphere
    PMEFloat                    pi_vol_inv;                         // PBC/PME constant
    PMEFloat                    fac;                                // PME Ewald factor
    PMEFloat                    fac2;                               // PME Ewald factor x 2
    bool                        is_orthog;                          // PBC cell orthogonality flag
    int                         nfft1;                              // x fft grid size
    int                         nfft2;                              // y fft grid size
    int                         nfft3;                              // z fft grid size
    int                         nfft1xnfft2;                        // Product of nfft1 and nfft2
    int                         nfft1xnfft2xnfft3;                  // Product of nfft1, nfft2 and nfft3
    int                         fft_x_dim;                          // x fft dimension
    int                         fft_y_dim;                          // y fft dimension
    int                         fft_z_dim;                          // z fft dimension
    int                         fft_y_dim_times_x_dim;              // Product of x and y dimension
    int                         nf1;                                // x scalar sum coefficient
    int                         nf2;                                // y scalar sum coefficient
    int                         nf3;                                // z scalar sum coefficient
    int                         n2Offset;                           // Offset to y prefac data
    int                         n3Offset;                           // Offset to z prefac data
    int                         nSum;                               // Total prefac data
    int                         orderMinusOne;                      // Interpolation order - 1
    int                         XYZStride;                          // Stride of PME buffers
    PMEFloat                    ew_coeff;                           // Ewald coefficient
    PMEFloat                    ew_coeff2;                          // Ewald coefficient squared
    PMEFloat                    negTwoEw_coeffRsqrtPI;              // Switching function constant

    // IPS paramers
    bool                        bIPSActive;                         // Flag to indicate IPS is active
    PMEFloat                    rips;                               // Radius of IPS local region
    PMEFloat                    rips2;                              // rips^2
    PMEFloat                    ripsr;                              // 1/rips
    PMEFloat                    rips2r;                             // 1/rips^2
    PMEFloat                    rips6r;                             // 1/rips^6
    PMEFloat                    rips12r;                            // 1/rips^12
    PMEDouble                   eipssnb;                            // IPS self VDW energy
    PMEDouble                   eipssel;                            // IPS self-electrostatic energy
    PMEDouble                   virips;                             // IPS self-virial energy
    PMEDouble                   EIPSEL;                             // IPS exclusion self electrostatic energy
    PMEDouble                   EIPSNB;                             // IPS exclusion self Nonbond energy

    //  Electrostatic IPS parameters:
    PMEFloat                    aipse0;
    PMEFloat                    aipse1;
    PMEFloat                    aipse2;
    PMEFloat                    aipse3;
    PMEFloat                    pipsec;
    PMEFloat                    pipse0;
    PMEFloat                    bipse1;
    PMEFloat                    bipse2;
    PMEFloat                    bipse3;

    //  Dispersion IPS parameters:
    PMEFloat                    aipsvc0;
    PMEFloat                    aipsvc1;
    PMEFloat                    aipsvc2;
    PMEFloat                    aipsvc3;
    PMEFloat                    pipsvcc;
    PMEFloat                    pipsvc0;
    PMEFloat                    bipsvc1;
    PMEFloat                    bipsvc2;
    PMEFloat                    bipsvc3;

    //  Repulsion IPS parameters:
    PMEFloat                    aipsva0;
    PMEFloat                    aipsva1;
    PMEFloat                    aipsva2;
    PMEFloat                    aipsva3;
    PMEFloat                    pipsvac;
    PMEFloat                    pipsva0;
    PMEFloat                    bipsva1;
    PMEFloat                    bipsva2;
    PMEFloat                    bipsva3;

    // AMD parameters:
    int                         iamd;
    int                         w_amd;
    int                         iamdlag;
    int                         amd_print_interval;
    int                         AMDNumRecs;
    int                         AMDNumLag;
    PMEDouble                   AMDtboost;
    PMEDouble                   AMDfwgt;
    PMEUllInt*                  pAMDEDihedral;
    PMEDouble*                  pAMDfwgtd;
    PMEDouble                   amd_EthreshP;
    PMEDouble                   amd_alphaP;
    PMEDouble                   amd_EthreshD;
    PMEDouble                   amd_alphaD;
    PMEDouble                   amd_temp0;
    PMEDouble                   amd_EthreshP_w;
    PMEDouble                   amd_alphaP_w;
    PMEDouble                   amd_EthreshD_w;
    PMEDouble                   amd_alphaD_w;
    PMEDouble                   amd_w_sign;

    // GaMD parameters:
    int                         igamd;
    int                         tspanP;
    int                         tspanD;
    int                         tspan;
    int                         igamdlag;
    int                         gamd_print_interval;
    int                         GaMDNumRecs;
    int                         GaMDNumLag;
    PMEDouble                   GaMDtboost;
    PMEDouble                   GaMDfwgt;
    PMEUllInt*                  pGaMDEDihedral;
    PMEDouble*                  pGaMDfwgtd;
    PMEDouble                   gamd_EthreshP;
    PMEDouble                   gamd_kP;
    PMEDouble                   gamd_EthreshD;
    PMEDouble                   gamd_kD;
    PMEDouble                   gamd_temp0;

    // scaledMD parameters:
    int                         scaledMD;
    PMEDouble                   scaledMD_lambda;
    PMEDouble                   scaledMD_energy;
    PMEDouble                   scaledMD_weight;
    PMEDouble                   scaledMD_unscaled_energy;

    // For relaxation dynamics
    int                         first_update_atom;                  // The first atom whose crds should be updated

    // NTP stuff
    NTPData*                    pNTPData;                           // PME NTP mutable values

    // Constant pH stuff
    double*                     pChargeRefreshBuffer;               // Pointer to new charges

    // Softcore TI stuff
    double                      lambda[2];
    PMEFloat                    lambdaSP[2];
    double                      scalpha;
    double                      scbeta;
    PMEFloat                    scalphaSP;
    PMEFloat                    scbetaSP;

#ifdef MPI
    // MPI stuff
    int                         minLocalAtom;                       // First local atom
    int                         maxLocalAtom;                       // Last local atom
#endif

    // Atom stuff
    double*                     pAtomX;                             // Atom X coordinates
    double*                     pAtomY;                             // Atom Y coordinates
    double*                     pAtomZ;                             // Atom Z coordinates
    double*                     pOldAtomX;                          // Old Atom X position
    double*                     pOldAtomY;                          // Old Atom Y position
    double*                     pOldAtomZ;                          // Old Atom Z position
    PMEFloat2*                  pAtomXYSP;                          // Single Precision Atom X and Y coordinates
    PMEFloat*                   pAtomZSP;                           // Single Precision Atom Z coordinates
    PMEFloat2*                  pAtomSigEps;                        // Atom nonbond parameters
    unsigned int*               pAtomLJID;                          // Atom Lennard-Jones index
    PMEFloat*                   pAtomS;                             // Atom S Parameter
    PMEFloat*                   pAtomRBorn;                         // Atom Born Radius
    double*                     pAtomCharge;                        // Atom charges
    PMEFloat*                   pAtomChargeSP;                      // Single precision atom charges
    PMEFloat2*                  pAtomChargeSPLJID;                  // Single precision atom charges and Lennard Jones atom type
    double*                     pAtomMass;                          // Atom masses
    double*                     pAtomInvMass;                       // Atom inverse masses
    int*                        pTIRegion;                          // Softcore TI Region
    PMEDouble*                  pReff;                              // Effective Born Radius
    PMEFloat*                   pPsi;                               // GB intermediate psi value
    PMEFloat*                   pReffSP;                            // Single Precision Effective Born Radius
    PMEFloat*                   pTemp7;                             // Single Precision Born Force
#ifdef MPI
    PMEAccumulator*             pPeerAccumulator;                   // Remote data accumulator
#endif
    double*                     pVelX;                              // Atom X velocities
    double*                     pVelY;                              // Atom Y velocities
    double*                     pVelZ;                              // Atom Z velocities
    double*                     pLVelX;                             // Atom X last velocities
    double*                     pLVelY;                             // Atom Y last velocities
    double*                     pLVelZ;                             // Atom Z last velocities
    int                         maxNonbonds;                        // Maximum nonbond interaction buffers
    PMEFloat*                   pXMax;                              // Recentering maximum x
    PMEFloat*                   pYMax;                              // Recentering maximum y
    PMEFloat*                   pZMax;                              // Recentering maximum z
    PMEFloat*                   pXMin;                              // Recentering minimum x
    PMEFloat*                   pYMin;                              // Recentering minimum y
    PMEFloat*                   pZMin;                              // Recentering minimum z

    // Neighbor List stuff
    PMEFloat                    skinnb;                             // Input Nonbond skin
    unsigned int*               pImageIndex;                        // Image # for spatial sort
    unsigned int*               pImageIndex2;                       // Image # for spatial sort
    unsigned int*               pImageHash;                         // Image hash for spatial sort
    unsigned int*               pImageHash2;                        // Image hash for spatial sort
    unsigned int*               pImageAtom;                         // Image atom #
    unsigned int*               pImageAtom2;                        // Image atom #
    unsigned int*               pImageAtomLookup;                   // Original atom lookup table
    PMEFloat2*                  pAtomXYSaveSP;                      // Saved atom coordinates from neighbor list generation
    PMEFloat*                   pAtomZSaveSP;                       // Saved atom coordinates from neighbor list generation
    double*                     pImageX;                            // Image x coordinates
    double*                     pImageY;                            // Image y coordinates
    double*                     pImageZ;                            // Image z coordinates
    double*                     pImageVelX;                         // Image x velocities
    double*                     pImageVelY;                         // Image y velocities
    double*                     pImageVelZ;                         // Image z velocities
    double*                     pImageLVelX;                        // Image last x velocities
    double*                     pImageLVelY;                        // Image last y velocities
    double*                     pImageLVelZ;                        // Image last z velocities
    double*                     pImageMass;                         // Image masses
    double*                     pImageInvMass;                      // Image inverse masses
    double*                     pImageCharge;                       // Image charges
    PMEFloat2*                  pImageSigEps;                       // Image sigma/epsilon data for nonbond interactions
    unsigned int*               pImageLJID;                         // Image Lennard-Jones index
    unsigned int*               pImageCellID;                       // Image cell ID for calculating local coordinate system
    int*                        pImageTIRegion;                     // Image Softcore TI data
    double*                     pImageX2;                           // Image x coordinates
    double*                     pImageY2;                           // Image y coordinates
    double*                     pImageZ2;                           // Image z coordinates
    double*                     pImageVelX2;                        // Image x velocities
    double*                     pImageVelY2;                        // Image y velocities
    double*                     pImageVelZ2;                        // Image z velocities
    double*                     pImageLVelX2;                       // Image last x velocities
    double*                     pImageLVelY2;                       // Image last y velocities
    double*                     pImageLVelZ2;                       // Image last z velocities
    double*                     pImageMass2;                        // Image masses
    double*                     pImageInvMass2;                     // Image inverse masses
    double*                     pImageCharge2;                      // Image charges
    PMEFloat2*                  pImageSigEps2;                      // Image sigma/epsilon data for nonbond interactions
    unsigned int*               pImageLJID2;                        // Image Lennard-Jones index
    unsigned int*               pImageCellID2;                      // Image cell ID for calculating local coordinate system
    int*                        pImageTIRegion2;                    // Image Softcore TI data
    int2*                       pImageBondID;                       // Remapped Bond i, j
    int2*                       pImageBondAngleID1;                 // Remapped Bond Angle i, j
    int*                        pImageBondAngleID2;                 // Remapped Bond Angle k;
    int4*                       pImageDihedralID1;                  // Remapped Dihedral i, j, k, l
    int2*                       pImageNb14ID;                       // Remapped 1-4 nonbond i, j
    int*                        pImageConstraintID;                 // Remapped Atom constraint ID
    int2*                       pImageUBAngleID;                    // Remapped Urey Bradley Angle i, j
    int4*                       pImageImpDihedralID1;               // Remapped Improper Dihedral i, j, k, l
    int4*                       pImageCmapID1;                      // Remapped Cmap i, j, k, l
    int*                        pImageCmapID2;                      // Remapped Cmap m
    int2*                       pImageNMRDistanceID;                // Remapped NMR Distance i, j
    int2*                       pImageNMRCOMDistanceID;             // Remapped NMR COM Distance i, j
    int2*                       pImageNMRCOMDistanceCOM;            // Remapped NMR Distance COM range .x to .y
    int2*                       pImageNMRCOMDistanceCOMGrp;         // Remapped NMR Distance COM group range .x to .y
// BUMP POTENTIAL
    int2*                       pImageNMRCOMBumpID;                 // Remapped NMR COM Distance i, j
    int2*                       pImageNMRCOMBumpCOM;                // Remapped NMR Distance COM range .x to .y
    int2*                       pImageNMRCOMBumpCOMGrp;             // Remapped NMR Distance COM group range .x to .y
// BUMP POTENTIAL
    int2*                       pImageNMRr6avDistanceID;            // Remapped NMR r6av Distance i, j
    int2*                       pImageNMRr6avDistancer6av;          // Remapped NMR Distance r6av range .x to .y
    int2*                       pImageNMRr6avDistancer6avGrp;       // Remapped NMR Distance r6av group range .x to .y
    int2*                       pImageNMRAngleID1;                  // Remapped NMR Angle i, j
    int*                        pImageNMRAngleID2;                  // Remapped NMR Angle k
    int4*                       pImageNMRTorsionID1;                // Remapped NMR Torsion i, j, k, l
    int4*                       pImageShakeID;                      // Remapped Shake Atom ID
    int4*                       pImageFastShakeID;                  // Remapped Fast Shake Atom ID
    int*                        pImageSlowShakeID1;                 // Remapped Slow Shake central Atom ID
    int4*                       pImageSlowShakeID2;                 // Remapped Slow Shake hydrogen Atom IDs
    int4*                       pImageSolventAtomID;                // Remapped solvent molecules/ions
    int*                        pImageSoluteAtomID;                 // Remapped solute atoms
    uint2*                      pNLNonbondCellStartEnd;             // Nonbond cell boundaries pointer
    int                         maxChargeGridBuffers;               // Total possible charge grid buffers
    int                         cells;                              // Total number of nonbond cells
    int                         xcells;                             // Number of x cells
    int                         ycells;                             // Number of y cells
    int                         zcells;                             // Number of z cells
    PMEDouble                   xcell;                              // x cell dimension
    PMEDouble                   ycell;                              // y cell dimension
    PMEDouble                   zcell;                              // z cell dimension
    PMEDouble                   minCellX;                           // Minimum x cell coordinate
    PMEDouble                   minCellY;                           // Minimum y cell coordinate
    PMEDouble                   minCellZ;                           // Minimum z cell coordinate
    PMEDouble                   maxCellX;                           // Maximum x cell coordinate
    PMEDouble                   maxCellY;                           // Maximum y cell coordinate
    PMEDouble                   maxCellZ;                           // Maximum z cell coordinate
    aligned_double              oneOverXcells;                      // Fractional x cell dimension
    aligned_double              oneOverYcells;                      // Fractional y cell dimension
    aligned_double              oneOverZcells;                      // Fractional z cell dimension
    PMEFloat                    oneOverXcellsf;                     // Single precision fractional x cell dimension
    PMEFloat                    oneOverYcellsf;                     // Single precision fractional y cell dimension
    PMEFloat                    oneOverZcellsf;                     // Single precision fractional z cell dimension
    PMEFloat                    cell;                               // minimum cell dimension
    PMEFloat                    nonbond_skin;                       // Effective nonbond skin
    PMEFloat                    one_half_nonbond_skin_squared;      // Skin test atom movement threshold
    bool*                       pNLbSkinTestFail;                   // Skin test result buffer
    unsigned int*               pNLCellHash;                        // Spatial ordering hash for within cells
    unsigned int*               pNLTotalOffset;                     // Pointer to total offset
    int                         NLMaxTotalOffset;                   // Maximum available exclusion masks
    unsigned int*               pNLAtomList;                        // Pointer to neighbor list atoms
    unsigned int*               pNLExclusionList;                   // Pointer to list of nonbond exclusions
    uint2*                      pNLExclusionStartCount;             // Pointer to per-atom exclusions
    unsigned int                NLExclusions;                       // Total number of exclusions
    unsigned int                NLAtoms;                            // Total number of neighbor list atoms
    NLRecord*                   pNLRecord;                          // Pointer to neighbor list records
    NLEntry*                    pNLEntry;                           // Active neighbor list
    unsigned int*               pNLEntries;                         // Number of entries in current neighbor list
    unsigned int*               pNLOffset;                          // Pointer to atom/exclusion offsets
    unsigned int*               pNLPosition;                        // Position in building/traversing neighbor list
    unsigned int                NLBuildWarps;                       // Number of warps in neighbor list build
    unsigned int                NLNonbondEnergyWarps;               // Number of warps in nonbond energy calculation
    unsigned int                NLNonbondForcesWarps;               // Number of warps in nonbond forces calculation
    unsigned int                NLCellBuffers;                      // Number of nonbond cell buffers
    unsigned int                NLRecords;                          // Number of neighbor list records
    unsigned int                NLMaxEntries;                       // Maximum allow entries in neighbor list (~5x expected)

    unsigned int                NLXEntryWidth;                      // Number of x atoms per NL entry
    unsigned int                NLYDivisor;                         // Number of y subdivisions per neighor list record
    unsigned int                NLYStride;                          // Neighbor List build vertical atom stride (NLYDivisor * NLAtomsPerWarp)
    unsigned int                NLEntryTypes;                       // Neighbor List entry types (used to determine output buffers offset)
    unsigned int                NLHomeCellBuffer;                   // Register atom buffer
    unsigned int                NLAtomsPerWarp;                     // Number of atoms to process in each warp's registers
    unsigned int                NLAtomsPerWarpBits;                 // Number of bits in atoms to toprocess in each warp's registers
    unsigned int                NLAtomsPerWarpBitsMask;             // NLAtomsPerWarp - 1
    unsigned int                NLAtomsPerWarpMask;                 // First AtomsPerWarp worth of bits set to 1
    unsigned int                NLOffsetPerWarp;                    // Number of entries needed to process 1 warp iteration's worth of nonbond forces
    unsigned int                NLMaxExclusionsPerWarp;             // Number of atoms to process in each warp's registers
    unsigned int                NLExclusionBufferSize;              // Exclusion buffer size for L1 cache-capable GPUs
    unsigned int                maxNonbondBuffers;                  // maximum nonbond buffers for NTP simulation
    unsigned int*               pBNLExclusionBuffer;                // Per-warp GMEM exclusion buffer

    // LJ data
    PMEFloat2*                  pLJTerm;                            // Lennard-Jones terms
    int                         LJTerms;                            // Number of Lennard-Jones terms
    int                         LJTypes;                            // Number of Lennard-Jones types
    int                         LJOffset;                           // Offset to SCTI LJ Terms

    // GB Data
    PMEFloat2*                  pNeckMaxValPos;                     // GB Born Radii and energy correction data
    PMEFloat*                   pgb_alpha;                          // Pointer to per-atom GB alpha
    PMEFloat*                   pgb_beta;                           // Pointer to per-atom GB beta
    PMEFloat*                   pgb_gamma;                          // Pointer to per-atom GB gamma

    // PME data
    long long int*              plliXYZ_q;                          // Input PME charge grid
    PMEFloat*                   pXYZ_q;                             // PME charge grid/buffer
    PMEComplex*                 pXYZ_qt;                            // FFTed PME charge grid
    PMEFloat*                   pPrefac1;                           // PME nfft1 pre-factors
    PMEFloat*                   pPrefac2;                           // PME nfft2 pre-factors
    PMEFloat*                   pPrefac3;                           // PME nfft3 pre-factors
    PMEFloat*                   pFractX;                            // PME unit cell fractional x coordinates
    PMEFloat*                   pFractY;                            // PME unit cell fractional y coordinates
    PMEFloat*                   pFractZ;                            // PME unit cell fractional z coordinates

    // NTP molecule data
    int                         soluteMolecules;                    // Total solute molecules
    int                         soluteMoleculeStride;               // Total solute molecule stride
    int                         soluteAtoms;                        // Total solute atoms
    int                         soluteAtomsOffset;                  // Used for remapping neighbor list data
    int                         solventMolecules;                   // Total solvent molecules
    int                         solventMoleculeStride;              // Total solvent molecules padded to warp width
    int*                        pSoluteAtomMoleculeID;              // List of solute molecule IDs
    int*                        pSoluteAtomID;                      // List of solute atom IDs
    PMEDouble*                  pSoluteAtomMass;                    // Solute atom masses
    PMEDouble*                  pSoluteCOMX;                        // X Last centers of mass for each solute molecule
    PMEDouble*                  pSoluteCOMY;                        // Y Last centers of mass for each solute molecule
    PMEDouble*                  pSoluteCOMZ;                        // Z Last centers of mass for each solute molecule
    PMEDouble*                  pSoluteDeltaCOMX;                   // X change in center of mass for each solute molecule
    PMEDouble*                  pSoluteDeltaCOMY;                   // Y change in center of mass for each solute molecule
    PMEDouble*                  pSoluteDeltaCOMZ;                   // Z change in center of mass for each solute molecule
    PMEUllInt*                  pSoluteUllCOMX;                     // X Current center of mass for each solute molecule
    PMEUllInt*                  pSoluteUllCOMY;                     // Y Current center of mass for each solute molecule
    PMEUllInt*                  pSoluteUllCOMZ;                     // Z Current center of mass for each solute molecule
    PMEUllInt*                  pSoluteUllEKCOMX;                   // Pointer to x component of COM Kinetic energy buffer
    PMEUllInt*                  pSoluteUllEKCOMY;                   // Pointer to x component of COM Kinetic energy buffer
    PMEUllInt*                  pSoluteUllEKCOMZ;                   // Pointer to x component of COM Kinetic energy buffer
    PMEDouble*                  pSoluteInvMass;                     // Total Inverse mass for each solute molecule
    int4*                       pSolventAtomID;                     // List of solvent molecules/ions of 4 or fewer atoms
    PMEDouble*                  pSolventAtomMass1;                  // First solvent atom mass
    PMEDouble*                  pSolventAtomMass2;                  // Second solvent atom mass
    PMEDouble*                  pSolventAtomMass3;                  // Third solvent atom mass
    PMEDouble*                  pSolventAtomMass4;                  // Fourth solvent atom mass
    PMEDouble*                  pSolventCOMX;                       // X Last centers of mass for each solvent molecule
    PMEDouble*                  pSolventCOMY;                       // Y Last centers of mass for each solvent molecule
    PMEDouble*                  pSolventCOMZ;                       // Z Last centers of mass for each solvent molecule
    PMEDouble*                  pSolventInvMass;                    // Total inverse mass for eache solvent molecule

    // NTP molecule constraint data
    PMEDouble*                  pConstraintAtomX;                   // Pre-centered constraint atom x
    PMEDouble*                  pConstraintAtomY;                   // Pre-centered constraint atom y
    PMEDouble*                  pConstraintAtomZ;                   // Pre-centered constraint atom z
    PMEDouble*                  pConstraintCOMX;                    // Original center of mass X for constraint atom in fractional coordinates
    PMEDouble*                  pConstraintCOMY;                    // Original center of mass Y for constraint atom in fractional coordinates
    PMEDouble*                  pConstraintCOMZ;                    // Original center of mass Z for constraint atom in fractional coordinates




    // Energy and Virial Buffers
    unsigned long long int*     pEnergyBuffer;                      // Generic energy buffer pointer
    unsigned long long int*     pEELT;                              // Pointer to electrostatic energy
    unsigned long long int*     pEVDW;                              // Pointer to vdw energy
    unsigned long long int*     pEGB;                               // Pointer to Generalized Born energy
    unsigned long long int*     pEBond;                             // Pointer to bond energy
    unsigned long long int*     pEAngle;                            // Pointer to bond angle energy
    unsigned long long int*     pEDihedral;                         // Pointer to dihedral energy
    unsigned long long int*     pEEL14;                             // Pointer to 1-4 electrostatic energy
    unsigned long long int*     pENB14;                             // Pointer to 1-4 vdw energy
    unsigned long long int*     pERestraint;                        // Pointer to restraint energy
    unsigned long long int*     pEER;                               // Pointer to PME reciprocal electrostatic energy
    unsigned long long int*     pEED;                               // Pointer to PME direct electrostatic energy
    unsigned long long int*     pEAngle_UB;                         // Pointer to CHARMM Urey Bradley energy
    unsigned long long int*     pEImp;                              // Pointer to CHARMM improper dihedral energy
    unsigned long long int*     pECmap;                             // Pointer to CHARMM cmap energy
    unsigned long long int*     pENMRDistance;                      // Pointer to NMR distance energy
    unsigned long long int*     pENMRCOMDistance;                   // Pointer to NMR COM distance energy
// BUMP POTENTIAL
    unsigned long long int*     pENMRCOMBump;                       // Pointer to NMR COM bump distance energy
// BUMP POTENTIAL
    unsigned long long int*     pENMRr6avDistance;                  // Pointer to NMR r6av distance energy
    unsigned long long int*     pENMRAngle;                         // Pointer to NMR angle energy
    unsigned long long int*     pENMRTorsion;                       // Pointer to NMR torsion energy
//  unsigned long long int*     pESurf;                             // Pointer to GBSA surface energy
    unsigned long long int*     pEEField;                           // Pointer to Electric Field energy
    unsigned long long int*     pVirial;                            // Pointer to PME virial
    unsigned long long int*     pVirial_11;                         // Pointer to PME virial component
    unsigned long long int*     pVirial_22;                         // Pointer to PME virial component
    unsigned long long int*     pVirial_33;                         // Pointer to PME virial component
    unsigned long long int*     pEKCOMX;                            // Pointer to x component of PME center of mass kinetic energy
    unsigned long long int*     pEKCOMY;                            // Pointer to y component of PME center of mass kinetic energy
    unsigned long long int*     pEKCOMZ;                            // Pointer to z component of PME center of mass kinetic energy
    unsigned int                EnergyTerms;                        // Total energy terms
    unsigned int                EStride;                            // Stride between energy buffers for Softcore TI

    // Kinetic Energy Buffers
    KineticEnergy*              pKineticEnergy;                     // Pointer to per-block Kinetic Energy entries

    // Random Number stuff
    unsigned int                randomSteps;                        // Number of steps between RNG calls
    unsigned int                randomNumbers;                      // Number of randoms to generate per atom per RNG call
    double*                     pRandom;                            // Pointer to overall RNG buffer
    double*                     pRandomX;                           // Pointer to x random numbers
    double*                     pRandomY;                           // Pointer to y random numbers
    double*                     pRandomZ;                           // Pointer to z random numbers

    // Extra points stuff
    int                         EPs;
    int                         EP11s;
    int                         EP12s;
    int                         EP21s;
    int                         EP22s;
    int                         EP11Offset;
    int                         EP12Offset;
    int                         EP21Offset;
    int                         EP22Offset;
    int4*                       pExtraPoint11Frame;
    int*                        pExtraPoint11Index;
    double*                     pExtraPoint11X;
    double*                     pExtraPoint11Y;
    double*                     pExtraPoint11Z;
    int4*                       pExtraPoint12Frame;
    int*                        pExtraPoint12Index;
    double*                     pExtraPoint12X;
    double*                     pExtraPoint12Y;
    double*                     pExtraPoint12Z;
    int4*                       pExtraPoint21Frame;
    int2*                       pExtraPoint21Index;
    double*                     pExtraPoint21X1;
    double*                     pExtraPoint21Y1;
    double*                     pExtraPoint21Z1;
    double*                     pExtraPoint21X2;
    double*                     pExtraPoint21Y2;
    double*                     pExtraPoint21Z2;
    int4*                       pExtraPoint22Frame;
    int2*                       pExtraPoint22Index;
    double*                     pExtraPoint22X1;
    double*                     pExtraPoint22Y1;
    double*                     pExtraPoint22Z1;
    double*                     pExtraPoint22X2;
    double*                     pExtraPoint22Y2;
    double*                     pExtraPoint22Z2;
    int4*                       pImageExtraPoint11Frame;
    int*                        pImageExtraPoint11Index;
    int4*                       pImageExtraPoint12Frame;
    int*                        pImageExtraPoint12Index;
    int4*                       pImageExtraPoint21Frame;
    int2*                       pImageExtraPoint21Index;
    int4*                       pImageExtraPoint22Frame;
    int2*                       pImageExtraPoint22Index;

    // Shake constraint stuff
    unsigned int                shakeConstraints;                   // Traditional SHAKE constraints
    unsigned int                shakeOffset;                        // Offset to end of traditional SHAKE constraints
    unsigned int                fastShakeConstraints;               // Fast SHAKE constraints (H2O molecules)
    unsigned int                fastShakeOffset;                    // Offset to end of fast SHAKE constraints
    unsigned int                slowShakeConstraints;               // XH4 (really slow) SHAKE constraints
    unsigned int                slowShakeOffset;                    // Offset to end of slow SHAKE constraints
    int4*                       pShakeID;                           // SHAKE central atom plus up to 3 hydrogens
    double2*                    pShakeParm;                         // SHAKE central atom mass and equilibrium bond length
    double*                     pShakeInvMassH;                     // SHAKE HMR inverse hydrogen mass
    int4*                       pFastShakeID;                       // H2O oxygen plus two hydrogens atom ID
    int*                        pSlowShakeID1;                      // Central atom of XH4 Shake constraint
    int4*                       pSlowShakeID2;                      // XH4 SHAKE constraint hydrogens
    double2*                    pSlowShakeParm;                     // XH4 SHAKE central atom mass and equilibrium bond length
    double*                     pSlowShakeInvMassH;                 // XH4 SHAKE HMR inverse hydrogen mass
    aligned_double              ra;                                 // Fast SHAKE parameter
    aligned_double              ra_inv;                             // Fast SHAKE parameter
    aligned_double              rb;                                 // Fast SHAKE parameter
    aligned_double              rc;                                 // Fast SHAKE parameter
    aligned_double              rc2;                                // Fast SHAKE parameter
    aligned_double              hhhh;                               // Fast SHAKE parameter
    aligned_double              wo_div_wohh;                        // Fast SHAKE parameter
    aligned_double              wh_div_wohh;                        // Fast SHAKE parameter

    // Bonded interaction stuff
    int                         bonds;                              // Total number of bonds
    int                         bondOffset;                         // Offset to end of bondss
    int                         bondAngles;                         // Total number of bond angles
    int                         bondAngleOffset;                    // Offset to end of bond angles
    int                         dihedrals;                          // Total number of dihedrals
    int                         dihedralOffset;                     // Offset to end of dihedrals
    int                         nb14s;                              // Total number of 1-4 nonbond interactions
    int                         nb14Offset;                         // Offset to end of 1-4 nonbond interactions
    int                         constraints;                        // Number of positional constraints
    int                         constraintOffset;                   // Offset to end of positional constraints
    int                         UBAngles;                           // Total number of Urey Bradley angles
    int                         UBAngleOffset;                      // Offset to end of Urey Bradley Angles
    int                         impDihedrals;                       // Total number of improper dihedrals
    int                         impDihedralOffset;                  // Offset to end of improper dihedrals
    int                         cmaps;                              // Total number of cmap terms
    int                         cmapOffset;                         // Offset to end of cmap terms
    PMEDouble2*                 pBond;                              // Bond rk, req
    int2*                       pBondID;                            // Bond i, j
    PMEDouble2*                 pBondAngle;                         // Bond Angle Kt, teq
    int2*                       pBondAngleID1;                      // Bond Angle i, j
    int*                        pBondAngleID2;                      // Bond Angle k;
    PMEDouble2*                 pDihedral1;                         // Dihedral Ipn, pn
    PMEDouble2*                 pDihedral2;                         // Dihedral pk, gamc
    PMEDouble*                  pDihedral3;                         // Dihedral gams
    int4*                       pDihedralID1;                       // Dihedral i, j, k, l
    PMEDouble2*                 pNb141;                             // 1-4 nonbond scee, scnb0
    PMEDouble2*                 pNb142;                             // 1-4 nonbond cn1, cn2
    int2*                       pNb14ID;                            // 1-4 nonbond i, j
    PMEDouble2*                 pConstraint1;                       // Constraint weight and xc
    PMEDouble2*                 pConstraint2;                       // Constraint yc and zc
    int*                        pConstraintID;                      // Atom constraint ID

    // CHARMM interaction stuff
    PMEDouble2*                 pUBAngle;                           // Urey Bradley Angle rk, r0
    int2*                       pUBAngleID;                         // Urey Bradley Angle i, j
    PMEDouble2*                 pImpDihedral;                       // Improper Dihedral pk, phase
    int4*                       pImpDihedralID1;                    // Improper Dihedral i, j, k, l
    int4*                       pCmapID1;                           // Cmap i, j, k, l
    int*                        pCmapID2;                           // Cmap m
    int*                        pCmapType;                          // Cmap type

    // NMR refinement stuff
    int                         NMRDistances;                       // Number of NMR distance constraints
    int                         NMRCOMDistances;                    // Number of NMR COM distance constraints
    int                         NMRCOMBump;                         // Number of NMR COM distance bump constraints
    int                         NMRr6avDistances;                   // Number of NMR r6av distance constraints
    int                         NMRMaxgrp;                          // Number of NMR distance COM groupings
    int                         NMRDistanceOffset;                  // Offset to end of distance constraints
    int                         NMRCOMDistanceOffset;               // Offset to end of COM distance constraints
    int                         NMRCOMBumpOffset;                   // Offset to end of COM distance bump constraints
    int                         NMRr6avDistanceOffset;              // Offset to end of r6av distance constraints
    int                         NMRAngles;                          // Number of NMR angle constraints
    int                         NMRAngleOffset;                     // Offset to end of angle constraints
    int                         NMRTorsions;                        // Number of NMR torsion constraints
    int                         NMRTorsionOffset;                   // Offset to end of torsion constraints
    bool                        bJar;                               // Determines whether Jarzynski MD is active
    double                      drjar;                              // Jar increment
    double*                     pNMRJarData;                        // Jarzynski accumulated work data
    int2*                       pNMRDistanceID;                     // NMR distance i, j
    PMEDouble2*                 pNMRDistanceR1R2;                   // NMR distance computed r1, r2
    PMEDouble2*                 pNMRDistanceR3R4;                   // NMR distance computed r3, r4
    PMEDouble2*                 pNMRDistanceK2K3;                   // NMR distance computed k2, k3
    PMEDouble*                  pNMRDistanceAve;                    // NMR distance restraint linear and exponential averaged value
    PMEDouble2*                 pNMRDistanceTgtVal;                 // NMR distance target and value for current step
    int2*                       pNMRDistanceStep;                   // NMR distance first and last step for application of restraint
    int*                        pNMRDistanceInc;                    // NMR distance increment for step weighting
    PMEDouble2*                 pNMRDistanceR1R2Slp;                // NMR distance r1, r2 slope
    PMEDouble2*                 pNMRDistanceR3R4Slp;                // NMR distance r3, r4 slope
    PMEDouble2*                 pNMRDistanceK2K3Slp;                // NMR distance k2, k3 slope
    PMEDouble2*                 pNMRDistanceR1R2Int;                // NMR distance r1, r2 intercept
    PMEDouble2*                 pNMRDistanceR3R4Int;                // NMR distance r3, r4 intercept
    PMEDouble2*                 pNMRDistanceK2K3Int;                // NMR distance k2, k3 intercept
    int2*                       pNMRCOMDistanceID;                  // NMR COM distance i, j
    int2*                       pNMRCOMDistanceCOM;                 // NMR COM distance COM ranges from .x to .y
    int2*                       pNMRCOMDistanceCOMGrp;              // NMR COM distance COM indexing for no. of atom ranges in a COM group from .x to .y
    PMEDouble2*                 pNMRCOMDistanceR1R2;                // NMR COM distance computed r1, r2
    PMEDouble2*                 pNMRCOMDistanceR3R4;                // NMR COM distance computed r3, r4
    PMEDouble2*                 pNMRCOMDistanceK2K3;                // NMR COM distance computed k2, k3
    PMEDouble*                  pNMRCOMDistanceAve;                 // NMR COM distance restraint linear and exponential averaged value
    PMEDouble2*                 pNMRCOMDistanceTgtVal;              // NMR COM distance target and value for current step
    int2*                       pNMRCOMDistanceStep;                // NMR COM distance first and last step for application of restraint
    int*                        pNMRCOMDistanceInc;                 // NMR COM distance increment for step weighting
    PMEDouble2*                 pNMRCOMDistanceR1R2Slp;             // NMR COM distance r1, r2 slope
    PMEDouble2*                 pNMRCOMDistanceR3R4Slp;             // NMR COM distance r3, r4 slope
    PMEDouble2*                 pNMRCOMDistanceK2K3Slp;             // NMR COM distance k2, k3 slope
    PMEDouble2*                 pNMRCOMDistanceR1R2Int;             // NMR COM distance r1, r2 intercept
    PMEDouble2*                 pNMRCOMDistanceR3R4Int;             // NMR COM distance r3, r4 intercept
    PMEDouble2*                 pNMRCOMDistanceK2K3Int;             // NMR COM distance k2, k3 intercept
    int*                        pNMRCOMDistanceWeights;             // NMR COM distance weights
    PMEDouble*                  pNMRCOMDistanceXYZ;                 // NMR X,Y,Z COM Distance Components
// BUMP POTENTIAL
    int2*                       pNMRCOMBumpID;                  // NMR COM distance i, j
    int2*                       pNMRCOMBumpCOM;                 // NMR COM distance COM ranges from .x to .y
    int2*                       pNMRCOMBumpCOMGrp;              // NMR COM distance COM indexing for no. of atom ranges in a COM group from .x to .y
    PMEDouble2*                 pNMRCOMBumpR1R2;                // NMR COM distance computed r1, r2
    PMEDouble2*                 pNMRCOMBumpR3R4;                // NMR COM distance computed r3, r4
    PMEDouble2*                 pNMRCOMBumpK2K3;                // NMR COM distance computed k2, k3
    PMEDouble*                  pNMRCOMBumpAve;                 // NMR COM distance restraint linear and exponential averaged value
    PMEDouble2*                 pNMRCOMBumpTgtVal;              // NMR COM distance target and value for current step
    int2*                       pNMRCOMBumpStep;                // NMR COM distance first and last step for application of restraint
    int*                        pNMRCOMBumpInc;                 // NMR COM distance increment for step weighting
    PMEDouble2*                 pNMRCOMBumpR1R2Slp;             // NMR COM distance r1, r2 slope
    PMEDouble2*                 pNMRCOMBumpR3R4Slp;             // NMR COM distance r3, r4 slope
    PMEDouble2*                 pNMRCOMBumpK2K3Slp;             // NMR COM distance k2, k3 slope
    PMEDouble2*                 pNMRCOMBumpR1R2Int;             // NMR COM distance r1, r2 intercept
    PMEDouble2*                 pNMRCOMBumpR3R4Int;             // NMR COM distance r3, r4 intercept
    PMEDouble2*                 pNMRCOMBumpK2K3Int;             // NMR COM distance k2, k3 intercept
    int*                        pNMRCOMBumpWeights;             // NMR COM distance weights
    PMEDouble*                  pNMRCOMBumpXYZ;                 // NMR X,Y,Z COM Distance Components
// BUMP POTENTIAL
    int2*                       pNMRr6avDistanceID;                 // NMR r6av distance i, j
    int2*                       pNMRr6avDistancer6av;               // NMR r6av distance r6av ranges from .x to .y
    int2*                       pNMRr6avDistancer6avGrp;            // NMR r6av distance r6av indexing for no. of atom ranges in a r6av group from .x to .y
    PMEDouble2*                 pNMRr6avDistanceR1R2;               // NMR r6av distance computed r1, r2
    PMEDouble2*                 pNMRr6avDistanceR3R4;               // NMR r6av distance computed r3, r4
    PMEDouble2*                 pNMRr6avDistanceK2K3;               // NMR r6av distance computed k2, k3
    PMEDouble*                  pNMRr6avDistanceAve;                // NMR r6av distance restraint linear and exponential averaged value
    PMEDouble2*                 pNMRr6avDistanceTgtVal;             // NMR r6av distance target and value for current step
    int2*                       pNMRr6avDistanceStep;               // NMR r6av distance first and last step for application of restraint
    int*                        pNMRr6avDistanceInc;                // NMR r6av distance increment for step weighting
    PMEDouble2*                 pNMRr6avDistanceR1R2Slp;            // NMR r6av distance r1, r2 slope
    PMEDouble2*                 pNMRr6avDistanceR3R4Slp;            // NMR r6av distance r3, r4 slope
    PMEDouble2*                 pNMRr6avDistanceK2K3Slp;            // NMR r6av distance k2, k3 slope
    PMEDouble2*                 pNMRr6avDistanceR1R2Int;            // NMR r6av distance r1, r2 intercept
    PMEDouble2*                 pNMRr6avDistanceR3R4Int;            // NMR r6av distance r3, r4 intercept
    PMEDouble2*                 pNMRr6avDistanceK2K3Int;            // NMR r6av distance k2, k3 intercept
    int2*                       pNMRAngleID1;                       // NMR angle i, j
    int*                        pNMRAngleID2;                       // NMR angle k
    PMEDouble2*                 pNMRAngleR1R2;                      // NMR angle computed r1, r2
    PMEDouble2*                 pNMRAngleR3R4;                      // NMR angle computed r3, r4
    PMEDouble2*                 pNMRAngleK2K3;                      // NMR angle computed k2, k3
    PMEDouble*                  pNMRAngleAve;                       // NMR angle restraint linear and exponential averaged value
    PMEDouble2*                 pNMRAngleTgtVal;                    // NMR angle target and value for current step
    int2*                       pNMRAngleStep;                      // NMR angle first and last step for application of restraint
    int*                        pNMRAngleInc;                       // NMR angle increment for step weighting
    PMEDouble2*                 pNMRAngleR1R2Slp;                   // NMR angle r1, r2 slope
    PMEDouble2*                 pNMRAngleR3R4Slp;                   // NMR angle r3, r4 slope
    PMEDouble2*                 pNMRAngleK2K3Slp;                   // NMR angle k2, k3 slope
    PMEDouble2*                 pNMRAngleR1R2Int;                   // NMR angle r1, r2 intercept
    PMEDouble2*                 pNMRAngleR3R4Int;                   // NMR angle r3, r4 intercept
    PMEDouble2*                 pNMRAngleK2K3Int;                   // NMR angle k2, k3 intercept
    int4*                       pNMRTorsionID1;                     // NMR torsion i, j, k, l
    PMEDouble2*                 pNMRTorsionR1R2;                    // NMR torsion computed r1, r2
    PMEDouble2*                 pNMRTorsionR3R4;                    // NMR torsion computed r3, r4
    PMEDouble2*                 pNMRTorsionK2K3;                    // NMR torsion computed k2, k3
    PMEDouble*                  pNMRTorsionAve1;                    // NMR torsion restraint linear and exponential averaged value
    PMEDouble*                  pNMRTorsionAve2;                    // NMR torsion restraint linear and exponential averaged value
    PMEDouble2*                 pNMRTorsionTgtVal;                  // NMR torsion target and value for current step
    int2*                       pNMRTorsionStep;                    // NMR torsion first and last step for application of restraint
    int*                        pNMRTorsionInc;                     // NMR torsion increment for step weighting
    PMEDouble2*                 pNMRTorsionR1R2Slp;                 // NMR torsion r1, r2 slope
    PMEDouble2*                 pNMRTorsionR3R4Slp;                 // NMR torsion r3, r4 slope
    PMEDouble2*                 pNMRTorsionK2K3Slp;                 // NMR torsion k2, k3 slope
    PMEDouble*                  pNMRTorsionK4Slp;                   // NMR torsion k4 slope
    PMEDouble2*                 pNMRTorsionR1R2Int;                 // NMR torsion r1, r2 intercept
    PMEDouble2*                 pNMRTorsionR3R4Int;                 // NMR torsion r3, r4 intercept
    PMEDouble2*                 pNMRTorsionK2K3Int;                 // NMR torsion k2, k3 intercept


    // Cmap lookup table
    int                         cmapTermStride;                     // Per term stride
    int                         cmapRowStride;                      // Per row of terms stride
    PMEFloat4*                  pCmapEnergy;                        // Pointer to Cmap LUT data (E, dPhi, dPsi, dPhi_dPsi)

    // Accumulation buffers
    PMEAccumulator*             pForceAccumulator;                  // Bare fixed point force accumulator
    PMEAccumulator*             pForceXAccumulator;                 // Bare fixed point x force accumulator
    PMEAccumulator*             pForceYAccumulator;                 // Bare fixed point y force accumulator
    PMEAccumulator*             pForceZAccumulator;                 // Bare fixed point z force accumulator
    PMEAccumulator*             pNBForceAccumulator;                // Bare fixed point nonbond force accumulator
    PMEAccumulator*             pNBForceXAccumulator;               // Fixed point nonbond x force accumulator
    PMEAccumulator*             pNBForceYAccumulator;               // Fixed point nonbond y force accumulator
    PMEAccumulator*             pNBForceZAccumulator;               // Fixed point nonbond z force accumulator
    PMEAccumulator*             pReffAccumulator;                   // Effective Born Radius buffer
    PMEAccumulator*             pSumdeijdaAccumulator;              // Atom Sumdeijda buffer
    PMEAccumulator*             pBondedForceAccumulator;            // Bare fixed point bonded force accumulator
    PMEAccumulator*             pBondedForceXAccumulator;           // Bare fixed point bonded x force accumulator
    PMEAccumulator*             pBondedForceYAccumulator;           // Bare fixed point bonded y force accumulator
    PMEAccumulator*             pBondedForceZAccumulator;           // Bare fixed point bonded z force accumulator
    int                         stride;                             // Atom quantity stride
    int                         stride2;                            // Atom quantity 2x stride
    int                         stride3;                            // Atom quantity 3x stride
    int                         stride4;                            // Atom quantity 4x stride
    int                         imageStride;                        // Neighor list index stride (1K-aligned) for Duane Merrill's radix sort


    // Kernel call stuff
    unsigned int                workUnits;                          // Total work units
    unsigned int                excludedWorkUnits;                  // Total work units with exclusions
    unsigned int*               pExclusion;                         // Exclusion masks
    unsigned int*               pWorkUnit;                          // Work unit list
    unsigned int*               pGBBRPosition;                      // Generalized Born Born Radius workunit position
    unsigned int*               pGBNB1Position;                     // Generalized Born Nonbond 1 workunit position
    unsigned int*               pGBNB2Position;                     // Generalized Born Nonbond 2 workunit position
    unsigned int                GBTotalWarps[3];                    // Total warps in use for nonbond kernels
    unsigned int                maxForceBuffers;                    // Total Force buffer count
    unsigned int                nonbondForceBuffers;                // Nonbond Force buffer count
    PMEFloat                    cellOffset[NEIGHBORCELLS][3];       // Local coordinate system offsets

};


template <typename T> struct GpuBuffer;


struct _gpuContext {
    SM_VERSION                  sm_version;

    // Memory parameters
    bool                        bECCSupport;                // Flag for ECC support to detect Tesla versus consumer GPU
    bool                        bCanMapHostMemory;          // Flag for pinned memory support
    aligned_lli                 totalMemory;                // Total memory on GPU
    aligned_lli                 totalCPUMemory;             // Approximate total allocated CPU memory
    aligned_lli                 totalGPUMemory;             // Approximate total allocated CPU memory

    // AMBER parameters
    int                         ntt;                        // AMBER Thermostat setting
    int                         ntb;                        // AMBER PBC setting
    int                         ips;                        // AMBER IPS setting
    int                         ntc;                        // AMBER SHAKE setting
    int                         imin;                       // AMBER minimization versus MD setting
    int                         ntf;                        // AMBER force field setting
    int                         ntpr;                       // AMBER status output interval
    int                         ntwe;                       // AMBER energy output interval
    int                         ntr;                        // AMBER Restraint flags
    int                         gbsa;                       // AMBER GBSA active flag
    int                         step;                       // Current step
    bool                        bCPURandoms;                // Generate random numbers on CPU instead of GPU

    // Atom stuff
    GpuBuffer<double>*          pbAtom;                     // Atom coordinates
    GpuBuffer<PMEFloat2>*       pbAtomXYSP;                 // Single Precision Atom X and Y coordinates
    GpuBuffer<PMEFloat>*        pbAtomZSP;                  // Single Precision Atom Z coordinate
    GpuBuffer<PMEFloat2>*       pbAtomSigEps;               // Atom nonbond parameters
    GpuBuffer<unsigned int>*    pbAtomLJID;                 // Atom Lennard Jone index
    GpuBuffer<PMEFloat>*        pbAtomS;                    // Atom scaled Born Radius
    GpuBuffer<PMEFloat>*        pbAtomRBorn;                // Atom Born Radius
    GpuBuffer<double>*          pbAtomCharge;               // Atom charges
    GpuBuffer<PMEFloat>*        pbAtomChargeSP;             // Single precision atom charges
    GpuBuffer<PMEFloat2>*       pbAtomChargeSPLJID;         // Single precision atom charges and Lennard Jones atom type
    GpuBuffer<double>*          pbAtomMass;                 // Atom masses
    GpuBuffer<PMEDouble>*       pbReff;                     // Effective Born Radius
    GpuBuffer<PMEFloat>*        pbReffSP;                   // Single Precision Effective Born Radius
    GpuBuffer<PMEFloat>*        pbTemp7;                    // Single Precision Born Force
    GpuBuffer<PMEFloat>*        pbPsi;                      // GB intermediate psi value
    GpuBuffer<int>*             pbTIRegion;                 // Softcore TI Region
#ifdef MPI
    bool                        bCalculateLocalForces;      // Flag to signal calculating local forces on this process
    bool                        bCalculateReciprocalSum;    // Flag to signal calculating reciprocal sum on this process
    bool                        bCalculateDirectSum;        // Flag to signal calculating direct sum on this process
    GpuBuffer<PMEAccumulator>*  pbPeerAccumulator;          // Remote data accumulator buffer
    PMEAccumulator**            pPeerAccumulatorList;       // List of accumulator buffer addresses
    cudaIpcMemHandle_t*         pPeerAccumulatorMemHandle;  // Memory handles for each peer accumulator
#endif
    GpuBuffer<double>*          pbVel;                      // Atom velocities
    GpuBuffer<double>*          pbLVel;                     // Atom last velocities
    GpuBuffer<PMEFloat>*        pbCenter;                   // Atom recentering extrema

    // LJ Stuff
    GpuBuffer<PMEFloat2>*       pbLJTerm;                   // Pointer to LJ terms buffer

    // GB Data
    GpuBuffer<PMEFloat2>*       pbNeckMaxValPos;            // GB Born Radii and Energy correction data
    GpuBuffer<PMEFloat>*        pbGBAlphaBetaGamma;         // GB per atom Alpha, Beta, and Gamma

    // PME data
    cufftHandle                 forwardPlan;                // PME FFT plan for cuFFT
    cufftHandle                 backwardPlan;               // PME IFFT plan for cuFFT
    GpuBuffer<long long int>*   pblliXYZ_q;                 // SPFP long long int PME charge grid
    GpuBuffer<PMEFloat>*        pbXYZ_q;                    // PME charge grid/buffer
    GpuBuffer<PMEComplex>*      pbXYZ_qt;                   // FFTed PME charge grid
    GpuBuffer<PMEFloat>*        pbPrefac;                   // PME FFT pre-factors
    GpuBuffer<PMEFloat>*        pbFract;                    // PME fractional coordinates
    GpuBuffer<int2>*            pbTileBoundary;             // PME charge interpolation box boundaries (min/max)

    // Self energy values, calculated on host
    PMEDouble                   ee_plasma;                  // Component of PME self energy
    PMEDouble                   self_energy;                // Total self energy
    PMEDouble                   vdw_recip;                  // Reciprocal vdw correction energy

    // Neighbor list stuff
    bool                        bNeighborList;              // Neighbor list activation flag
    bool                        bNewNeighborList;           // Newly generated neighbor list activation flag
    bool                        bNeedNewNeighborList;       // Need newly generated neighbor list activation flag
    bool                        bOddNLCells;                // Flag to determine whether charge grid needs 8 or 16 output buffers
    bool                        bSmallBox;                  // Flag to determine whether any nonbond cell count is 1 or 2 on any axis
    unsigned int                neighborListBits;           // Number of bits in cell list
    GpuBuffer<unsigned int>*    pbImageIndex;               // Image indexing data
    GpuBuffer<PMEFloat2>*       pbAtomXYSaveSP;             // Saved neighbor list coordinates
    GpuBuffer<PMEFloat>*        pbAtomZSaveSP;              // Saved neighbor list coordinates
    GpuBuffer<double>*          pbImage;                    // Image coordinates
    GpuBuffer<double>*          pbImageVel;                 // Image velocities
    GpuBuffer<double>*          pbImageLVel;                // Image last velocities
    GpuBuffer<double>*          pbImageMass;                // Image masses
    GpuBuffer<double>*          pbImageCharge;              // Image charges
    GpuBuffer<PMEFloat2>*       pbImageSigEps;              // Image sigma/epsilon data for nonbond interactions
    GpuBuffer<unsigned int>*    pbImageLJID;                // Image Lennard Jones index for nonbond interactions
    GpuBuffer<int>*             pbImageTIRegion;            // Softcore TI region
    GpuBuffer<unsigned int>*    pbImageCellID;              // Image per atom nonbond cell ID
    GpuBuffer<unsigned int>*    pbNLExclusionList;          // Raw exclusion list
    GpuBuffer<uint2>*           pbNLExclusionStartCount;    // Per atom exclusion bounds
    GpuBuffer<uint2>*           pbNLNonbondCellStartEnd;    // Nonbond cells start and end
    GpuBuffer<bool>*            pbNLbSkinTestFail;          // Skin test result buffer
    GpuBuffer<unsigned int>*    pbNLCellHash;               // Spatial ordering hash for within cells
    GpuBuffer<NLRecord>*        pbNLRecord;                 // Pointer to neighbor list records
    GpuBuffer<NLEntry>*         pbNLEntry;                  // Active neighbor list
    GpuBuffer<unsigned int>*    pbNLAtomList;               // Pointer to atom list and exclusion data
    GpuBuffer<unsigned int>*    pbNLTotalOffset;            // Current atom list offset
    GpuBuffer<unsigned int>*    pbNLPosition;               // Position in building neighbor list
    GpuBuffer<unsigned int>*    pbNLEntries;                // Total nonbond entries
    GpuBuffer<unsigned int>*    pbBNLExclusionBuffer;       // Per-warp GMEM exclusion buffer
    PMEFloat                    nonbond_skin;               // Nonbond skin thickness

    // Remapped bonded interactions and Shake constraint atom IDs
    GpuBuffer<int2>*            pbImageBondID;              // Remapped Bond i, j
    GpuBuffer<int2>*            pbImageBondAngleID1;        // Remapped Bond Angle i, j
    GpuBuffer<int>*             pbImageBondAngleID2;        // Remapped Bond Angle k;
    GpuBuffer<int4>*            pbImageDihedralID1;         // Remapped Dihedral i, j, k, l
    GpuBuffer<int2>*            pbImageNb14ID;              // Remapped 1-4 nonbond i, j
    GpuBuffer<int>*             pbImageConstraintID;        // Remapped Atom constraint ID
    GpuBuffer<int2>*            pbImageUBAngleID;           // Remapped Urey Bradley Angle i, j
    GpuBuffer<int4>*            pbImageImpDihedralID1;      // Remapped Improper Dihedral i, j, k, l
    GpuBuffer<int4>*            pbImageCmapID1;             // Remapped Cmap i, j, k, l
    GpuBuffer<int>*             pbImageCmapID2;             // Remapped Cmap m
    GpuBuffer<int2>*            pbImageCmapID3;             // Remapped Cmap lbuff, mbuff
    GpuBuffer<int2>*            pbImageNMRDistanceID;       // Remapped NMR Distance i, j
    GpuBuffer<int2>*            pbImageNMRCOMDistanceID;    // Remapped NMR COM Distance i, j
    GpuBuffer<int2>*            pbImageNMRCOMDistanceCOM;   // Remapped NMR COM DistanceCOM range .x to .y
    GpuBuffer<int2>*            pbImageNMRCOMDistanceCOMGrp;// Remapped NMR COM DistanceCOMGrp range .x to .y
// BUMP POTENTIAL
    GpuBuffer<int2>*            pbImageNMRCOMBumpID;    // Remapped NMR COM Distance i, j
    GpuBuffer<int2>*            pbImageNMRCOMBumpCOM;   // Remapped NMR COM DistanceCOM range .x to .y
    GpuBuffer<int2>*            pbImageNMRCOMBumpCOMGrp;// Remapped NMR COM DistanceCOMGrp range .x to .y
// BUMP POTENTIAL
    GpuBuffer<int2>*            pbImageNMRr6avDistanceID;     // Remapped NMR r6av Distance i, j
    GpuBuffer<int2>*            pbImageNMRr6avDistancer6av;   // Remapped NMR r6av Distancer6av range .x to .y
    GpuBuffer<int2>*            pbImageNMRr6avDistancer6avGrp;// Remapped NMR r6av Distancer6avGrp range .x to .y
    GpuBuffer<int2>*            pbImageNMRAngleID1;         // Remapped NMR Angle i, j
    GpuBuffer<int>*             pbImageNMRAngleID2;         // Remapped NMR Angle k
    GpuBuffer<int4>*            pbImageNMRTorsionID1;       // Remapped NMR Torsion i, j, k, l
    GpuBuffer<int4>*            pbImageShakeID;             // Remapped traditional SHAKE IDs
    GpuBuffer<int4>*            pbImageFastShakeID;         // Remapped H2O (Fast) SHAKE IDs
    GpuBuffer<int>*             pbImageSlowShakeID1;        // Remapped Central atom of XH4 (Slow) Shake constraint
    GpuBuffer<int4>*            pbImageSlowShakeID2;        // Remapped XH4 (Slow) SHAKE constraint hydrogens
    GpuBuffer<int4>*            pbImageSolventAtomID;       // Remapped solvent molecules/ions
    GpuBuffer<int>*             pbImageSoluteAtomID;        // Remapped solute atoms

    // NTP molecule data
    int                         maxSoluteMolecules;         // Maximum solute molecules for most NTP kernels
    int                         maxPSSoluteMolecules;       // Maximum solute molecules for NTP pressure scaling kernels
    GpuBuffer<int>*             pbSoluteAtomID;             // Solute per atom ID
    GpuBuffer<PMEDouble>*       pbSoluteAtomMass;           // Solute atom masses
    GpuBuffer<PMEDouble>*       pbSolute;                   // Current and last centers of mass, kinetic energy and inverse mass for each solute molecule
    GpuBuffer<PMEUllInt>*       pbUllSolute;                // Current COM and EKCOM in integer form
    GpuBuffer<int4>*            pbSolventAtomID;            // List of solvent molecules/ions of 4 or fewer atoms
    GpuBuffer<PMEDouble>*       pbSolvent;                  // Last centers of mass, atom masses and inverse mass for each solvent molecule
    GpuBuffer<NTPData>*         pbNTPData;                  // NTP mutable values

    // NTP constraint molecule data
    GpuBuffer<PMEDouble>*       pbConstraintAtomX;          // Original atom x for constraint
    GpuBuffer<PMEDouble>*       pbConstraintAtomY;          // Original atom y for constraint
    GpuBuffer<PMEDouble>*       pbConstraintAtomZ;          // Original atom z for constraint
    GpuBuffer<PMEDouble>*       pbConstraintCOMX;           // Original x COM for constraint in fractional coordinates
    GpuBuffer<PMEDouble>*       pbConstraintCOMY;           // Original y COM for constraint in fractional coordinates
    GpuBuffer<PMEDouble>*       pbConstraintCOMZ;           // Original z COM for constraint in fractional coordinates

    // Radix sort data
    GpuBuffer<unsigned int>*    pbRadixBlockSum;            // Radix sums per thread group
    GpuBuffer<unsigned int>*    pbRadixCounter;             // Radix counters per thread group for scatter
    GpuBuffer<unsigned int>*    pbRadixSum;                 // Pointer to indivudal thread block radix sums

    // Random number stuff
    unsigned int                randomCounter;              // Counter for triggering RNG
    GpuBuffer<double>*          pbRandom;                   // Pointer to overall RNG buffer
    curandGenerator_t           RNG;                        // CURAND RNG

    // Bonded interactions
    bool                        bLocalInteractions;         // Flag indicating presence or absence of local interactions
    bool                        bCharmmInteractions;        // Flag indicating presence or absence of CHARMM interactions
    GpuBuffer<PMEDouble2>*      pbBond;                     // Bond Kr, Req
    GpuBuffer<int2>*            pbBondID;                   // Bond i, j
    GpuBuffer<PMEDouble2>*      pbBondAngle;                // Bond Angle Ka, Aeq
    GpuBuffer<int2>*            pbBondAngleID1;             // Bond Angle i, j
    GpuBuffer<int>*             pbBondAngleID2;             // Bond Angle k;
    GpuBuffer<PMEDouble2>*      pbDihedral1;                // Dihedral gmul, pn
    GpuBuffer<PMEDouble2>*      pbDihedral2;                // Dihedral pk, gamc
    GpuBuffer<PMEDouble>*       pbDihedral3;                // Dihedral gams
    GpuBuffer<int4>*            pbDihedralID1;              // Dihedral i, j, k, l
    GpuBuffer<PMEDouble2>*      pbNb141;                    // 1-4 nonbond scee, scnb0
    GpuBuffer<PMEDouble2>*      pbNb142;                    // 1-4 nonbond cn1, cn2
    GpuBuffer<int2>*            pbNb14ID;                   // 1-4 nonbond i, j
    GpuBuffer<PMEDouble2>*      pbConstraint1;              // Constraint weight and xc
    GpuBuffer<PMEDouble2>*      pbConstraint2;              // Constraint yc and zc
    GpuBuffer<int>*             pbConstraintID;             // Atom constraint ID
    GpuBuffer<PMEDouble2>*      pbUBAngle;                  // Urey Bradley Angle rk, r0
    GpuBuffer<int2>*            pbUBAngleID;                // Urey Bradley Angle i, j
    GpuBuffer<PMEDouble2>*      pbImpDihedral;              // Improper Dihedral pk, phase
    GpuBuffer<int4>*            pbImpDihedralID1;           // Improper Dihedral i, j, k, l
    GpuBuffer<int4>*            pbCmapID1;                  // Cmap i, j, k, l
    GpuBuffer<int>*             pbCmapID2;                  // Cmap m
    GpuBuffer<int>*             pbCmapType;                 // Cmap type
    GpuBuffer<PMEFloat4>*       pbCmapEnergy;               // Pointer to Cmap LUT data (E, dPhi, dPsi, dPhi_dPsi)

    // NMR stuff
    bool                        bNMRInteractions;           // Flag indicating presence or absence of NMR interactions
    int                         NMRnstep;                   // Imported NMR variable for time-dependent restraints
    GpuBuffer<double>*          pbNMRJarData;               // Jarzynski accumulated work data
    GpuBuffer<int2>*            pbNMRDistanceID;            // NMR distance i, j
    GpuBuffer<PMEDouble2>*      pbNMRDistanceR1R2;          // NMR distance computed r1, r2
    GpuBuffer<PMEDouble2>*      pbNMRDistanceR3R4;          // NMR distance computed r3, r4
    GpuBuffer<PMEDouble2>*      pbNMRDistanceK2K3;          // NMR distance computed k2, k3
    GpuBuffer<PMEDouble>*       pbNMRDistanceK4;            // NMR distance computed k4
    GpuBuffer<PMEDouble>*       pbNMRDistanceAve;           // NMR distance restraint linear and exponential averaged value
    GpuBuffer<PMEDouble2>*      pbNMRDistanceTgtVal;        // NMR distance target and actual value for current step
    GpuBuffer<int2>*            pbNMRDistanceStep;          // NMR distance first and last step for application of restraint
    GpuBuffer<int>*             pbNMRDistanceInc;           // NMR distance increment for step weighting
    GpuBuffer<PMEDouble2>*      pbNMRDistanceR1R2Slp;       // NMR distance r1, r2 slope
    GpuBuffer<PMEDouble2>*      pbNMRDistanceR3R4Slp;       // NMR distance r3, r4 slope
    GpuBuffer<PMEDouble2>*      pbNMRDistanceK2K3Slp;       // NMR distance k2, k3 slope
    GpuBuffer<PMEDouble>*       pbNMRDistanceK4Slp;         // NMR distance k4 slope
    GpuBuffer<PMEDouble2>*      pbNMRDistanceR1R2Int;       // NMR distance r1, r2 intercept
    GpuBuffer<PMEDouble2>*      pbNMRDistanceR3R4Int;       // NMR distance r3, r4 intercept
    GpuBuffer<PMEDouble2>*      pbNMRDistanceK2K3Int;       // NMR distance k2, k3 intercept
    GpuBuffer<PMEDouble>*       pbNMRDistanceK4Int;         // NMR distance k4 intercept
    GpuBuffer<int2>*            pbNMRCOMDistanceID;         // NMR COM distance i, j
    GpuBuffer<int2>*            pbNMRCOMDistanceCOM;        // NMR COM distance COM ranges from .x to .y
    GpuBuffer<int2>*            pbNMRCOMDistanceCOMGrp;     // NMR COM distance COM indexing for no. of atom ranges in a COM group from .x to .y
    GpuBuffer<PMEDouble2>*      pbNMRCOMDistanceR1R2;       // NMR COM distance computed r1, r2
    GpuBuffer<PMEDouble2>*      pbNMRCOMDistanceR3R4;       // NMR COM distance computed r3, r4
    GpuBuffer<PMEDouble2>*      pbNMRCOMDistanceK2K3;       // NMR COM distance computed k2, k3
    GpuBuffer<PMEDouble>*       pbNMRCOMDistanceK4;         // NMR COM distance computed k4
    GpuBuffer<PMEDouble>*       pbNMRCOMDistanceAve;        // NMR COM distance restraint linear and exponential averaged value
    GpuBuffer<PMEDouble2>*      pbNMRCOMDistanceTgtVal;     // NMR COM distance target and actual value for current step
    GpuBuffer<int2>*            pbNMRCOMDistanceStep;       // NMR COM distance first and last step for application of restraint
    GpuBuffer<int>*             pbNMRCOMDistanceInc;        // NMR COM distance increment for step weighting
    GpuBuffer<PMEDouble2>*      pbNMRCOMDistanceR1R2Slp;    // NMR COM distance r1, r2 slope
    GpuBuffer<PMEDouble2>*      pbNMRCOMDistanceR3R4Slp;    // NMR COM distance r3, r4 slope
    GpuBuffer<PMEDouble2>*      pbNMRCOMDistanceK2K3Slp;    // NMR COM distance k2, k3 slope
    GpuBuffer<PMEDouble>*       pbNMRCOMDistanceK4Slp;      // NMR COM distance k4 slope
    GpuBuffer<PMEDouble2>*      pbNMRCOMDistanceR1R2Int;    // NMR COM distance r1, r2 intercept
    GpuBuffer<PMEDouble2>*      pbNMRCOMDistanceR3R4Int;    // NMR COM distance r3, r4 intercept
    GpuBuffer<PMEDouble2>*      pbNMRCOMDistanceK2K3Int;    // NMR COM distance k2, k3 intercept
    GpuBuffer<PMEDouble>*       pbNMRCOMDistanceK4Int;      // NMR COM distance k4 intercept
    GpuBuffer<int>*             pbNMRCOMDistanceWeights;    // NMR COM distance weights
    GpuBuffer<PMEDouble>*       pbNMRCOMDistanceXYZ;        // NMR COM X,Y,Z distance components
// BUMP POTENTIAL
    GpuBuffer<int2>*            pbNMRCOMBumpID;             // NMR COM bump distance i, j
    GpuBuffer<int2>*            pbNMRCOMBumpCOM;            // NMR COM bump distance COM ranges from .x to .y
    GpuBuffer<int2>*            pbNMRCOMBumpCOMGrp;         // NMR COM bump distance COM indexing for no. of atom ranges in a COM group from .x to .y
    GpuBuffer<PMEDouble2>*      pbNMRCOMBumpR1R2;           // NMR COM bump distance computed r1, r2
    GpuBuffer<PMEDouble2>*      pbNMRCOMBumpR3R4;           // NMR COM bump distance computed r3, r4
    GpuBuffer<PMEDouble2>*      pbNMRCOMBumpK2K3;           // NMR COM bump distance computed k2, k3
    GpuBuffer<PMEDouble>*       pbNMRCOMBumpK4;             // NMR COM bump distance computed k4
    GpuBuffer<PMEDouble>*       pbNMRCOMBumpAve;            // NMR COM bump distance restraint linear and exponential averaged value
    GpuBuffer<PMEDouble2>*      pbNMRCOMBumpTgtVal;         // NMR COM bump distance target and actual value for current step
    GpuBuffer<int2>*            pbNMRCOMBumpStep;           // NMR COM bump distance first and last step for application of restraint
    GpuBuffer<int>*             pbNMRCOMBumpInc;            // NMR COM bump distance increment for step weighting
    GpuBuffer<PMEDouble2>*      pbNMRCOMBumpR1R2Slp;        // NMR COM bump distance r1, r2 slope
    GpuBuffer<PMEDouble2>*      pbNMRCOMBumpR3R4Slp;        // NMR COM bump distance r3, r4 slope
    GpuBuffer<PMEDouble2>*      pbNMRCOMBumpK2K3Slp;        // NMR COM bump distance k2, k3 slope
    GpuBuffer<PMEDouble>*       pbNMRCOMBumpK4Slp;          // NMR COM bump distance k4 slope
    GpuBuffer<PMEDouble2>*      pbNMRCOMBumpR1R2Int;        // NMR COM bump distance r1, r2 intercept
    GpuBuffer<PMEDouble2>*      pbNMRCOMBumpR3R4Int;        // NMR COM bump distance r3, r4 intercept
    GpuBuffer<PMEDouble2>*      pbNMRCOMBumpK2K3Int;        // NMR COM bump distance k2, k3 intercept
    GpuBuffer<PMEDouble>*       pbNMRCOMBumpK4Int;          // NMR COM bump distance k4 intercept
    GpuBuffer<int>*             pbNMRCOMBumpWeights;        // NMR COM bump distance weights
    GpuBuffer<PMEDouble>*       pbNMRCOMBumpXYZ;            // NMR COM bump X,Y,Z distance components
// BUMP POTENTIAL
    GpuBuffer<int2>*            pbNMRr6avDistanceID;        // NMR r6av distance i, j
    GpuBuffer<int2>*            pbNMRr6avDistancer6av;      // NMR r6av distance r6av ranges from .x to .y
    GpuBuffer<int2>*            pbNMRr6avDistancer6avGrp;   // NMR r6av distance r6av indexing for no. of atom ranges in a r6av group from .x to .y
    GpuBuffer<PMEDouble2>*      pbNMRr6avDistanceR1R2;      // NMR r6av distance computed r1, r2
    GpuBuffer<PMEDouble2>*      pbNMRr6avDistanceR3R4;      // NMR r6av distance computed r3, r4
    GpuBuffer<PMEDouble2>*      pbNMRr6avDistanceK2K3;      // NMR r6av distance computed k2, k3
    GpuBuffer<PMEDouble>*       pbNMRr6avDistanceK4;        // NMR r6av distance computed k4
    GpuBuffer<PMEDouble>*       pbNMRr6avDistanceAve;       // NMR r6av distance restraint linear and exponential averaged value
    GpuBuffer<PMEDouble2>*      pbNMRr6avDistanceTgtVal;    // NMR r6av distance target and actual value for current step
    GpuBuffer<int2>*            pbNMRr6avDistanceStep;      // NMR r6av distance first and last step for application of restraint
    GpuBuffer<int>*             pbNMRr6avDistanceInc;       // NMR r6av distance increment for step weighting
    GpuBuffer<PMEDouble2>*      pbNMRr6avDistanceR1R2Slp;   // NMR r6av distance r1, r2 slope
    GpuBuffer<PMEDouble2>*      pbNMRr6avDistanceR3R4Slp;   // NMR r6av distance r3, r4 slope
    GpuBuffer<PMEDouble2>*      pbNMRr6avDistanceK2K3Slp;   // NMR r6av distance k2, k3 slope
    GpuBuffer<PMEDouble>*       pbNMRr6avDistanceK4Slp;     // NMR r6av distance k4 slope
    GpuBuffer<PMEDouble2>*      pbNMRr6avDistanceR1R2Int;   // NMR r6av distance r1, r2 intercept
    GpuBuffer<PMEDouble2>*      pbNMRr6avDistanceR3R4Int;   // NMR r6av distance r3, r4 intercept
    GpuBuffer<PMEDouble2>*      pbNMRr6avDistanceK2K3Int;   // NMR r6av distance k2, k3 intercept
    GpuBuffer<PMEDouble>*       pbNMRr6avDistanceK4Int;     // NMR r6av distance k4 intercept
    GpuBuffer<int2>*            pbNMRAngleID1;              // NMR angle i, j
    GpuBuffer<int>*             pbNMRAngleID2;              // NMR angle k
    GpuBuffer<PMEDouble2>*      pbNMRAngleR1R2;             // NMR angle computed r1, r2
    GpuBuffer<PMEDouble2>*      pbNMRAngleR3R4;             // NMR angle computed r3, r4
    GpuBuffer<PMEDouble2>*      pbNMRAngleK2K3;             // NMR angle computed k2, k3
    GpuBuffer<PMEDouble>*       pbNMRAngleK4;               // NMR angle computed k4
    GpuBuffer<PMEDouble>*       pbNMRAngleAve;              // NMR angle restraint linear and exponential averaged value
    GpuBuffer<PMEDouble2>*      pbNMRAngleTgtVal;           // NMR angle target and actual value for current step
    GpuBuffer<int2>*            pbNMRAngleStep;             // NMR angle first and last step for application of restraint
    GpuBuffer<int>*             pbNMRAngleInc;              // NMR angle increment for step weighting
    GpuBuffer<PMEDouble2>*      pbNMRAngleR1R2Slp;          // NMR angle r1, r2 slope
    GpuBuffer<PMEDouble2>*      pbNMRAngleR3R4Slp;          // NMR angle r3, r4 slope
    GpuBuffer<PMEDouble2>*      pbNMRAngleK2K3Slp;          // NMR angle k2, k3 slope
    GpuBuffer<PMEDouble>*       pbNMRAngleK4Slp;            // NMR angle k4 slope
    GpuBuffer<PMEDouble2>*      pbNMRAngleR1R2Int;          // NMR angle r1, r2 intercept
    GpuBuffer<PMEDouble2>*      pbNMRAngleR3R4Int;          // NMR angle r3, r4 intercept
    GpuBuffer<PMEDouble2>*      pbNMRAngleK2K3Int;          // NMR angle k2, k3 intercept
    GpuBuffer<PMEDouble>*       pbNMRAngleK4Int;            // NMR angle k4 intercept
    GpuBuffer<int4>*            pbNMRTorsionID1;            // NMR torsion i, j, k, l
    GpuBuffer<PMEDouble2>*      pbNMRTorsionR1R2;           // NMR torsion computed r1, r2
    GpuBuffer<PMEDouble2>*      pbNMRTorsionR3R4;           // NMR torsion computed r3, r4
    GpuBuffer<PMEDouble2>*      pbNMRTorsionK2K3;           // NMR torsion computed k2, k3
    GpuBuffer<PMEDouble>*       pbNMRTorsionK4;             // NMR torsion computed k4
    GpuBuffer<PMEDouble>*       pbNMRTorsionAve1;           // NMR torsion restraint linear and exponential averaged value
    GpuBuffer<PMEDouble>*       pbNMRTorsionAve2;           // NMR torsion restraint linear and exponential averaged value
    GpuBuffer<PMEDouble2>*      pbNMRTorsionTgtVal;         // NMR torsion target and actual value for current step
    GpuBuffer<int2>*            pbNMRTorsionStep;           // NMR torsion first and last step for application of restraint
    GpuBuffer<int>*             pbNMRTorsionInc;            // NMR torsion increment for step weighting
    GpuBuffer<PMEDouble2>*      pbNMRTorsionR1R2Slp;        // NMR torsion r1, r2 slope
    GpuBuffer<PMEDouble2>*      pbNMRTorsionR3R4Slp;        // NMR torsion r3, r4 slope
    GpuBuffer<PMEDouble2>*      pbNMRTorsionK2K3Slp;        // NMR torsion k2, k3 slope
    GpuBuffer<PMEDouble>*       pbNMRTorsionK4Slp;          // NMR torsion k4 slope
    GpuBuffer<PMEDouble2>*      pbNMRTorsionR1R2Int;        // NMR torsion r1, r2 intercept
    GpuBuffer<PMEDouble2>*      pbNMRTorsionR3R4Int;        // NMR torsion r3, r4 intercept
    GpuBuffer<PMEDouble2>*      pbNMRTorsionK2K3Int;        // NMR torsion k2, k3 intercept
    GpuBuffer<PMEDouble>*       pbNMRTorsionK4Int;          // NMR torsion k4 intercept

    // Force accumulators for atomic ops
    GpuBuffer<PMEAccumulator>*  pbReffAccumulator;          // Effective Born Radius accumulator
    GpuBuffer<PMEAccumulator>*  pbSumdeijdaAccumulator;     // Sumdeijda accumulator
    GpuBuffer<PMEAccumulator>*  pbForceAccumulator;         // Force accumulators
    GpuBuffer<unsigned long long int>*   pbEnergyBuffer;    // Energy accumulation buffer
    GpuBuffer<KineticEnergy>*   pbKineticEnergyBuffer;      // Kinetic energy accumulation buffer

    // AMD buffers
    PMEDouble*                  pAmdWeightsAndEnergy;       // AMD
    GpuBuffer<PMEDouble>*       pbAMDfwgtd;

    // GaMD buffers
    PMEDouble*                  pGaMDWeightsAndEnergy;       // GaMD
    GpuBuffer<PMEDouble>*       pbGaMDfwgtd;

    // Constant pH stuff
    GpuBuffer<double>*          pbChargeRefreshBuffer;      // Pointer to new charges buffer

    // Extra points data
    GpuBuffer<int4>*            pbExtraPoint11Frame;        // Type 1 single extra point parent_atm, atm1, atm2, atm3
    GpuBuffer<int>*             pbExtraPoint11Index;        // Type 1 single extra point EP
    GpuBuffer<double>*          pbExtraPoint11;             // Type 1 single extra point coordinates
    GpuBuffer<int4>*            pbExtraPoint12Frame;        // Type 2 single extra point parent_atm, atm1, atm2, atm3
    GpuBuffer<int>*             pbExtraPoint12Index;        // Type 2 single extra point EP
    GpuBuffer<double>*          pbExtraPoint12;             // Type 2 single extra point coordinates
    GpuBuffer<int4>*            pbExtraPoint21Frame;        // Type 1 dual extra point parent_atm, atm1, atm2, atm3
    GpuBuffer<int2>*            pbExtraPoint21Index;        // type 1 dual extra point EP1, EP2
    GpuBuffer<double>*          pbExtraPoint21;             // Type 1 dual extra point coordinates
    GpuBuffer<int4>*            pbExtraPoint22Frame;        // Type 2 dual extra point parent_atm, atm1, atm2, atm3
    GpuBuffer<int2>*            pbExtraPoint22Index;        // Type 2 dual extra point EP1. EP2
    GpuBuffer<double>*          pbExtraPoint22;             // Type 2 dual extra point coordinates
    GpuBuffer<int4>*            pbImageExtraPoint11Frame;   // Remapped Type 1 single extra point parent_atm, atm1, atm2, atm3
    GpuBuffer<int>*             pbImageExtraPoint11Index;   // Remapped Type 1 single extra point EP
    GpuBuffer<int4>*            pbImageExtraPoint12Frame;   // Remapped Type 2 single extra point parent_atm, atm1, atm2, atm3
    GpuBuffer<int>*             pbImageExtraPoint12Index;   // Remapped Type 2 single extra point EP
    GpuBuffer<int4>*            pbImageExtraPoint21Frame;   // Remapped Type 1 dual extra point parent_atm, atm1, atm2, atm3
    GpuBuffer<int2>*            pbImageExtraPoint21Index;   // Remapped type 1 dual extra point EP1, EP2
    GpuBuffer<int4>*            pbImageExtraPoint22Frame;   // Remapped Type 2 dual extra point parent_atm, atm1, atm2, atm3
    GpuBuffer<int2>*            pbImageExtraPoint22Index;   // Remapped Type 2 dual extra point EP1. EP2

    // SHAKE constraints
    bool                        bUseHMR;                    // use HMR for SHAKE flag
    GpuBuffer<int4>*            pbShakeID;                  // SHAKE central Atom plus up to 3 hydrogens
    GpuBuffer<double2>*         pbShakeParm;                // SHAKE central atom mass and equilibrium bond length
    GpuBuffer<double>*          pbShakeInvMassH;            // SHAKE HMR inverse hydrogen mass
    GpuBuffer<int4>*            pbFastShakeID;              // H2O oxygen plus two hydrogens atom ID
    GpuBuffer<int>*             pbSlowShakeID1;             // Central atom of XH4 Shake constraint
    GpuBuffer<int4>*            pbSlowShakeID2;             // XH4 SHAKE constraint hydrogens
    GpuBuffer<double2>*         pbSlowShakeParm;            // XH4 SHAKE central atom mass and equilibrium bond length
    GpuBuffer<double>*          pbSlowShakeInvMassH;        // XH4 SHAKE HMR inverse hydrogen mass


    // Launch parameters
    int                         gpu_device_id;              // allow -1 for default = choose gpu based on memory.
    unsigned int                blocks;
    unsigned int                threadsPerBlock;
    unsigned int                clearForcesThreadsPerBlock;
    unsigned int                NLClearForcesThreadsPerBlock;
    unsigned int                reduceForcesThreadsPerBlock;
    unsigned int                NLReduceForcesThreadsPerBlock;
    unsigned int                reduceBufferThreadsPerBlock;
    unsigned int                localForcesBlocks;
    unsigned int                localForcesThreadsPerBlock;
    unsigned int                CHARMMForcesBlocks;
    unsigned int                CHARMMForcesThreadsPerBlock;
    unsigned int                NMRForcesBlocks;
    unsigned int                NMRForcesThreadsPerBlock;
    unsigned int                GBBornRadiiThreadsPerBlock;
    unsigned int                GBBornRadiiIGB78ThreadsPerBlock;
    unsigned int                GBNonbondEnergy1ThreadsPerBlock;
    unsigned int                GBNonbondEnergy2ThreadsPerBlock;
    unsigned int                GBNonbondEnergy2IGB78ThreadsPerBlock;
    unsigned int                PMENonbondEnergyThreadsPerBlock;
    unsigned int                PMENonbondForcesThreadsPerBlock;
    unsigned int                IPSNonbondEnergyThreadsPerBlock;
    unsigned int                IPSNonbondForcesThreadsPerBlock;
    unsigned int                updateThreadsPerBlock;
    unsigned int                shakeThreadsPerBlock;
    unsigned int                generalThreadsPerBlock;
    unsigned int                GBBornRadiiBlocks;
    unsigned int                GBNonbondEnergy1Blocks;
    unsigned int                GBNonbondEnergy2Blocks;
    unsigned int                PMENonbondBlocks;
    unsigned int                IPSNonbondBlocks;
    unsigned int                BNLBlocks;
    unsigned int                NLCalculateOffsetsThreadsPerBlock;
    unsigned int                NLBuildNeighborList32ThreadsPerBlock;
    unsigned int                NLBuildNeighborList16ThreadsPerBlock;
    unsigned int                NLBuildNeighborList8ThreadsPerBlock;
    unsigned int                updateBlocks;
    unsigned int                readSize;
    bool                        bNoDPTexture;               // Kludge for HW bug on Ge Force GPUs that freak out when a kernel uses lots of DP and Texture

    // Nonbond Kernel stuff
    GpuBuffer<unsigned int>*    pbExclusion;                // Exclusion masks
    GpuBuffer<unsigned int>*    pbWorkUnit;                 // Work unit list
    GpuBuffer<unsigned int>*    pbGBPosition;               // Nonbond kernel worunit positions

    cudaSimulation              sim;                        // All simulation data destined for GPU constant RAM


#ifdef MPI
    // Single-node multi-gpu parameters
    bool                        bSingleNode;                // Flag to indicate MPI run is all on one node
    bool                        bP2P;                       // Flag to indicate P2P connectivity between all processes

    // Multi-gpu parameters
    int                         nGpus;                      // Number of GPUs involved in calculation
    int                         gpuID;                      // Local GPU ID
    MPI_Comm                    comm;                       // MPI Communicator for all collective MPI calls
    int                         minLocalCell;               // First nonbond cell owned by local GPU
    int                         maxLocalCell;               // End of nonbond cells owned by local GPU
    int*                        pMinLocalCell;              // First cell owned by each node
    int*                        pMaxLocalCell;              // Last cell owned by each node
    int*                        pMinLocalAtom;              // First atom owned by each node
    int*                        pMaxLocalAtom;              // Last atom owned by each node
#endif

#ifdef AWSMM
    // AWSMM stuff
    int                         postProcessingFlags;        // List of desired post-processing steps
    int                         nAlphaCarbons;              // Number of alpha carbons in system
    int*                        pAlphaCarbonIndex;          // Alpha carbon indices
    double*                     pRefAlphaCarbon;            // Alpha carbon initial coordinates
    FILE*                       pPPRMSD;                    // Pointer to RMSD output file
    FILE*                       pPPEnergy;                  // Pointer to energy output file
    FILE*                       pPPVelocity;                // Pointer to velocity output file
#endif

    // Methods
    _gpuContext();
    ~_gpuContext();
};

typedef struct _gpuContext *gpuContext;

template <typename T>
struct GpuBuffer
{
    unsigned long long int  _length;
    bool                    _bSysMem;
    bool                    _bPinned;
    T*                      _pSysData;
    T*                      _pDevData;
    GpuBuffer(int length, bool bSysMem = true, bool bPinned = false);
    GpuBuffer(unsigned int length, bool bSysMem = true, bool bPinned = false);
    virtual ~GpuBuffer();
    void Allocate();
    void Deallocate();
    void Upload(T* pBuff = NULL);
    void Download(T * pBuff = NULL);
};

#ifdef GPU_CPP
static gpuContext gpu = NULL;

template <typename T>
GpuBuffer<T>::GpuBuffer(unsigned int length, bool bSysMem, bool bPinned) : _length(length), _bSysMem(bSysMem), _bPinned(bPinned), _pSysData(NULL), _pDevData(NULL)
{
    Allocate();
}

template <typename T>
GpuBuffer<T>::GpuBuffer(int length, bool bSysMem, bool bPinned) : _length(length), _bSysMem(bSysMem), _bPinned(bPinned), _pSysData(NULL), _pDevData(NULL)
{
    Allocate();
}

template <typename T>
GpuBuffer<T>::~GpuBuffer()
{
    Deallocate();
}

template <typename T>
void GpuBuffer<T>::Allocate()
{
#ifdef MEMTRACKING
    printf("Allocating %llu bytes of GPU memory", _length * sizeof(T));
    if (!_bSysMem && !_bPinned)
        printf(", unshadowed");
    else if (_bPinned)
        printf(", pinned");
    printf("\n");
#endif
    cudaError_t status;
    if (_bPinned)
    {
        status = cudaHostAlloc((void **)&_pSysData, _length * sizeof(T), cudaHostAllocMapped);
        RTERROR(status, "cudaHostalloc GpuBuffer::Allocate failed");
        gpu->totalCPUMemory                +=  _length * sizeof(T);
        gpu->totalGPUMemory                +=  _length * sizeof(T);
        status = cudaHostGetDevicePointer((void **)&_pDevData, (void *)_pSysData, 0);
        RTERROR(status, "cudaGetDevicePointer GpuBuffer::failed to get device pointer");
        memset(_pSysData, 0, _length * sizeof(T));
    }
    else
    {
        if (_bSysMem)
        {
            _pSysData =     new T[_length];
            gpu->totalCPUMemory            +=  _length * sizeof(T);
            memset(_pSysData, 0, _length * sizeof(T));
        }

        status = cudaMalloc((void **) &_pDevData, _length * sizeof(T));
        gpu->totalGPUMemory                +=  _length * sizeof(T);
        RTERROR(status, "cudaMalloc GpuBuffer::Allocate failed");
        status = cudaMemset((void *) _pDevData, 0, _length * sizeof(T));
        RTERROR(status, "cudaMemset GpuBuffer::Allocate failed");
    }
#ifdef MEMTRACKING
    printf("Mem++: %llu %llu\n", gpu->totalGPUMemory, gpu->totalCPUMemory);
#endif
}

template <typename T>
void GpuBuffer<T>::Deallocate()
{
    cudaError_t status;
    if (_bPinned)
    {
        status = cudaFreeHost(_pSysData);
        gpu->totalCPUMemory                -=  _length * sizeof(T);
        gpu->totalGPUMemory                -=  _length * sizeof(T);
    }
    else
    {
        if (_bSysMem)
        {
            delete[] _pSysData;
            gpu->totalCPUMemory            -=  _length * sizeof(T);
        }
        status = cudaFree(_pDevData);
        gpu->totalGPUMemory                -=  _length * sizeof(T);
    }
    RTERROR(status, "cudaFree GpuBuffer::Deallocate failed");
    _pSysData = NULL;
    _pDevData = NULL;
#ifdef MEMTRACKING
    printf("Mem--: %lld %lld\n", gpu->totalGPUMemory, gpu->totalCPUMemory);
#endif
}
#endif

template <typename T>
void GpuBuffer<T>::Upload(T* pBuff)
{
    if (pBuff)
    {
        cudaError_t status;
        status = cudaMemcpy(_pDevData, pBuff, _length * sizeof(T), cudaMemcpyHostToDevice);
        RTERROR(status, "cudaMemcpy GpuBuffer::Upload failed");
    }
    else if (_bSysMem)
    {
        cudaError_t status;
        status = cudaMemcpy(_pDevData, _pSysData, _length * sizeof(T), cudaMemcpyHostToDevice);
        RTERROR(status, "cudaMemcpy GpuBuffer::Upload failed");
    }
}

template <typename T>
void GpuBuffer<T>::Download(T* pBuff)
{
    if (pBuff)
    {
        cudaError_t status;
        status = cudaMemcpy(pBuff, _pDevData, _length * sizeof(T), cudaMemcpyDeviceToHost);
        RTERROR(status, "cudaMemcpy GpuBuffer::Download failed");
    }
    else if (_bSysMem)
    {
        cudaError_t status;
        status = cudaMemcpy(_pSysData, _pDevData, _length * sizeof(T), cudaMemcpyDeviceToHost);
        RTERROR(status, "cudaMemcpy GpuBuffer::Download failed");
    }
}

#if defined(__CUDA_ARCH__) && (__CUDA_ARCH__ < 200)
#define printf(f,...)
#endif
#endif


