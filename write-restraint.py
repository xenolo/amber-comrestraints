# -*- coding: utf-8 -*-
# A script to write the restraints file
# the COM bump potential is applied between 
# the backbone atoms of two groups of residues.
#
#
text = '&rst iat=-1,-1,\n '\
    'iresid=1,irstyp=0,ifvari=0,ir6=-1,\n '\
    'r1=30,r2=35,r3=45,r4=50,rk2=10.0,rk3=10.0 \n'\
    'igr1='
gr1 = ''    
grnam1 = ''    
for i,resi in enumerate(range(33,115,2)):
    gr1 = gr1 + '{},'.format(resi)
    grnam1 = grnam1 + "grnam1({})='CA','C','O',".format(i+1)

gr2 = 'igr2='    
grnam2 = '' 
for i,resi in enumerate(range(261,307,2)):
    gr2 = gr2 + '{},'.format(resi)
    grnam2 = grnam2 + "grnam2({})='CA','C','O',".format(i+1)    
    
    
print(text + gr1 +'\n'+ grnam1 + '\n' + gr2 +'\n'+ grnam2 + '\n/' )

f= open("prod/rst.dat","w+")
f.write(text + gr1 +'\n'+ grnam1 + '\n' + gr2 +'\n'+ grnam2 + '\n/' + '\n')
f.close()

